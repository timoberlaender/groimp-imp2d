# Math plugin 

Math defines interfaces and implementations for simple functions, vertex
lists, B-spline curves and surfaces. This includes non-uniform rational B-
splines, i. e., NURBS. A lot of algorithms of _The NURBS book [L. Piegl and W. Tiller.]_ like the computation
of swept surfaces are also provided.
