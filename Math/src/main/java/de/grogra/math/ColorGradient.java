/*
 * Copyright (C) 2020 GroIMP Developer Team (MH)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.math;

import java.awt.Color;

import de.grogra.graph.GraphState;

public class ColorGradient {

	private String name = "jet";
	private float min = 0;
	private float max = 100;

	private int mode = 0;

	// Some predefined gradients are inspired by plotly (https://github.com/plotly)

	/** BLUE, CYAN, GREEN, YELLOW, ORANGE, RED */
	public static final Color[] HEAT = new Color[] { Color.BLUE, Color.CYAN, Color.GREEN, Color.YELLOW, Color.ORANGE,
			Color.RED };

	/** WHITE, GREEN */
	public static final Color[] YIGNBN = new Color[] { Color.WHITE, Color.GREEN };

	/** BLACK GREEN YELLOW WHITE */
	public static final Color[] LEMON = new Color[] { Color.BLACK,Color.GREEN.darker().darker(), Color.GREEN.darker(), Color.ORANGE, Color.YELLOW, Color.WHITE };

	/** RED, ORANGE, GOLD, TAN, WHITE */
	public static final Color[] YIORRD = new Color[] { Color.RED, Color.ORANGE, Color.decode("#ffd700"),
			Color.decode("#d2b48c"), Color.WHITE };

	/** BRIGHT_BLUE, PURPLE, BRIGHT_RED */
	public static final Color[] BLUERED = new Color[] { Color.decode("#00a1c2"), Color.decode("#800080"),
			Color.decode("#be0032") };

	/** BLUE, RED */
	public static final Color[] RDBU = new Color[] { Color.BLUE, Color.RED };

	/** BLUE, LIGHT_BLUE, WHITE, PINK */
	public static final Color[] PICNIC = new Color[] { Color.BLUE, Color.decode("#add8e6"), Color.WHITE, Color.PINK };

	/** BLUE, GREEN, YELLOW, ORANGE, RED */
	public static final Color[] PORTLAND = new Color[] { Color.BLUE, Color.GREEN, Color.YELLOW, Color.ORANGE,
			Color.RED };

	/** BLUE, LIGHT_BLUE, GREEN, YELLOW, ORANGE, RED */
	public static final Color[] JET = new Color[] { Color.BLUE, Color.CYAN, Color.GREEN, Color.YELLOW, Color.ORANGE, Color.RED };

	/** TAN, YELLOW, RED, BLACK */
	public static final Color[] HOT = new Color[] { Color.BLACK, Color.RED, Color.YELLOW, Color.WHITE };

	/** BLACK, RED, YELLOW, WHITE, LIGHT_BLUE */
	public static final Color[] BLACKBODY = new Color[] { Color.BLACK, Color.RED, Color.YELLOW, Color.WHITE,
			Color.decode("#add8e6") };

	/** BLUE, GREEN, YELLOW, BROWN, TAN, WHITE */
	public static final Color[] EARTH = new Color[] { Color.BLUE, Color.GREEN, Color.YELLOW, Color.decode("#a52a2a"),
			Color.decode("#d2b48c"), Color.WHITE };

	/** BLACK, PURPLE, ORANGE, YELLOW, WHITE */
	public static final Color[] ELECTRIC = new Color[] { Color.BLACK, Color.decode("#800080"), Color.ORANGE,
			Color.YELLOW, Color.WHITE };

	/** BLUE, RED, YELLOW */
	public static final Color[] WEI = new Color[] { Color.BLUE, Color.RED, Color.YELLOW };
	public static final Color[] WEI2 = new Color[] { Color.BLACK, Color.BLUE.darker().darker(), Color.decode("#800080"),
			Color.MAGENTA.darker(), Color.RED.brighter(), Color.ORANGE, Color.YELLOW, Color.WHITE };

	/** RED, YELLOW, WHITE, CYAN, BLUE */
	public static final Color[] HARMONIC = new Color[] { Color.RED, Color.YELLOW, Color.WHITE, Color.CYAN, Color.BLUE };
	/** ?? */
	public static final Color[] PURPLE = new Color[] { Color.decode("#5e0063"), Color.decode("#8a175c"),
			Color.decode("#b13b4c"), Color.decode("#d88e6c"), Color.decode("#ffebaa") };

	public Color[] USER = null;

	public ColorGradient() {
	}

	public ColorGradient(float min, float max) {
		setBorders(min, max);
	}

	public ColorGradient(String name, float min, float max, GraphState graphState) {
		setMap(name);
		setBorders(min, max);
		setGraphState(graphState);
		setMode(1);
	}

	public ColorGradient(String name, GraphState graphState) {
		setMap(name);
		setBorders(min, max);
		setGraphState(graphState);
		setMode(1);
	}

	public ColorGradient(Color[] colors, float min, float max, GraphState graphState) {
		setMap("user");
		USER = new Color[colors.length];
		for (int i = 0; i < colors.length; i++) {
			USER[i] = colors[i];
		}
		setBorders(min, max);
		setGraphState(graphState);
		setMode(1);
	}

	public ColorGradient(String name, float min, float max, GraphState graphState, int mode) {
		setMap(name);
		setBorders(min, max);
		setGraphState(graphState);
		setMode(mode);
	}

	public ColorGradient(String name, GraphState graphState, int mode) {
		setMap(name);
		setBorders(min, max);
		setGraphState(graphState);
		setMode(mode);
	}

	public ColorGradient(Color[] colors, float min, float max, GraphState graphState, int mode) {
		setMap("user");
		USER = new Color[colors.length];
		for (int i = 0; i < colors.length; i++) {
			USER[i] = colors[i];
		}
		setBorders(min, max);
		setGraphState(graphState);
		setMode(mode);
	}

	public ColorGradient(String name, float min, float max) {
		setMap(name);
		setBorders(min, max);
	}

	public ColorGradient(String name) {
		setMap(name);
		setBorders(min, max);
	}

	public ColorGradient(Color[] colors, float min, float max) {
		setMap("user");
		USER = new Color[colors.length];
		for (int i = 0; i < colors.length; i++) {
			USER[i] = colors[i];
		}
		setBorders(min, max);
	}

	public void setMap(String name) {
		this.name = name.toLowerCase();
	}

	public void setBorders(float min, float max) {
		if (min < max) {
			this.min = min;
			this.max = max;
		} else {
			this.min = max;
			this.max = min;
		}
	}

	// r,g,b in [0,255]
	public float[] getColorRGB(float value) {
		int argb = getColor(value);
		return new float[] { (argb >> 16) & 0xff, (argb >> 8) & 0xff, (argb) & 0xff };
		// Color c = new Color(getColor(value), true);
		// return float[] { c.getRed(), c.getGreen(), c.getBlue() };
	}

	public int getColor(float value) {
		if (value > max)
			value = max;
		if (value < min)
			value = min;
		float m1 = 0;
		float m2 = 0;
		float v2 = 0;
		if (min >= 0 && max > 0) {
			m1 = min;
			m2 = max - m1;
			v2 = (value - m1) / m2;
		}
		if (min < 0) {
			m1 = Math.abs(min);
			m2 = max + m1;
			v2 = (value + m1) / m2;
		}

		switch (this.name) {
		case "hsv":
			// 0-360°
			return Color.HSBtoRGB(v2, 1, 1);
		case "jet":
			// 300-0°
			return Color.HSBtoRGB((float) (300 / 360.0 - (300 / 360.0 * v2)), 1, 1);
		case "cool":
			// 180-315°
			return Color.HSBtoRGB((float) (0.5 + (135 / 360.0 * v2)), 1, 1);
		case "summer":
			// 120-60°
			return Color.HSBtoRGB((float) (120 / 360f - (60 / 360.0 * v2)), 1, 1);
		case "autumn":
			// 0-60°
			return Color.HSBtoRGB((float) ((60 / 360.0 * v2)), 1, 1);
		case "winter":
			// 240-120°
			return Color.HSBtoRGB((float) (240 / 360f - (120 / 360.0 * v2)), 1, 1);
		case "groimp":
			// 120-360°
			return Color.HSBtoRGB((float) (120 / 360f + (240 / 360.0 * v2)), 1, 1);
		case "gray":
			return Color.HSBtoRGB(0, 0, v2);
		case "heat":
			return getInterpolatedColor(HEAT, v2);
		case "yignbn":
			return getInterpolatedColor(YIGNBN, v2);
		case "lemon":
			return getInterpolatedColor(LEMON, v2);
		case "yiorrd":
			return getInterpolatedColor(YIORRD, v2);
		case "bluered":
			return getInterpolatedColor(BLUERED, v2);
		case "rdbu":
			return getInterpolatedColor(RDBU, v2);
		case "picnic":
			return getInterpolatedColor(PICNIC, v2);
		case "portland":
			return getInterpolatedColor(PORTLAND, v2);
		case "jet2":
			return getInterpolatedColor(JET, v2);
		case "hot":
			return getInterpolatedColor(HOT, v2);
		case "blackbody":
			return getInterpolatedColor(BLACKBODY, v2);
		case "earth":
			return getInterpolatedColor(EARTH, v2);
		case "electric":
			return getInterpolatedColor(ELECTRIC, v2);
		case "harmonic":
			return getInterpolatedColor(HARMONIC, v2);
		case "wei":
			return getInterpolatedColor(WEI, v2);
		case "wei2":
			return getInterpolatedColor(WEI2, v2);
		case "purple":
			return getInterpolatedColor(PURPLE, v2);
		case "user":
			return getInterpolatedColor(USER, v2);
		default:
			return 0x000000;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getMin() {
		return min;
	}

	public void setMin(float min) {
		setBorders(min, max);
	}

	public float getMax() {
		return max;
	}

	public void setMax(float max) {
		setBorders(min, max);
	}

	private boolean isLinL = false;

	public void isLinL(boolean value) {
		isLinL = value;
	}

	// D65 reference white point:
	private final static double Xref = 0.950456;
	private final static double Yref = 1.000000;
	private final static double Zref = 1.088754;

	public static float[] lab2rgb(float[] Lab) {
		double ll = (Lab[0] + 16.0) / 116.0;
		float Y65 = (float) (Yref * f2(ll));
		float X65 = (float) (Xref * f2(ll + Lab[1] / 500.0));
		float Z65 = (float) (Zref * f2(ll - Lab[2] / 200.0));

		// XYZ -> RGB (linear components)
		double r = (3.240479 * X65 + -1.537150 * Y65 + -0.498535 * Z65);
		double g = (-0.969256 * X65 + 1.875992 * Y65 + 0.041556 * Z65);
		double b = (0.055648 * X65 + -0.204043 * Y65 + 1.057311 * Z65);

		// RGB -> sRGB (nonlinear components)
		float rr = (float) gammaFwd(r);
		float gg = (float) gammaFwd(g);
		float bb = (float) gammaFwd(b);

		return new float[] { rr, gg, bb };
	}

	public static float[] rgb2lab(float[] srgb) {
		// get linear rgb components:
		double r = gammaInv(srgb[0]);
		double g = gammaInv(srgb[1]);
		double b = gammaInv(srgb[2]);

		// convert to XYZ (D65-based, Poynton/ITU709)
		float X = (float) (0.412453 * r + 0.357580 * g + 0.180423 * b);
		float Y = (float) (0.212671 * r + 0.715160 * g + 0.072169 * b);
		float Z = (float) (0.019334 * r + 0.119193 * g + 0.950227 * b);

		double xx = f1(X / Xref);
		double yy = f1(Y / Yref);
		double zz = f1(Z / Zref);
		float L = (float) (116.0 * yy - 16.0);
		float a = (float) (500.0 * (xx - yy));
		float bb = (float) (200.0 * (yy - zz));
		return new float[] { L, a, bb };
	}

	// specs according to official sRGB standard:
	static final double s = 12.92;
	static final double a0 = 0.0031308;
	static final double b0 = s * a0; // 0.040449936
	static final double d = 0.055;
	static final double gamma = 2.4;

	public static double gammaFwd(double lc) { // input: linear RGB component value in [0,1]
		return (lc <= a0) ? (lc * s) : ((1 + d) * Math.pow(lc, 1 / gamma) - d);
	}

	public static double gammaInv(double nc) { // input: nonlinear sRGB component value in [0,1]
		return (nc <= b0) ? (nc / s) : Math.pow((nc + d) / (1 + d), gamma);
	}

	private static final double epsilon = 216.0 / 24389;
	private static final double kappa = 841.0 / 108;

	// Gamma correction for L* (forward)
	private static double f1(double c) {
		if (c > epsilon) // 0.008856
			return Math.cbrt(c);
		else
			return (kappa * c) + (16.0 / 116);
	}

	// Gamma correction for L* (inverse)
	private static double f2(double c) {
		double c3 = c * c * c; // Math.pow(c, 3.0);
		if (c3 > epsilon)
			return c3;
		else
			return (c - 16.0 / 116) / kappa;
	}

	public void setMode(int value) {
		mode = value;
		if (mode < 0)
			mode = 0;
		if (mode > 5)
			mode = 0;
		if (mode == 4)
			isLinL(false);
		if (mode == 5)
			isLinL(true);
	}

	private int getInterpolatedColor(Color[] colors, float fraction) {
		if (mode == 1) {
			return getBSplineInterpolationRGB(colors, fraction);
		}
		if (mode == 2) {
			return getBezierInterpolationRGB(colors, fraction);
		}
		if (mode == 3) {
			return getBSplineInterpolationLab(colors, fraction);
		}

		if (mode == 4 || mode == 5) {
			return getBezierInterpolationLab(colors, fraction);
		}
		if (fraction >= 1)
			fraction = 0.999999f;
		fraction = Math.max(fraction, 0f);
		float rel_fraction = fraction * (colors.length - 1);
		int i = (int) (rel_fraction - rel_fraction % 1);
		float rest = rel_fraction - i;
		return interpolateColor(colors[i], colors[i + 1], rest);

	}

	private int interpolateColor(final Color COLOR1, final Color COLOR2, float fraction) {
		fraction = Math.min(fraction, 1f);
		fraction = Math.max(fraction, 0f);

		int red = Math.round(COLOR1.getRed() + ((COLOR2.getRed() - COLOR1.getRed()) * fraction));
		int green = Math.round(COLOR1.getGreen() + ((COLOR2.getGreen() - COLOR1.getGreen()) * fraction));
		int blue = Math.round(COLOR1.getBlue() + ((COLOR2.getBlue() - COLOR1.getBlue()) * fraction));

		red = (int) Math.min(red, 255f);
		red = (int) Math.max(red, 0f);
		green = (int) Math.min(green, 255f);
		green = (int) Math.max(green, 0f);
		blue = (int) Math.min(blue, 255f);
		blue = (int) Math.max(blue, 0f);

		// return new Color(red, green, blue);
		return 0xff000000 | (red << 16) | (green << 8) | (blue << 0);
	}

	private BSplineCurve bSplineCRGB = null;

	private int getBSplineInterpolationRGB(Color[] colors, float fraction) {
		if (gs == null)
			return 0x000000;

		fraction = Math.min(fraction, 1f);
		fraction = Math.max(fraction, 0f);

		if (bSplineCRGB == null) {
			float[] data = new float[colors.length * 3];
			int i = 0;

			for (Color c : colors) {
				float tmp[] = c.getRGBColorComponents(null);
				data[i] = tmp[0];
				i++;
				data[i] = tmp[1];
				i++;
				data[i] = tmp[2];
				i++;
			}
			bSplineCRGB = new BSplineOfVertices(new VertexListImpl(data, 3), 3, false, false);
		}
		float[] out = new float[3];
		BSpline.evaluate(out, bSplineCRGB, fraction, gs);

		float red = out[0];
		float green = out[1];
		float blue = out[2];

		red = Math.min(red, 1f);
		red = Math.max(red, 0f);
		int redI = Math.round(255 * red);
		green = Math.min(green, 1f);
		green = Math.max(green, 0f);
		int greenI = Math.round(255 * green);
		blue = Math.min(blue, 1f);
		blue = Math.max(blue, 0f);
		int blueI = Math.round(255 * blue);

		// return new Color(red, green, blue);
		return 0xff000000 | (redI << 16) | (greenI << 8) | (blueI << 0);
	}

	private BSplineCurve bSplineCLab = null;

	private int getBSplineInterpolationLab(Color[] colors, float fraction) {
		if (gs == null)
			return 0x000000;

		fraction = Math.min(fraction, 1f);
		fraction = Math.max(fraction, 0f);

		if (bSplineCLab == null) {
			float[] data = new float[colors.length * 3];
			int i = 0;

			for (Color c : colors) {
				float tmp[] = rgb2lab(c.getRGBColorComponents(null));
				data[i] = tmp[0];
				i++;
				data[i] = tmp[1];
				i++;
				data[i] = tmp[2];
				i++;
			}
			bSplineCLab = new BSplineOfVertices(new VertexListImpl(data, 3), 3, false, false);
		}
		float[] out = new float[3];
		BSpline.evaluate(out, bSplineCLab, fraction, gs);

		float[] out3 = lab2rgb(out);
		float red = out3[0];
		float green = out3[1];
		float blue = out3[2];

		red = Math.min(red, 1f);
		red = Math.max(red, 0f);
		int redI = Math.round(255 * red);
		green = Math.min(green, 1f);
		green = Math.max(green, 0f);
		int greenI = Math.round(255 * green);
		blue = Math.min(blue, 1f);
		blue = Math.max(blue, 0f);
		int blueI = Math.round(255 * blue);

		// return new Color(red, green, blue);
		return 0xff000000 | (redI << 16) | (greenI << 8) | (blueI << 0);
	}

	private BSplineCurve bscLab = null;

	private int getBezierInterpolationLab(Color[] colors, float fraction) {
		if (gs == null)
			return 0x000000;

		fraction = Math.min(fraction, 1f);
		fraction = Math.max(fraction, 0f);

		if (bscLab == null) {
			float[] data = new float[colors.length * 3];
			int i = 0;

			for (Color c : colors) {
				float tmp[] = rgb2lab(c.getRGBColorComponents(null));
				data[i] = tmp[0];
				i++;
				data[i] = tmp[1];
				i++;
				data[i] = tmp[2];
				i++;
			}
			bscLab = new BezierCurve(data, 3);
		}
		float[] out = new float[3];
		BSpline.evaluate(out, bscLab, fraction, gs);

		// VertexList profile = new VertexListImpl(data, 3);
		// BSplineCurve bsc2 = new BSplineOfVertices(profile, 3, false, true);
		// float[] out2 = new float[3];
		// BSpline.evaluate(out2, bsc2, fraction, gs);

		// linear interpolation of L between C_min and C_max
		if (isLinL) {
			float[] outMin = new float[3];
			BSpline.evaluate(outMin, bscLab, 0, gs);
			float[] outMax = new float[3];
			BSpline.evaluate(outMax, bscLab, 1, gs);
			out[0] = outMin[0] + fraction * (outMax[0] - outMin[0]);
		}

		float[] out3 = lab2rgb(out);
		float red = out3[0];
		float green = out3[1];
		float blue = out3[2];

		red = Math.min(red, 1f);
		red = Math.max(red, 0f);
		int redI = Math.round(255 * red);
		green = Math.min(green, 1f);
		green = Math.max(green, 0f);
		int greenI = Math.round(255 * green);
		blue = Math.min(blue, 1f);
		blue = Math.max(blue, 0f);
		int blueI = Math.round(255 * blue);

		// return new Color(red, green, blue);
		return 0xff000000 | (redI << 16) | (greenI << 8) | (blueI << 0);
	}

	private BSplineCurve bscRGB = null;

	private int getBezierInterpolationRGB(Color[] colors, float fraction) {
		if (gs == null)
			return 0x000000;

		fraction = Math.min(fraction, 1f);
		fraction = Math.max(fraction, 0f);

		if (bscRGB == null) {
			float[] data = new float[colors.length * 3];
			int i = 0;

			for (Color c : colors) {
				float tmp[] = c.getRGBColorComponents(null);
				data[i] = tmp[0];
				i++;
				data[i] = tmp[1];
				i++;
				data[i] = tmp[2];
				i++;
			}
			bscRGB = new BezierCurve(data, 3);
		}
		float[] out = new float[3];
		BSpline.evaluate(out, bscRGB, fraction, gs);

		float red = out[0];
		float green = out[1];
		float blue = out[2];

		red = Math.min(red, 1f);
		red = Math.max(red, 0f);
		int redI = Math.round(255 * red);
		green = Math.min(green, 1f);
		green = Math.max(green, 0f);
		int greenI = Math.round(255 * green);
		blue = Math.min(blue, 1f);
		blue = Math.max(blue, 0f);
		int blueI = Math.round(255 * blue);

		// return new Color(red, green, blue);
		return 0xff000000 | (redI << 16) | (greenI << 8) | (blueI << 0);
	}

	private GraphState gs = null;

	public void setGraphState(GraphState graphState) {
		gs = graphState;
	}

	static private double Gamma = 0.80;
	static private double IntensityMax = 255;

	
	//Taken from Earl F. Glynn's web page: * <a href=" ">Spectra Lab Report</a > *
	/**
	 * Example
	 * 
	 * 	protected void init () [
	 * 		Axiom ==> 	
	 * 		for(int i:(350:800)) (
	 * 			{int[] tmp = ColorGradient.wavelength2RGB(i);}
	 * 			[Null(0,0.0,0.02*i) Cylinder(0.02, 0.5).(setShader(new RGBAShader(tmp[0]/255f, tmp[1]/255f, tmp[2]/255f))) ]
	 * 		)
	 * 		;
	 * 	]
	 * 
	 */
	public static int[] wavelength2RGB(double wavelength) {
		double factor;
		double Red, Green, Blue;
		if ((wavelength >= 380) && (wavelength < 440)) {
			Red = -(wavelength - 440) / (440 - 380);
			Green = 0.0;
			Blue = 1.0;
		} else if ((wavelength >= 440) && (wavelength < 490)) {
			Red = 0.0;
			Green = (wavelength - 440) / (490 - 440);
			Blue = 1.0;
		} else if ((wavelength >= 490) && (wavelength < 510)) {
			Red = 0.0;
			Green = 1.0;
			Blue = -(wavelength - 510) / (510 - 490);
		} else if ((wavelength >= 510) && (wavelength < 580)) {
			Red = (wavelength - 510) / (580 - 510);
			Green = 1.0;
			Blue = 0.0;
		} else if ((wavelength >= 580) && (wavelength < 645)) {
			Red = 1.0;
			Green = -(wavelength - 645) / (645 - 580);
			Blue = 0.0;
		} else if ((wavelength >= 645) && (wavelength < 781)) {
			Red = 1.0;
			Green = 0.0;
			Blue = 0.0;
		} else {
			Red = 0.0;
			Green = 0.0;
			Blue = 0.0;
		}
		// Let the intensity fall off near the vision limits
		if ((wavelength >= 380) && (wavelength < 420)) {
			factor = 0.3 + 0.7 * (wavelength - 380) / (420 - 380);
		} else if ((wavelength >= 420) && (wavelength < 701)) {
			factor = 1.0;
		} else if ((wavelength >= 701) && (wavelength < 781)) {
			factor = 0.3 + 0.7 * (780 - wavelength) / (780 - 700);
		} else {
			factor = 0.0;
		}
		
		int[] rgb = new int[3];
		// Don't want 0^x = 1 for x <> 0 
		rgb[0] = Red==0.0 ? 0 : (int) Math.round(IntensityMax * Math.pow(Red * factor, Gamma));
		rgb[1] = Green == 0.0 ? 0 : (int) Math.round(IntensityMax * Math.pow(Green * factor, Gamma));
		rgb[2] = Blue == 0.0 ? 0 : (int) Math.round(IntensityMax * Math.pow(Blue * factor, Gamma));
		return rgb;
	}

}
