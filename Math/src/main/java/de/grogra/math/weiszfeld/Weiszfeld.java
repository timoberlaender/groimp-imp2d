package de.grogra.math.weiszfeld;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.vecmath.Point3d;

/**
 * Created by josue on 12/08/15. To get the geometric median for a set of
 * n-dimensional points. Based on the Modified Weiszfeld Method from
 * 
 * @link{http://ie.technion.ac.il/Home/Users/becka/Weiszfeld_review-v3.pdf 
 * described up to the section 6.3
 */
public class Weiszfeld {
	private final List<WeightedPoint3d> points;
	private double epsilon = 0.0005; // maximum permissible permissibleError
	private int maxIterations = 50;

	public Weiszfeld(List<WeightedPoint3d> points) {
		this.points = points;
	}

	public Weiszfeld(List<Point3d> points, float w) {
		this.points = new ArrayList<WeightedPoint3d>(points.size());
		for(Point3d p:points) this.points.add(new WeightedPoint3d(p, w));
	}
	
	public Weiszfeld(List<WeightedPoint3d> points, double epsilon, int maxIterations) {
		this.points = points;
		this.epsilon = epsilon;
		this.maxIterations = maxIterations;
	}
	
	/**
	 * Return epsilon parameter (accuracy)
	 * 
	 * @return epsilon parameter (accuracy)
	 */
	public double getEpsilon() {
		return epsilon;
	}
	
	/**
	 * Set epsilon parameter (accuracy). Should be a small number 0.0 < epsilon
	 * < 0.1
	 * 
	 * @param epsilon
	 *            parameter (accuracy)
	 */
	public void setEpsilon(double epsilon) {
		this.epsilon = epsilon;
	}
	
	public void setMaxIterations(int maxIterations) {
		this.maxIterations = maxIterations;
	}
	
	public WeightedPoint3d getGeometricMedian() {
		// filtering repeated points
		Map<WeightedPoint3d, Double> map = new HashMap<WeightedPoint3d, Double>();
		for (WeightedPoint3d wPoint : points) {
			WeightedPoint3d point = wPoint;
			if (map.containsKey(point)) {
				map.put(point, map.get(point) + wPoint.getWeight());
			} else {
				map.put(point, wPoint.getWeight());
			}
		}

		// anchor points
		List<WeightedPoint3d> aPoints = new ArrayList<WeightedPoint3d>(map.size());
		for (Map.Entry<WeightedPoint3d, Double> entry : map.entrySet()) 
			aPoints.add(new WeightedPoint3d(entry.getKey(), entry.getValue()));


		// choosing starting point
		WeightedPoint3d startPoint = null;
		double mini = Double.POSITIVE_INFINITY;
		for (WeightedPoint3d wPoint : aPoints) {
			double eval = evaluateF(wPoint, aPoints);
			if (eval < mini) {
				mini = eval;
				startPoint = wPoint;
			}
		}

		WeightedPoint3d x = startPoint;
		WeightedPoint3d lastX;
		double error;
		int iterationCounter = 0;
		do {
			lastX = x;
			if (map.containsKey(x)) {
				WeightedPoint3d rj = R(x, aPoints);
				double wj = map.get(x);
				if (rj.length() > wj) {
					x = operatorS(x, wj, rj, aPoints);
				}
			} else {
				x = operatorT(x, aPoints);
			}
			error = WeightedPoint3d.substraction(x, lastX).length();
			iterationCounter++;

			// System.out.println(iterationCounter+"  "+x);
//			System.out.print(x + ", ");
		} while (error > epsilon && iterationCounter < maxIterations);
		/*
		 * Stops whenever the error is less than or equal the permissibleError
		 * or reaches the maximum number of iterations.
		 */

		// lastError = error;
		// numberOfIterations = iterationCounter;
		return x;
	}

	private WeightedPoint3d operatorT(WeightedPoint3d x, List<WeightedPoint3d> aPoints) {
		WeightedPoint3d result = new WeightedPoint3d();

		double weightsSum = 0;
		for (WeightedPoint3d a : aPoints) {
			double w = a.getWeight();
			double curWeight = w / WeightedPoint3d.substraction(x, a).length();
			WeightedPoint3d cur = new WeightedPoint3d(a);
			cur.scale(curWeight);

			weightsSum += curWeight;
			result.add(cur);
		}

		result.scale(1d / weightsSum);
		return result;
	}

	private WeightedPoint3d operatorS(WeightedPoint3d aj, double wj, WeightedPoint3d rj, List<WeightedPoint3d> aPoints) {
		double rjNorm = rj.length();
		WeightedPoint3d dj = new WeightedPoint3d();
		dj.add(rj);
		dj.scale(-1.0 / rjNorm);

		// calculating tj (stepsize) taken from Vardi and Zhang
		double lj = operatorL(aj, aPoints);
		double tj = (rjNorm - wj) / lj;

		dj.scale(tj);
		dj.add(aj);

		return dj;
	}

	private WeightedPoint3d R(WeightedPoint3d aj, List<WeightedPoint3d> aPoints) {
		WeightedPoint3d result = new WeightedPoint3d();

		for (WeightedPoint3d ai : aPoints) {
			if (!ai.equals(aj)) {
				double w = ai.getWeight();
				WeightedPoint3d dif = WeightedPoint3d.substraction(ai, aj);
				double factor = w / dif.length();
				dif.scale(factor);

				result.add(dif);
			}
		}

		return result;
	}

	private double operatorL(WeightedPoint3d aj, List<WeightedPoint3d> aPoints) {
		double res = 0;
		for (WeightedPoint3d ai : aPoints) {
			if (!aj.equals(ai)) {
				WeightedPoint3d dif = WeightedPoint3d.substraction(aj, ai);
				res += ai.getWeight() / dif.length();
			}
		}
		return res;
	}

	/**
	 * Evaluating the objective function in a given point x
	 * 
	 * @param x
	 *            WeightedPoint3d to evaluate the function.
	 * @param aPoints
	 *            List of weighted points.
	 * @return
	 */
	private double evaluateF(WeightedPoint3d x, List<WeightedPoint3d> aPoints) {
		double res = 0;
		for (WeightedPoint3d ai : aPoints) {
			res += ai.getWeight() * WeightedPoint3d.substraction(ai, x).length();
		}
		return res;
	}

}
