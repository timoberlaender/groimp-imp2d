module groPhysics {
	exports uk.ac.soton.grophysics;

	requires graph;
	requires imp;
	requires imp3d;
	requires math;
	requires rgg;
	requires utilities;
	requires vecmath;
	requires xl.core;
	requires java.desktop;
	requires jbullet;
}