package de.grogra.ray.debug3d;

import java.io.BufferedWriter;

public interface ExportableToVRML {

	public void exportToVRML(BufferedWriter writer);
	
}
