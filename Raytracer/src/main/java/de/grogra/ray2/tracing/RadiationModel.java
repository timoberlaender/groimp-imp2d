/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.ray2.tracing;

import javax.vecmath.Color3f;
import javax.vecmath.Vector3f;

import de.grogra.ray.physics.Collector;
import de.grogra.ray.physics.Spectrum;
import de.grogra.ray2.ProgressMonitor;
import de.grogra.vecmath.geom.Volume;
import de.grogra.xl.util.IntList;
import de.grogra.xl.util.ObjectList;

/**
 * Abstract base class for radiation model. 
 * 
 * @author Ole Kniemeyer
 *
 */
public abstract class RadiationModel implements Cloneable
{
	/**
	 * This field is set in the constructor to a completely black spectrum.
	 */
	final Spectrum black;
	final ObjectList<Spectrum> blackD;

	/**
	 * For each volume group, the number of rays which hits its surface
	 * is stored in this array using the index of the volume group.
	 * 
	 * @see #idToGroup
	 */
	final IntList hitCounterSum;
	final ObjectList<IntList> hitCounterSumD;
	
	/**
	 * For each volume group, the radiant power which is received by its surface
	 * is stored in this array using the index of the volume group.
	 * 
	 * @see #idToGroup
	 */
	final ObjectList<Spectrum> radiantPowerSum;
	final ObjectList<ObjectList<Spectrum>> radiantPowerSumD;

	/**
	 * For each volume group, the radiant power which is transmitted by its surface
	 * is stored in this array using the index of the volume group.
	 * 
	 * @see #idToGroup
	 */
	final ObjectList<Spectrum> transmittedPowerSum;
	final ObjectList<ObjectList<Spectrum>> transmittedPowerSumD;
	
	/**
	 * For each volume group, the radiant power which is absorbed by its surface
	 * is stored in this array using the index of the volume group.
	 * 
	 * @see #idToGroup
	 */
	final ObjectList<Spectrum> absorbedPowerSum;
	final ObjectList<ObjectList<Spectrum>> absorbedPowerSumD;

	/**
	 * For each volume group, the radiant power which is reflected by its surface
	 * is stored in this array using the index of the volume group.
	 * 
	 * @see #idToGroup
	 */
	final ObjectList<Spectrum> reflectedPowerSum;
	final ObjectList<ObjectList<Spectrum>> reflectedPowerSumD;

	
	/**
	 * For each volume group, the irradiance which is sensed by its surface
	 * is stored in this array using the index of the volume group.
	 * 
	 * @see #idToGroup
	 */
	final ObjectList<Spectrum> sensedIrradianceSum;
	final ObjectList<ObjectList<Spectrum>> sensedIrradianceSumD;

	/**
	 * Specifies the grouping of volumes into groups. For each volume, the
	 * group to which it belongs is given by the value of this array when
	 * indexed with the ID of the volume.
	 * 
	 * @see Volume#getId()
	 */
	final int[] idToGroup;

	public class RayPoint
	{
		public Vector3f point;
		public Color3f color;		
	}

	final ObjectList<ObjectList<RayPoint>> tracedRays;

	/**
	 * Create a new radiation model that
	 * adds collected radiation values to the lists.
	 * 
	 * @param spectrumFactory factory to create spectra
	 * @param radiantPowerSum list to which absorbed radiant powers are added
	 * @param absorbedPowerSum list to which received radiant powers are added
	 * @param reflectedPowerSum list to which reflected radiant powers are added
	 * @param transmittedPowerSum list to which transmitted radiant powers are added
	 * @param sensedIrradianceSum list to which sensed irradiances are added
	 * @param idToGroup mapping from volume id to group index
	 * @param rays for debugging purposes, the traced rays will be added to the list. Use null if this is not needed
	 * @param hit counter list to which the number of hitting rays are added
	 */
	public RadiationModel (Spectrum spectrumFactory, 
			ObjectList<Spectrum> radiantPowerSum, 
			ObjectList<Spectrum> absorbedPowerSum,
			ObjectList<Spectrum> reflectedPowerSum, 
			ObjectList<Spectrum> transmittedPowerSum, 
			ObjectList<Spectrum> sensedIrradianceSum, 
			int[] idToGroup, 
			ObjectList<ObjectList<RayPoint>> rays, 
			IntList hitCounterSum)
	{
		this.black = spectrumFactory.newInstance ();
		this.black.setZero ();
		this.radiantPowerSum = radiantPowerSum;
		this.absorbedPowerSum = absorbedPowerSum;
		this.reflectedPowerSum = reflectedPowerSum;
		this.transmittedPowerSum = transmittedPowerSum;
		this.sensedIrradianceSum = sensedIrradianceSum;
		this.idToGroup = idToGroup;
		this.tracedRays = rays;
		this.hitCounterSum = hitCounterSum;
		
		blackD = null;
		radiantPowerSumD = null;
		absorbedPowerSumD = null;
		reflectedPowerSumD = null;
		transmittedPowerSumD = null;
		sensedIrradianceSumD = null;
		hitCounterSumD = null;
	}

	public RadiationModel (Spectrum spectrumFactory, 
			ObjectList<ObjectList<Spectrum>> radiantPowerSumD, 
			ObjectList<ObjectList<Spectrum>> absorbedPowerSumD,
			ObjectList<ObjectList<Spectrum>> reflectedPowerSumD, 
			ObjectList<ObjectList<Spectrum>> transmittedPowerSumD, 
			ObjectList<ObjectList<Spectrum>> sensedIrradianceSumD, 
			int[] idToGroup, 
			ObjectList<ObjectList<RayPoint>> rays, 
			ObjectList<IntList> hitCounterSumD)
	{
		black = spectrumFactory.newInstance ();
		black.setZero ();
		this.blackD = new ObjectList<Spectrum>();
		for(int i=0; i<radiantPowerSumD.size(); i++) blackD.add(black);

		this.radiantPowerSumD = radiantPowerSumD;
		this.absorbedPowerSumD = absorbedPowerSumD;
		this.reflectedPowerSumD = reflectedPowerSumD;
		this.transmittedPowerSumD = transmittedPowerSumD;
		this.sensedIrradianceSumD = sensedIrradianceSumD;
		this.idToGroup = idToGroup;
		this.tracedRays = rays;
		this.hitCounterSumD = hitCounterSumD;
		
		radiantPowerSum = null;
		absorbedPowerSum = null;
		reflectedPowerSum = null;
		transmittedPowerSum = null;
		sensedIrradianceSum = null;
		hitCounterSum = null;
	}

	static void addAndClear (double factor, ObjectList<Spectrum> add, ObjectList<Spectrum> sum)
	{
		synchronized (sum)
		{
			for (int i = add.size - 1; i >= 0; i--)
			{
				Spectrum s = add.get (i);
				if (s != null)
				{
					s.scale (factor);
					Spectrum t = sum.get (i);
					if (t == null)
					{
						t = s.clone ();
						sum.set (i, t);
					}
					else
					{
						t.add (s);
					}
					s.setZero ();
				}
			}
		}
	}

	static void addAndClear (IntList add, IntList sum)
	{
		synchronized (sum)
		{
			for (int i = add.size - 1; i >= 0; i--)
			{
				int s = add.get (i);
				if (s != 0)
				{
					sum.set (i, s + sum.get (i));
					add.set (i, 0);
				}
			}
		}
	}


	static void addAndClearD (double factor, ObjectList<ObjectList<Spectrum>> add, ObjectList<ObjectList<Spectrum>> sum) {
		synchronized (sum) {
			for (int i = add.size - 1; i >= 0; i--) {
				ObjectList<Spectrum> addD = add.get (i);
				if (addD != null) {
					ObjectList<Spectrum> sumD = sum.get (i);
					if(sumD == null) sumD = new ObjectList<Spectrum>();
					for (int j = addD.size - 1; j >= 0; j--) {
						Spectrum s = addD.get (j);
						if (s != null) {
							s.scale (factor);
							Spectrum t = sumD.get (j);
							if (t == null) {
								t = s.clone ();
							} else {
								t.add (s);
							}
							sumD.set (j, t);
						}
					}
					sum.set(i,  sumD);
				}
			}
		}
	}


	static void addAndClearD (ObjectList<IntList> add, ObjectList<IntList> sum)
	{
		synchronized (sum)
		{
			for (int j = add.size - 1; j >= 0; j--)
			{
				IntList addD = add.get(j);
				IntList sumD = sum.get(j);
				if(sumD == null) sumD = new IntList();
				if(addD != null)
				{
					for (int i = addD.size - 1; i >= 0; i--)
					{
						int s = addD.get (i);
						if (s != 0)
						{
							sumD.set (i, s + sumD.get (i));
							addD.set (i, 0);
						}
					}
					sum.set(j, sumD);
				}
			}
		}
	}


	public abstract void compute (long rayCount, long seed, ProgressMonitor progress, int depth, double minPower);

	/**
	 * Returns the number of rays which is hit the surface
	 * of the volume of the given <code>node</code>. If the <code>node</code>
	 * does not define a volume, the zero is returned.
	 * 
	 * @param node a node of the graph
	 * @return the number of rays which is hit the node
	 */
	public int getHitCount (int volumeIndex)
	{
		Integer count = hitCounterSum.get (volumeIndex);
		if (count == null)
		{
			count = 0;
		}
		return count.intValue();
	}
	
	
	/**
	 * Obtain the radiation power that was received by a volume.
	 * @param volumeIndex index into {@link #idToGroup}
	 * @return 
	 */
	public Spectrum getReceivedPower (int volumeIndex)
	{
		Spectrum pwr = radiantPowerSum.get (volumeIndex);
		if (pwr == null)
		{
			pwr = black;
		}
		return pwr;
	}

	/**
	 * Obtain the radiation power that was transmitted by a volume.
	 * @param volumeIndex index into {@link #idToGroup}
	 * @return 
	 */
	public Spectrum getTransmittedPower (int volumeIndex)
	{
		Spectrum pwr = transmittedPowerSum.get (volumeIndex);
		if (pwr == null)
		{
			pwr = black;
		}
		return pwr;
	}
	
	/**
	 * Obtain the radiation power that was absorbed by a volume.
	 * @param volumeIndex index into {@link #idToGroup}
	 * @return 
	 */
	public Spectrum getAbsorbedPower (int volumeIndex)
	{
		Spectrum pwr = absorbedPowerSum.get (volumeIndex);
		if (pwr == null)
		{
			pwr = black;
		}
		return pwr;
	}

	/**
	 * Obtain the radiation power that was reflected by a volume.
	 * @param volumeIndex index into {@link #idToGroup}
	 * @return 
	 */
	public Spectrum getReflectedPower (int volumeIndex)
	{
		Spectrum pwr = reflectedPowerSum.get (volumeIndex);
		if (pwr == null)
		{
			pwr = black;
		}
		return pwr;
	}

	/**
	 * Obtain the irradiance that was sensed by a volume.
	 * @param volumeIndex index into {@link #idToGroup}
	 * @return
	 */
	public Spectrum getSensedIrradiance (int volumeIndex)
	{
		Spectrum pwr = sensedIrradianceSum.get (volumeIndex);
		if (pwr == null)
		{
			pwr = black;
		}
		return pwr;
	}


	/**
	 * Returns the number of rays which is hit the surface
	 * of the volume of the given <code>node</code>. If the <code>node</code>
	 * does not define a volume, the zero is returned.
	 * 
	 * @param node a node of the graph
	 * @return the number of rays which is hit the node
	 */
	public int[] getHitCountD (int volumeIndex)
	{
		IntList count = hitCounterSumD.get (volumeIndex);
		if (count == null)
		{
			count = new IntList(hitCounterSumD.size());
		}
		return count.toArray();
	}


	/**
	 * Obtain the radiation power that was received by a volume.
	 * @param volumeIndex index into {@link #idToGroup}
	 * @return 
	 */
	public Spectrum[] getReceivedPowerD (int volumeIndex)
	{
		ObjectList<Spectrum> pwr = radiantPowerSumD.get (volumeIndex);
		if (pwr == null)
		{
			pwr = blackD;
		}
		Spectrum[] s = new Spectrum[pwr.size()];
		return pwr.toArray(s);
	}

	/**
	 * Obtain the radiation power that was transmitted by a volume.
	 * @param volumeIndex index into {@link #idToGroup}
	 * @return 
	 */
	public Spectrum[] getTransmittedPowerD (int volumeIndex)
	{
		ObjectList<Spectrum> pwr = transmittedPowerSumD.get (volumeIndex);
		if (pwr == null)
		{
			pwr = blackD;
		}
		Spectrum[] s = new Spectrum[pwr.size()];
		return pwr.toArray(s);
	}
	
	/**
	 * Obtain the radiation power that was absorbed by a volume.
	 * @param volumeIndex index into {@link #idToGroup}
	 * @return 
	 */
	public Spectrum[] getAbsorbedPowerD (int volumeIndex)
	{
		ObjectList<Spectrum> pwr = absorbedPowerSumD.get (volumeIndex);
		if (pwr == null)
		{
			pwr = blackD;
		}
		Spectrum[] s = new Spectrum[pwr.size()];
		return pwr.toArray(s);
	}

	/**
	 * Obtain the radiation power that was reflected by a volume.
	 * @param volumeIndex index into {@link #idToGroup}
	 * @return 
	 */
	public Spectrum[] getReflectedPowerD (int volumeIndex)
	{
		ObjectList<Spectrum> pwr = reflectedPowerSumD.get (volumeIndex);
		if (pwr == null)
		{
			pwr = blackD;
		}
		Spectrum[] s = new Spectrum[pwr.size()];
		return pwr.toArray(s);
	}

	/**
	 * Obtain the irradiance that was sensed by a volume.
	 * @param volumeIndex index into {@link #idToGroup}
	 * @return
	 */
	public Spectrum[] getSensedIrradianceD (int volumeIndex)
	{
		ObjectList<Spectrum> pwr = sensedIrradianceSumD.get (volumeIndex);
		if (pwr == null)
		{
			pwr = blackD;
		}
		Spectrum[] s = new Spectrum[pwr.size()];
		return pwr.toArray(s);
	}


	public Collector getSensedIrradianceCollector(int volumeIndex) 
	{
		Spectrum col = sensedIrradianceSum.get (volumeIndex);
		
		if (!(col instanceof Collector))
		{
			col = black;
		}
		
		return (Collector) col;
	}


	public Collector getAbsorbedPowerCollector(int volumeIndex) 
	{
		Spectrum col = absorbedPowerSum.get (volumeIndex);
		
		if (!(col instanceof Collector))
		{
			col = black;
		}
		
		return (Collector) col;
	}

	public ObjectList<ObjectList<RadiationModel.RayPoint>> getTracedRays()
	{
		return tracedRays;
	}
}
