/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.ray2.tracing;

import java.util.ArrayList;
import java.util.Random;

import javax.vecmath.Color4f;
import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;

import de.grogra.ray.physics.Environment;
import de.grogra.ray.physics.Spectrum;
import de.grogra.ray.util.Ray;
import de.grogra.ray2.Scene;
import de.grogra.ray2.light.LightProcessor;
import de.grogra.vecmath.geom.Intersection;
import de.grogra.vecmath.geom.IntersectionList;
import de.grogra.vecmath.geom.Line;
import net.goui.util.MTRandom;

public abstract class RayProcessorBase extends ProcessorBase implements
		RayProcessor
{
	public static final String RECURSION_DEPTH = "rayprocessor.depth";

	int maxDepth = 5;

	protected PixelwiseRenderer renderer;
	LightProcessor lightProcessor = null;

	public /*protected*/ Scene scene;

	IntersectionList ilist = new IntersectionList ();
	Tuple3d tmpColor = new Point3d ();
	protected Tuple3d sumColor = new Point3d ();

	public EnteredSolidsList enteredSolids = new EnteredSolidsList ();

	long primaryCount = 0;

	public void setLightProcessor (LightProcessor proc)
	{
		lightProcessor = proc;
	}

	public LightProcessor getLightProcessor(){
		return lightProcessor;
	}
	
	public void setRecursionDepth (int value)
	{
		if (value >= 0)
		{
			maxDepth = value;
		}
	}

	@Override
	public RayProcessor dup (Scene scene)
	{
		RayProcessorBase p = (RayProcessorBase) clone ();
		p.lightProcessor = lightProcessor.dup (scene);
		p.scene = scene;
		p.initLocals ();
		return p;
	}

	@Override
	protected void mergeStatistics (ProcessorBase src)
	{
		primaryCount += ((RayProcessorBase) src).primaryCount;
	}

	@Override
	protected void initLocals ()
	{
		ilist = new IntersectionList ();
		enteredSolids = new EnteredSolidsList ();
		locals = new Locals ();
		tmpColor = new Point3d ();
		sumColor = new Point3d ();
	}

	@Override
	public void initialize (PixelwiseRenderer renderer, Scene scene)
	{
		this.renderer = renderer;
		this.scene = scene;
		initLocals ();
		lightProcessor.initialize (scene, getEnvironmentType (), new MTRandom (renderer.getSeed ()));
		setRecursionDepth (Math.min (Math.max (renderer.getNumericOption (
			RECURSION_DEPTH, maxDepth).intValue (), 0), 10));
	}


	@Override
	public void initializeBeforeTracing (Random random){
		// to be overwritten
	}


	Locals locals;

	public double getIOR (Intersection is, Spectrum spec)
	{
		return enteredSolids.getIOR(scene, is , spec);
	}

	float getBrightness ()
	{
		return 1;
	}

	@Override
	public void getColorFromRay (Line ray, Spectrum resp, Color4f color, Random random)
	{
		primaryCount++;
		enteredSolids.clear ();
		ilist.clear ();

		scene.computeIntersections (ray, Intersection.CLOSEST, ilist, null,
			null);
		if (ilist.size == 0)
		{
			// no intersection for this ray -> return transparency
			color.set (0, 0, 0, 0);
			return;
		}

		Intersection is = ilist.elements[0];

		sumColor.set (0, 0, 0);

		// calculate color recursively
		float transparency = traceRay (1, is, resp, sumColor,
			locals.nextReflected (), random);

		sumColor.scale (getBrightness ());
		color.x = (float) sumColor.x;
		color.y = (float) sumColor.y;
		color.z = (float) sumColor.z;
		color.w = (transparency < 1) ? 1 - transparency : 0;

	}

	public class Locals
	{
		public Locals nextTransmitted;
		public Locals nextReflected;

		public Spectrum newWeight = scene.createSpectrum ();
		public Ray reflected = new Ray (newWeight);
		public Ray transmitted = new Ray (newWeight);
		public Spectrum tmpSpectrum = scene.createSpectrum ();
		public Line tmpRay = new Line ();

		public Environment env = new Environment (scene.getBoundingBox (), newWeight, getEnvironmentType ());

		public ArrayList lightCache = new ArrayList ();

		public Locals ()
		{
			tmpRay.start = 0;
			tmpRay.end = Double.POSITIVE_INFINITY;
		}

		public Locals nextTransmitted ()
		{
			if (nextTransmitted == null)
			{
				nextTransmitted = new Locals ();
			}
			return nextTransmitted;
		}

		public Locals nextReflected ()
		{
			if (nextReflected == null)
			{
				nextReflected = new Locals ();
			}
			return nextReflected;
		}
	}
	
	abstract int getEnvironmentType ();

	abstract float traceRay (int depth, Intersection desc, Spectrum weight,
			Tuple3d color, Locals loc, Random random);

}
