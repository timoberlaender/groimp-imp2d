# Raytracer

Raytracer is used by IMP-3D to integrate the raytracer Twilight, which is a subproject of the whole GroIMP project but independent of the GroIMP application. This raytracer implements both a conventional ray-tracer and a path-tracer based on _Robust Monte Carlo Methods for Light Transport Simulation [E. Veach.]_. For the representation of geometry, it relies on the plugin Vecmath. Its algorithms are also used for the radiation model.
