package de.grogra.pf.registry;

public class PluginExclude extends Exclude
{
//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (new PluginExclude ());
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new PluginExclude ();
	}

//enh:end


	PluginExclude ()
	{
		super ();
	}


	@Override
	protected boolean readAttribute (String uri, String name, String value)
		throws org.xml.sax.SAXException
	{
		if ("".equals (uri))
		{
			if ("plugin".equals (name))
			{
				setName (value);
				return true;		
			}
		}
		return super.readAttribute (uri, name, value);
	}


	@Override
	public boolean isFulfilled ()
	{
		throw new AssertionError ();
	}


	@Override
	public void addMessage (StringBuffer b, String plugin)
	{
		b.append (de.grogra.pf.boot.Main.getI18NBundle ().msg
				  ("plugin.conflict", getName (), plugin));
	}

}
