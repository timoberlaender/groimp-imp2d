package de.grogra.pf.registry.expr;

import java.nio.file.Path;

import de.grogra.pf.registry.*;
import de.grogra.util.StringMap;
import de.grogra.util.Utils;

/**
 * A registry item that checks the boolean value of an option. If the value is 
 * true, the content of its parent registry item is resolved when activated.
 * Its take a default "value" and an option path "optPath" as parameters.
 */
public final class BooleanValueOption extends Expression
{
	private boolean value=false;
	//enh:field
	
	private String optPath;
	//enh:field

//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field value$FIELD;
	public static final NType.Field optPath$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (BooleanValueOption.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		public void setBoolean (Object o, boolean value)
		{
			switch (id)
			{
				case 0:
					((BooleanValueOption) o).value = (boolean) value;
					return;
			}
			super.setBoolean (o, value);
		}

		@Override
		public boolean getBoolean (Object o)
		{
			switch (id)
			{
				case 0:
					return ((BooleanValueOption) o).value;
			}
			return super.getBoolean (o);
		}

		@Override
		protected void setObjectImpl (Object o, Object value)
		{
			switch (id)
			{
				case 1:
					((BooleanValueOption) o).optPath = (String) value;
					return;
			}
			super.setObjectImpl (o, value);
		}

		@Override
		public Object getObject (Object o)
		{
			switch (id)
			{
				case 1:
					return ((BooleanValueOption) o).optPath;
			}
			return super.getObject (o);
		}
	}

	static
	{
		$TYPE = new NType (new BooleanValueOption ());
		$TYPE.addManagedField (value$FIELD = new _Field ("value", _Field.PRIVATE  | _Field.SCO, de.grogra.reflect.Type.BOOLEAN, null, 0));
		$TYPE.addManagedField (optPath$FIELD = new _Field ("optPath", _Field.PRIVATE  | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (String.class), null, 1));
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new BooleanValueOption ();
	}

//enh:end

	public BooleanValueOption ()
	{
		super (null);
	}


	public BooleanValueOption (String name, boolean value, String optPath)
	{
		super (name);
		this.value = value;
		this.optPath = optPath;
	}


	@Override
	public Object evaluate (RegistryContext ctx, StringMap args)
	{
		String p = optPath.substring(0, optPath.lastIndexOf('/')) ;
		String c = optPath.substring(optPath.lastIndexOf('/')+1);
		Item opt = Item.resolveItem (ctx, p);
		return (opt!=null)? Boolean.valueOf (Utils.getBoolean (
				opt, c, value)):value;
	}

}
