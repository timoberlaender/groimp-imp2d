package de.grogra.pf.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Map.Entry;
import java.util.TreeMap;

import de.grogra.pf.registry.Registry;
import de.grogra.util.MimeType;
import de.grogra.util.ModifiableMap;

public class PreferencesFile extends FileSource {
	
	TreeMap<String, String> preferencesMap = new TreeMap<String, String>();
	TreeMap<String, String> readMap = new TreeMap<String, String>();
	
	public PreferencesFile(File file, MimeType mimeType, Registry reg, ModifiableMap metaData) {
		super(file, mimeType, reg, metaData);
	}


	public PreferencesFile(File file, Registry reg, ModifiableMap m) {
		super(file, MimeType.TEXT_PLAIN, reg, m);
	}


    public void loadProperties() throws IOException {
        final int lhs = 0;
        final int rhs = 1;
        
        Object f = getFile();
        if (f != null && f instanceof File) {
	        BufferedReader  bfr = new BufferedReader(new FileReader((File)f));
	
	        String line;
	        while ((line = bfr.readLine()) != null) {
	            if (!line.startsWith("#") && !line.isEmpty()) {
	                String[] pair = line.trim().split("=");
	                preferencesMap.put(pair[lhs].trim(), pair[rhs].trim());
	            }
	        }
	        bfr.close();
	        readMap = (TreeMap<String, String>) preferencesMap.clone();
        }
    }
    
    /**
     * Preferences are only get once, then they are set to global pref.
     * @param name
     * @return
     */
    public String getPreference(String name) {
    	String value = readMap.get(name);
    	if (value != null ) {
    		readMap.remove(name);
    	}
		return value;
    }
    
    /**
     * Return the value of a preference without removing it from the set.
     * @param name
     * @return
     */
    public String peekAt(String name) {
    	return readMap.get(name);
    }
    
    
    /**
     * Add a option value. If the option is already referred in the property file change its value instead.
     */
    public void writeOption(String opt, String val) {
    	if (preferencesMap.containsKey(opt) && !preferencesMap.get(opt).equals(val)) {
    		preferencesMap.replace(opt, val);
    		StringBuffer sb = new StringBuffer();
    		for (Entry<String, String> e : preferencesMap.entrySet()) {
    			sb.append(e.getKey() + "=" + e.getValue() + System.lineSeparator());
    		}
    		try (BufferedWriter w = new BufferedWriter(new FileWriter((File)getFile()))){
        		w.write(sb.toString());
    		} catch(IOException e) {
    		} 
    	} else {
    		try (BufferedWriter w = new BufferedWriter(new FileWriter((File)getFile(), true))) {
        		w.write(System.lineSeparator() + opt + "=" +val);
    		} catch(IOException e) {
    		} 
    	}
    }
}
