# IMP-3D plugin 

IMP-3D plays a central role in the management of GroIMP's object in the scene. It defines node classes for all kinds of 3D geometry from primitives like points, spheres and boxes to complex objects like NURBS surfaces and polygon meshes. Cameras, light sources and sky objects are defined, and physically valid shaders can be constructed from procedural textures and images. This plug-in also provides a 3D view based on wireframe, and manipulation tools for translation, rotation and scaling.
