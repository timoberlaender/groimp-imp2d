package de.grogra.imp3d;

import de.grogra.imp.viewhandler.ViewEventFactory;
import de.grogra.imp.viewhandler.EmptyHighlighter;
import de.grogra.imp.viewhandler.SelectionEventFactory;
import de.grogra.imp.viewhandler.ToolEventFactory;
import de.grogra.persistence.SCOType;

public class NoHighlightView3DEventFactory extends ViewEventFactory{

	//enh:sco SCOType
	
		public NoHighlightView3DEventFactory() {
			this.navigatorFactory = new Navigator3DFactory();
			this.toolFactory = new ToolEventFactory();
			this.selectionFactory = new SelectionEventFactory();
			this.highlighter = new EmptyHighlighter();
			
			setPickVisitor(new PickRayVisitor ());
			setPickToolVisitor(new PickToolVisitor ());
		}
			
		//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;


	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (NoHighlightView3DEventFactory representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}


		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		public Object newInstance ()
		{
			return new NoHighlightView3DEventFactory ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (NoHighlightView3DEventFactory.class);
		$TYPE.validate ();
	}

//enh:end
}
