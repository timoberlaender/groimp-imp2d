module platform {
	exports de.grogra.pf.ui;
	exports de.grogra.msml;
	exports de.grogra.pf.ui.tree;
	exports de.grogra.pf.ui.registry;
	exports de.grogra.pf.ui.util;
	exports de.grogra.pf.ui.event;
	exports de.grogra.pf.ui.edit;
	exports de.grogra.pf.data;
	exports de.grogra.pf.ui.projectexplorer;

	// modification from jfree 1.5.3
	exports org.jfree.data;
	exports org.jfree.data.category;
	exports org.jfree.data.flow;
	exports org.jfree.data.function;
	exports org.jfree.data.gantt;
	exports org.jfree.data.general;
	exports org.jfree.data.io;
	exports org.jfree.data.json;
	exports org.jfree.data.json.impl;
	exports org.jfree.data.resources;
	exports org.jfree.data.statistics;
	exports org.jfree.data.time;
	exports org.jfree.data.time.ohlc;
	exports org.jfree.data.xml;
	exports org.jfree.data.xy;
	
	exports org.jfree.chart;
	exports org.jfree.chart.annotations;
	exports org.jfree.chart.axis;
	exports org.jfree.chart.block;
	exports org.jfree.chart.date;
	exports org.jfree.chart.editor;
	exports org.jfree.chart.encoders;
	exports org.jfree.chart.entity;
	exports org.jfree.chart.event;
	exports org.jfree.chart.imagemap;
	exports org.jfree.chart.labels;
	exports org.jfree.chart.needle;
	exports org.jfree.chart.panel;
	exports org.jfree.chart.plot;
	exports org.jfree.chart.plot.dial;
	exports org.jfree.chart.plot.flow;
	exports org.jfree.chart.renderer;
	exports org.jfree.chart.renderer.xy;
	exports org.jfree.chart.renderer.category;
	exports org.jfree.chart.resources;
	exports org.jfree.chart.text;
	exports org.jfree.chart.title;
	exports org.jfree.chart.ui;
	exports org.jfree.chart.urls;
	exports org.jfree.chart.util;

//	exports org.jfree.chart3d;
//	exports org.jfree.chart3d.axis;
//	exports org.jfree.chart3d.data;
//	exports org.jfree.chart3d.data.category;
//	exports org.jfree.chart3d.data.function;
//	exports org.jfree.chart3d.data.xyz;
//	exports org.jfree.chart3d.export;
//	exports org.jfree.chart3d.graphics2d;
//	exports org.jfree.chart3d.graphics3d;
//	exports org.jfree.chart3d.graphics3d.internal;
//	exports org.jfree.chart3d.graphics3d.swing;
//	exports org.jfree.chart3d.interaction;
//	exports org.jfree.chart3d.internal;
//	exports org.jfree.chart3d.label;
//	exports org.jfree.chart3d.legend;
//	exports org.jfree.chart3d.marker;
//	exports org.jfree.chart3d.plot;
//	exports org.jfree.chart3d.renderer;
//	exports org.jfree.chart3d.renderer.category;
//	exports org.jfree.chart3d.renderer.xyz;
//	exports org.jfree.chart3d.style;
//	exports org.jfree.chart3d.table;
//	exports org.jfree.chart3d.util.json;
//	exports org.jfree.chart3d.util.json.parser;

	requires graph;
	requires platform.core;
	requires utilities;
	requires xl.core;
	requires serializer;
	requires java.datatransfer;
	requires java.desktop;
	requires java.logging;
	requires java.prefs;
	requires java.sql;
	
	requires org.apache.commons.collections4;
	requires org.apache.commons.codec;
	requires org.apache.commons.compress;
//	requires xbean;
//	requires org.apache.xmlbeans;
	
	// itext7 modules (import from name ...)
	requires layout;
	requires commons;
	requires kernel;
	requires io;
	
	// poi modules (v 4.1.2 isn't module yet)
	requires poi;
	requires poi.ooxml;
	requires poi.ooxml.schemas;
//	(v5.2.3) schemas not working for some reason
//	requires org.apache.poi;
//	requires org.apache.poi.ooxml;
//	requires org.apache.poi.ooxml.schemas;

			
	
	opens de.grogra.pf.ui to utilities;
}