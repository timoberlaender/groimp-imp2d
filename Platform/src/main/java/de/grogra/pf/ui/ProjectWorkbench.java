package de.grogra.pf.ui;

import java.io.File;
import java.io.InputStream;
import de.grogra.graph.impl.Node;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.ItemCriterion;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.registry.CommandItem;
import de.grogra.pf.ui.registry.SourceFile;
import de.grogra.util.Map;
import de.grogra.util.MimeType;

public abstract class ProjectWorkbench extends Workbench implements ProjectContext{
	protected Project project;
	private Object token;
	public ProjectWorkbench(JobManager jm, UIToolkit ui, Map initParams) {
		super(jm, ui, initParams);
	}
	public void setProject(Project project) {
		this.project=project;
		updateName();
	}
	
	@Override
	public Project getProject() {
		return project;
	}
		
	@Override
	public Registry getRegistry() {
		return project.getRegistry();
	}
	
	public static void getProjectInfo(Item item, Object info, Context ctx) {
		((ProjectWorkbench)ctx.getWorkbench()).getProjectInfo(info);		
	}
	
	public abstract void getProjectInfo(Object info);
	
	public static void export(Item item, Object info, Context ctx) throws Exception {
		((ProjectWorkbench)ctx.getWorkbench()).export(info);

	}

	public abstract void export(Object info) throws Exception;

	
	public static void exportFromNode(Item item, Object info, Context ctx) throws Exception {
		((ProjectWorkbench)ctx.getWorkbench()).exportFromNode(info);

	}

	// info is an action edit event for impworkbench
	public abstract void exportFromNode(Object info) throws Exception;
	
	public abstract void exportFromNode(Object info, Node root) throws Exception;

	
	public boolean saveAs(File file, MimeType mimeType) {
		return project.saveAs(file,mimeType);
	}
	
	
	public static void close(Item item, Object info, Context ctx) {
		ctx.getWorkbench().close(null);
	}
	
	public void loadProjectFile(FilterSource fs, Map initParams) {
			project.loadProjectFile(fs, initParams,this);
	}
	
	public static void addNode(Item item, Object info, Context ctx) throws Exception {
		((ProjectWorkbench)ctx.getWorkbench()).addNode(info);
	}
	
	public abstract void addNode(Object info) throws Exception;
	
	public void addNode(Node node) {
		project.addNode(node,this);
	}
		
	public static void addSourceFile(Item item, Object info, Context ctx) throws Exception {
		Workbench.setCurrent(ctx.getWorkbench());
		Registry.setCurrent(ctx.getWorkbench().getRegistry());
		((ProjectWorkbench)ctx.getWorkbench()).addSourceFile(info);
	}
	
	public abstract void addSourceFile(Object info) throws Exception;
	

	public SourceFile addSourceFile(String fileName, MimeType mt, InputStream ins, Object dest){
		return project.addSourceFile(fileName, mt, ins,dest);
	}
	
	public SourceFile addSourceFile (File file, MimeType mt) {
		return project.addSourceFile(file, mt);
	}

	public SourceFile addSourceFile (File file, MimeType mt, Object dest) {
		
		return project.addSourceFile(file, mt,dest);
	}
	
	public static void renameSourceFile(Item item, Object info, Context ctx) throws Exception {
		((ProjectWorkbench)ctx.getWorkbench()).renameSourceFile( info);
	}
	public abstract void renameSourceFile(Object info) throws Exception;
	
	public void renameSourceFile(String file, String newName) {
		SourceFile sf = getFileNode(file);
		if(sf!=null) {
			project.renameFile(sf, newName, this);
		}
	}
	
	public static void removeSourceFile(Item item, Object info, Context ctx) throws Exception {
		((ProjectWorkbench)ctx.getWorkbench()).removeSourceFile( info);
		
	}
	public abstract void removeSourceFile(Object info) throws Exception;
	
	public void removeSourceFile(String name) {
		SourceFile sf =getFileNode(name);
		if(sf!=null) {
			project.removeFile(sf,this);
		}
	}
	
	public static void listFunctions(Item item, Object info, Context ctx) {
		((ProjectWorkbench) ctx.getWorkbench()).listFunctions(info);
	}
	
	public abstract void listFunctions(Object info);
	
	public Command[] getFunctions() {
		return project.listFunctions();
	}
	
	
	
	public static void compile(Item item, Object info, Context ctx) {
		((ProjectWorkbench)ctx.getWorkbench()).compile(info);
	}

	public void compile(Object info) {
		project.compile(this, null);
	}

	public void compile() {
		project.compile(this, null);
	}
	
	public static void execute(Item item, Object info, Context ctx) throws Exception {
		((ProjectWorkbench)ctx.getWorkbench()).execute(info);
	}
	
	public abstract void execute(Object info) throws Exception;
	
	public void execute(String commandName) {
		project.execute(commandName, this);
	}
	
	public void execute(CommandItem command) {
		project.execute(command, this);
	}
			
	protected SourceFile getFileNode(String name) {
		String systemId =IO.toSystemId(getRegistry().getFileSystem(), name);
		Item node = Item.findFirst (getRegistry(), "/project/objects",
	    		new ItemCriterion ()
    		{
    			@Override
    			public boolean isFulfilled (Item item, Object info)
    			{
    				return item.getSystemId()!=null && item.getSystemId().equals(systemId);
    			}
				@Override
				public String getRootDirectory() {return null;}
    		}, systemId, false);
		return (SourceFile)node;
	}
	
	
	public Object getToken() {
		return token;
	}

	public void setToken(Object token) {
		this.token = token;		
	}

	public abstract String getApplicationName();
	public abstract void dispose(Command afterDispose);
	
}
