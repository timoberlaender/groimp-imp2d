package de.grogra.pf.ui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Filter;
import java.util.logging.Level;

import de.grogra.graph.GraphState;
import de.grogra.graph.impl.GraphManager;
import de.grogra.graph.impl.Node;
import de.grogra.pf.io.FileWriterSource;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSourceImpl;
import de.grogra.pf.registry.Directory;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.registry.RegistryContext;
import de.grogra.pf.ui.registry.SourceDirectory;
import de.grogra.pf.ui.registry.SourceFile;
import de.grogra.util.Map;
import de.grogra.util.MimeType;
import de.grogra.vfs.FileSystem;
import de.grogra.xl.util.ObjectList;

/***
 * The abstract Project class holds all information regarding the Project, such
 * as the registry, the file and mimeType and the name of the project This class
 * also holds all functions for any file interaction with the project. Not to be
 * confused with projectGraph hosted in the registry (which is a GraphManager).
 * 
 * @author Tim Oberländer
 *
 */

public abstract class Project implements RegistryContext {
	// state is not garbage collected
	protected GraphState regState;
	private Object id;
	protected Registry registry;
	private File file;
	private MimeType mimeType;
	private String name;
	private boolean started=false;
	
	protected Filter logFilter;
	protected ObjectList pendingLogs=new ObjectList();


	public Project(Registry registry) {
		this.registry = registry;
	}

	public void init() {
		if (started) {
			System.err.println("Project already started");
		}
		started=true;
		initLogger();
	}
	
	public abstract void dispose();
	public Object getId() {
		return this.id;
	}

	public void setId(Object id) {
		this.id = id;
	}

	public void loadProjectFile(FilterSource fs, Map initParams, Context context) {
	}

	/**
	 * defines the name in the local variable as well as in the registry
	 * 
	 * @param name
	 */
	public void setName(String name) {
		getRegistry().setProjectName(name);
		this.name = name;
		updateName();
	}

	public String getName() {
		return (name != null) ? name : null;
	}

	public MimeType getMimeType() {
		return mimeType;
	}

	public void setMimeType(MimeType mimeType) {
		this.mimeType = mimeType;
	}

	public Registry getRegistry() {
		return registry;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	/**
	 * Checks if the file and the mimetype is not null and if GroIMP is able to
	 * write to this file Type The defined file is only used to save changes to the
	 * same location from which the file was opened
	 * 
	 * @param file
	 * @param mimeType
	 */
	public void setFile(File file, MimeType mimeType) {
		if ((file != null) && (mimeType != null)
				&& IOFlavor.REGISTRY.isWritableTo(new IOFlavor(mimeType, IOFlavor.FILE_WRITER, null))) {
			this.file = file;
			this.mimeType = mimeType;
		} else {
			this.file = null;
			this.mimeType = null;
		}
	}

	/**
	 * saves Project to the location from which it was opened
	 * 
	 * @return
	 */
	public boolean save() {
		if (file == null || mimeType == null) {
			return false;
		}

		return save(getRegistry(), file, mimeType);
	}

	/**
	 * Saves file to given location
	 * 
	 * @param file
	 * @param mimeType
	 * @return
	 */

	public boolean saveAs(File file, MimeType mimeType) {
		return save(getRegistry(), file, mimeType);
	}

	/**
	 * Save Object (normally the Registry) to a given location Maybe obsolete in
	 * this design since the object should always be the Registry
	 * 
	 * @param object
	 * @param f
	 * @param mt
	 * @return
	 */

	public boolean save(Object object, File f, MimeType mt) {
		FilterSource fs = IO.createPipeline(
				new ObjectSourceImpl(object, "registry", IOFlavor.valueOf(object.getClass()), getRegistry(), null),
				new IOFlavor(mt, IOFlavor.FILE_WRITER, null));
		if (fs == null) {
			logInfo(IO.I18N.msg("save.unsupported", f, IO.getDescription(mt)));
			return false;
		}
		try {
			// the State is in the original workbench save used to define the last used
			// Panels / the layout
			Item s = new Directory("state");
			getState(s);
			Item c = (Item) s.getBranch();
			if (c != null) {
				s.makeUserItem(true);
				s.setBranch(null);
				s = getRegistry().getDirectory("/workbench/state", null);
				s.setBranch(c);
			}
			((FileWriterSource) fs).write(f);
			setFile(f);
			setMimeType(mt);
			return true;
		} catch (IOException e) {
			logInfo(IO.I18N.msg("saveproject.failed", file), e);
			return false;
		}
	}

	/**
	 * To add a node the used workbench must be added as a parameter to provide a
	 * context that contains a JobManager
	 * 
	 * @param node
	 * @param context
	 */

	public void addNode(Node node, Context context) {
		UI.executeLockedly(getRegistry().getProjectGraph(), true, new Command() {
			@Override
			public String getCommandName() {
				return null;
			}

			@Override
			public void run(Object arg, Context c) {
				((Node) node).setExtentIndex(Node.LAST_EXTENT_INDEX);
				GraphManager g = getRegistry().getProjectGraph();
				g.getRoot().addEdgeBitsTo((Node) node, de.grogra.graph.Graph.BRANCH_EDGE, g.getActiveTransaction());
			}
		}, null, context, JobManager.ACTION_FLAGS);
	}

	public void removeFile(SourceFile sf, Context context) {
		removeNode(sf, context);
	}

	public void removeNode(Node node, Context context) {
		UI.executeLockedly(getRegistry().getProjectGraph(), true, new Command() {
			@Override
			public String getCommandName() {
				return null;
			}

			@Override
			public void run(Object arg, Context c) {
				remove((Item) node);
			}
		}, null, context, JobManager.ACTION_FLAGS);

	}

	private void remove(Item node) {
		if (node.getBranch() == null) {
			node.deactivate();
			node.remove();
		}
	}

	/**
	 * Rename a given Item using the JobManager of the given context
	 * 
	 * @param node
	 * @param newName
	 * @param context
	 */

	public void renameItem(Item node, String newName, Context context) {
		UI.executeLockedly(getRegistry().getProjectGraph(), true, new Command() {
			@Override
			public String getCommandName() {
				return null;
			}

			@Override
			public void run(Object arg, Context c) {
				Node.name$FIELD.setObject(node, null, newName, node.getTransaction());

			}
		}, null, context, JobManager.ACTION_FLAGS);
	}

	public void renameFile(SourceFile file, String newName, Context context) {
		renameItem(file, newName, context);
	}

	
	/**
	 * To add a SourceFile from a stream an empty file is created and the stream is written in it
	 * 
	 * @param fileName
	 * @param mt
	 * @param ins
	 * @param dest
	 * @return
	 */
	
	public SourceFile addSourceFile(String fileName, MimeType mt, InputStream ins, Object des) {
		FileSystem fs = getRegistry().getFileSystem();
		Object dest = fs.getRoot();
		Object destDir = getRegistry().getDirectory("/project/objects/files", null);
		if (des != null) {
			destDir = des;
			if(des instanceof SourceDirectory) {
				dest = getRegistry().getProjectFile((String) ((Item) des).getSystemId());
			}
		}
		
		Object f=null;
		try {
			f = fs.create(dest, fileName, false, true);
			
			 SourceFile file = new SourceFile(fileName, mt, IO.toSystemId(fs, f));
			 if (file != null) {
					((Item) destDir).addUserItem(file);
					file.activate();
					setSourceFileContent(file,ins);
				}

			
			 return file;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public void setSourceFileContent(String fileName, InputStream ins) throws IOException {
		Item i = Item.resolveItem(getRegistry(), "/project/objects/files/"+fileName);
		if(i!=null && i instanceof SourceFile) {
			setSourceFileContent((SourceFile)i,ins);
		}else {
			throw new FileNotFoundException();
		}
	}
	
	
	public void setSourceFileContent(SourceFile file, InputStream ins) throws IOException {
		 FileSystem fs = getRegistry().getFileSystem();
		 Object f =getRegistry().getProjectFile(file.getSystemId());
		 OutputStream fstream = fs.getOutputStream(f,false);
		 fstream.write(ins.readAllBytes());
		 fstream.close();
		 ins.close();
	}
	
	/**
	 * Add a new SourceFile to the Project
	 * 
	 * @param file
	 * @param mt
	 * @return
	 */

	public SourceFile addSourceFile(File file, MimeType mt) {
		return addSourceFile(file, mt, null);
	}

	/**
	 * Add a new sourceFile to the project at a specific destination If no
	 * destination is null the file is added to the default location
	 * 
	 * @param file
	 * @param mt
	 * @param dest
	 * @return
	 */
	public SourceFile addSourceFile(File file, MimeType mt, Object dest) {
		Object destDir = null;
		if (dest != null) {
			destDir = dest;
		} else {
			destDir = getRegistry().getDirectory("/project/objects/files", null);
		}
		SourceFile f = toSourceFile(file, mt, destDir);
		if (f != null) {
			((Item) destDir).addUserItem(f);
			f.activate();
		}
		return f;
	}

	/**
	 * create a sourceFile form a file
	 * 
	 * @param file
	 * @param mt
	 * @param dest
	 * @return
	 */

	public SourceFile toSourceFile(File file, MimeType mt, Object dest) {
		FileSystem fs = getRegistry().getFileSystem();
		Object f;
		if (dest instanceof SourceDirectory)
			dest = getRegistry().getProjectFile((String) ((Item) dest).getSystemId());
		else
			dest = fs.getRoot();
		try {
			if (file.exists()) {
				f = fs.addLocalFile(file, dest, file.getName());
			} else {
				f = fs.create(dest, file.getName(), false, true);
			}
		} catch (IOException e) {
			logInfo(IO.I18N.msg("addfile.failed", file), e);
			return null;
		}
		return new SourceFile(file.getName(), mt, IO.toSystemId(fs, f));
	}

	protected void logInfo(String msg, Throwable e) {
		registry.getLogger().log(Level.WARNING, msg);
	}

	protected void logInfo(String msg) {
		registry.getLogger().log(Level.INFO, msg);

	}
	
	public Filter getLogFilter() {
		return logFilter;
	}
	
	public ObjectList getPendingLogs() {
		return pendingLogs;
	}
	
	public void setLogFilter(Filter newFilter) {
		logFilter=newFilter;
	}
	
	public void setPendingLogs(ObjectList newList) {
		pendingLogs = newList;
	}

	public abstract void initialize(JobManager jm);

	protected abstract void updateName();

	public abstract Command[] listFunctions();

	public abstract void execute(String command, Context ctx);

	public abstract void execute(Command command, Context ctx);

	public abstract void compile(Context context, Command afterCommand);

	protected abstract void getState(Item s);

	public abstract void stopLocalFileSynchronization();

	public abstract void startLocalFileSynchronization();

	protected abstract void initLogger();
}
