/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.pf.ui.registry;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import de.grogra.graph.impl.Node.NType;
import de.grogra.persistence.Manageable;
import de.grogra.pf.io.*;
import de.grogra.pf.registry.*;
import de.grogra.pf.ui.*;
import de.grogra.util.*;
import de.grogra.vfs.FileSystem;
import de.grogra.xl.util.ObjectList;

public class FileFactory extends ObjectItemFactory
{
	String directory;
	//enh:field

	String mimeType;
	//enh:field

	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field directory$FIELD;
	public static final NType.Field mimeType$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (FileFactory.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		protected void setObjectImpl (Object o, Object value)
		{
			switch (id)
			{
				case 0:
					((FileFactory) o).directory = (String) value;
					return;
				case 1:
					((FileFactory) o).mimeType = (String) value;
					return;
			}
			super.setObjectImpl (o, value);
		}

		@Override
		public Object getObject (Object o)
		{
			switch (id)
			{
				case 0:
					return ((FileFactory) o).directory;
				case 1:
					return ((FileFactory) o).mimeType;
			}
			return super.getObject (o);
		}
	}

	static
	{
		$TYPE = new NType (new FileFactory ());
		$TYPE.addManagedField (directory$FIELD = new _Field ("directory", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (String.class), null, 0));
		$TYPE.addManagedField (mimeType$FIELD = new _Field ("mimeType", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (String.class), null, 1));
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new FileFactory ();
	}

//enh:end
	

	// return an item or a objectlist<Item>
	@Override
	protected Object createItemImpl (Context ctx)
	{
		ObjectList<Item> l;
		l = read (ctx.getWorkbench ().getRegistry (), true, ctx
			.getWorkbench ());
		if (l.size<2) {
			return (l.get(0) == null) ? null : (Item) l.get(0);
		}
		return l;
	}

	// returns a list of item and not a list of Object[] as ReadOne produces
	private ObjectList<Item> read (Registry reg, boolean forceItem,
			Workbench ctx)
	{
		IOFlavor flavor = IOFlavor.valueOf (getObjectType ()
			.getImplementationClass ());
		FileChooserResult fr = ctx.getToolkit().chooseFile (
				null, 
				IO.getReadableFileTypes(new IOFlavor[] { flavor }),
				Window.ADD_FILE, 
				true, null, ctx, null);
		if (fr == null)
		{
			return new ObjectList<Item>(0);
		}
		ObjectList<Item> result = new ObjectList<Item>(fr.files.length);
		// files is >2
		ObjectList<FileSource> srcs = fr.createAllFileSources (reg, new StringMap (this));
		boolean askLink = srcs.size >= 2;
		
		for (FilterSource src: srcs) {
			Object o = ctx.readObject (src, flavor);
			if (o == null)
			{
				break;
			}
			//Object[] a = {o, null};
			boolean embeddable = (o instanceof Manageable)
				&& ((Manageable) o).getManageableType ().isSerializable ();
			if (!forceItem && embeddable)
			{
				result.add(null) ;
				break;
			}
			ObjectItem item=null;
			if (src instanceof FileSource)
			{
				File f = ((FileSource) src).getInputFile ();
				switch (askLink? 0 : ctx.getWindow ().showChoiceDialog (
						IO.toName (src.getSystemId ()),
						UI.I18N,
						"addfiledialog",
						embeddable ? new String[] {"add", "link", "embed"}
								: new String[] {"add", "link"}))
					{
						case 0:
							FileSystem pfs = reg.getFileSystem ();
							Object dir = pfs.getRoot ();
							if (directory != null)
							{
								try
								{
									dir = pfs.create (dir, directory, true);
								}
								catch (IOException e)
								{
									ctx.logGUIInfo (IO.I18N.msg ("mkdir.failed",
										directory), e);
									return new ObjectList<Item>(0);
								}
							}
							Object dest;
							try
							{
								if (pfs.isIn(f.getName()))
									dest = pfs.createIfDoesntExist (dir, f.getName (), false, false);
								else {
									dest = pfs.createIfDoesntExist (dir, f.getName (), false, false);
									pfs.addLocalFile (f, dir, pfs.getName (dest), false, false);
								}						
							}
							catch (IOException e)
							{
								ctx.logGUIInfo (IO.I18N
									.msg ("addfile.failed", directory), e);
								return new ObjectList<Item>(0);
							}
							item = new FileObjectItem (IO.toSystemId (pfs, dest), src
								.getFlavor ().getMimeType (), o, getTypeAsString ());
							break;
						case 1:
							item = new FileObjectItem ((FileSource) src, o,
								getTypeAsString ());
							break;
						case 2:
							item = ObjectItem.createReference (reg, o, IO.toSimpleName (src
								.getSystemId ()));
							break;
						default:
							break;
					}
				//
//				FileSystem pfs = reg.getFileSystem ();
//				Object dir = pfs.getRoot ();
//				if (directory != null)
//				{
//					try
//					{
//						dir = pfs.create (dir, directory, true);
//					}
//					catch (IOException e)
//					{
//						ctx.logGUIInfo (IO.I18N.msg ("mkdir.failed",
//							directory), e);
//						break;
//					}
//				}
//				Object dest;
//				try
//				{
//					if (pfs.isIn(f.getName()))
//						dest = pfs.createIfDoesntExist (dir, f.getName (), false, false);
//					else {
//						dest = pfs.createIfDoesntExist (dir, f.getName (), false, false);
//						pfs.addLocalFile (f, dir, pfs.getName (dest), false, false);
//					}						
//				}
//				catch (IOException e)
//				{
//					ctx.logGUIInfo (IO.I18N
//						.msg ("addfile.failed", directory), e);
//					break;
//				}
//				item = new FileObjectItem (IO.toSystemId (pfs, dest), src
//					.getFlavor ().getMimeType (), o, getTypeAsString ());
			}
			else
			{
				item = ObjectItem.createReference (reg, o, IO
					.toSimpleName (src.getSystemId ()));
			}
			result.add( item);
		}
		return result;
		
	}
	
	
	private Object[] readOne (Registry reg, FilterSource src, boolean forceItem,
			Workbench ctx)
	{
		IOFlavor flavor = IOFlavor.valueOf (getObjectType ()
				.getImplementationClass ());
			if (src == null)
			{
				FileChooserResult fr = ctx.getToolkit().chooseFile (
						null, 
						IO.getReadableFileTypes(new IOFlavor[] { flavor }),
						Window.ADD_FILE, 
						true, null, ctx, null);
				if (fr == null)
				{
					return null;
				}
				src = fr.createFileSource (reg, new StringMap (this));
			}
			Object o = ctx.readObject (src, flavor);
			if (o == null)
			{
				return null;
			}

			Object[] a = {o, null};
			boolean embeddable = (o instanceof Manageable)
				&& ((Manageable) o).getManageableType ().isSerializable ();
			if (!forceItem && embeddable)
			{
				return a;
			}
			ObjectItem item;
			if (src instanceof FileSource)
			{
				File f = ((FileSource) src).getInputFile ();
				FileSystem pfs = reg.getFileSystem ();
				Object dir = pfs.getRoot ();
				if (directory != null)
				{
					try
					{
						dir = pfs.create (dir, directory, true);
					}
					catch (IOException e)
					{
						ctx.logGUIInfo (IO.I18N.msg ("mkdir.failed",
							directory), e);
						return null;
					}
				}
				Object dest;
				try
				{
					if (pfs.isIn(f.getName()))
						dest = pfs.createIfDoesntExist (dir, f.getName (), false, false);
					else {
						dest = pfs.createIfDoesntExist (dir, f.getName (), false, false);
						pfs.addLocalFile (f, dir, pfs.getName (dest), false, false);
					}						
				}
				catch (IOException e)
				{
					ctx.logGUIInfo (IO.I18N
						.msg ("addfile.failed", directory), e);
					return null;
				}
				item = new FileObjectItem (IO.toSystemId (pfs, dest), src
					.getFlavor ().getMimeType (), o, getTypeAsString ());
			}
			else
			{
				item = ObjectItem.createReference (reg, o, IO
					.toSimpleName (src.getSystemId ()));
			}
			a[1] = item;
			return a;
	}

	@Override
	public Object evaluate (RegistryContext ctx, de.grogra.util.StringMap args)
	{
		URL url = null;
		if (getBranch () != null)
		{
			Object[] a = getArgs ((Item) getBranch (), ctx, args, this);
			if ((a.length > 0) && (a[0] instanceof URL))
			{
				url = (URL) a[0];
			}
		}
		return addFromURL (ctx.getRegistry (), url, args, ((Context) args
			.get ("context")).getWorkbench());
	}

	/**
	 * Reads an object from <code>url</code>. If necessary,
	 * an item reference to the object is created in the registry.
	 * 
	 * @param reg registry in which the reference shall be created
	 * @param url URL to read
	 * @param params parameters, may be <code>null</code>
	 * @param ui workbench to use for UI
	 * @return the import object, or <code>null</code> in case of problems
	 */
	public Object addFromURL (Registry reg, URL url, ModifiableMap params, Workbench ui)
	{
		FilterSource fs = null;
		if (url != null)
		{
			try
			{
				MimeType mt = (mimeType != null) ? MimeType.valueOf (mimeType)
						: IO.getMimeType (url.toString ());
				File file = Utils.urlToFile (url);
				if (file.exists ())
				{
					fs = new FileSource (file, mt, reg, params);
				}
				else
				{
					fs = new InputStreamSourceImpl (url.openStream (), url
						.toString (), mt, reg, params);
				}
			}
			catch (IOException e)
			{
				ui.logGUIInfo (IO.I18N.msg ("openfile.failed", url.getFile ()),
					e);
				return null;
			}
		}
		Object[] a = readOne (reg, fs, false, ui);
		if (a == null)
		{
			return null;
		}
		ObjectItem i = (ObjectItem) a[1];
		if (i != null)
		{
			i.setObjDescribes (true);
			ResourceDirectory rd = ResourceDirectory.get (this);
			rd.getProjectDirectory (reg).addUserItemWithUniqueName (i,
				rd.getBaseName ());
		}
		return a[0];
	}
	
	
	/**
	 * Reads an object from a given FilterSoruce. If necessary,
	 * an item reference to the object is created in the registry.
	 * 
	 * @param reg registry in which the reference shall be created
	 * @param fs a filtersource
	 * @param params parameters, may be <code>null</code>
	 * @param ui workbench to use for UI
	 * @return the import object, or <code>null</code> in case of problems
	 */
	
	public Object addFromFilterSource(Registry reg, FilterSource fs, ModifiableMap params, Workbench ui) {
		Object[] a = readOne (reg, fs, false, ui);
		if (a == null)
		{
			return null;
		}
		ObjectItem i = (ObjectItem) a[1];
		if (i != null)
		{
			i.setObjDescribes (true);
			ResourceDirectory rd = ResourceDirectory.get (this);
			rd.getProjectDirectory (reg).addUserItemWithUniqueName (i,
				rd.getBaseName ());
		}
		return a[0];
		
	}
	
	/**
	 * Reads an object from <code>url</code>. Add an item from url into a resource directory.
	 * Does not create a copy on local file system.
	 * 
	 * @param reg registry in which the reference shall be created
	 * @param url URL to read
	 * @param params parameters, may be <code>null</code>
	 * @param ui workbench to use for UI
	 * @param dest A resource directory in the registry where the file will be added
	 * @return the import object, or <code>null</code> in case of problems
	 */
	public Object linkFromURLToResourceDir (Registry reg, URL url, ModifiableMap params, Workbench ui, Item dest)
	{
		FilterSource fs = null;
		if (url != null)
		{
			try
			{
				MimeType mt = (mimeType != null) ? MimeType.valueOf (mimeType)
						: IO.getMimeType (url.toString ());
				File file = Utils.urlToFile (url);
				if (file.exists ())
				{
					fs = new FileSource (file, mt, reg, params);
				}
				else
				{
					fs = new InputStreamSourceImpl (url.openStream (), url
						.toString (), mt, reg, params);
				}
			}
			catch (IOException e)
			{
				ui.logGUIInfo (IO.I18N.msg ("openfile.failed", url.getFile ()),
					e);
				return null;
			}
		}
		Object[] a = readOne (reg, fs, false, ui);
		if (a == null)
		{
			return null;
		}
		ObjectItem i = (ObjectItem) a[1];
		if (i != null)
		{
			i.setObjDescribes (true);
			if (dest instanceof ResourceDirectory) {
				((ResourceDirectory) dest).getProjectDirectory (reg).addUserItemWithUniqueName (i,
						((ResourceDirectory) dest).getBaseName ());
			}
			else if (dest instanceof Directory) {
				dest.addUserItem(i);
			}
		}
		return a[0];
	}

}
