package de.grogra.pf.ui;

import java.io.IOException;

import de.grogra.pf.io.FilterSource;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.registry.RegistryContext;
import de.grogra.pf.ui.registry.ProjectFactory;
import de.grogra.util.Map;

public interface ProjectManager extends RegistryContext{
	
	/**
	 * Create a new project 
	 * @param porjectFactory
	 * @return
	 * @throws IOException 
	 */

	public Project createProject(ProjectFactory projectFactory) throws IOException;
	
	/**
	 * Open an existing project into a workbench created by the uiapp (client)
	 * 
	 * @param client
	 * @param porjectFactory
	 * @param fs
	 * @param initParams
	 * @return
	 * @throws IOException
	 */
	public Project openProject(ProjectFactory projectFactory, FilterSource fs, Map initParams) throws IOException;
	
	/**
	 * Get the id of a project 
	 * @param p
	 * @return
	 */
	public Object getProjectId(Project p);
	
	/**
	 * Get exiting Project by ID
	 */
	public Project getProject(Object projectId);
		
	/**
	 * Return a list of string "project id" + "projectname"
	 */
	public String[] listOpenProjects();

	/**
	 * Link an existing project to a given wb
	 * @param projectId
	 * @param client
	 * @return
	 */
	public ProjectWorkbench connectToProject(Object p, ProjectWorkbenchLauncher wbLauncher);
	
	/**
	 * Remove the link of a project to the given workbench. 
	 * The workbench without project is to be disposed
	 * @param wb
	 */
	public void disconnectProject(ProjectWorkbench wb);
	
	/**
	 * get the workbenches that are linked to the project p
	 * @param p
	 * @return
	 */
	public ProjectWorkbench[] getLinkedWorkbenches(Project p);
	
	/**
	 * required method to unsure there is only one instance of project manager per groimp instance
	 * @return
	 */
	public static ProjectManager getInstance() {
		return null;
	}
	
	/**
	 * link a Workbench to a Project
	 * @param projectId
	 * @param client
	 */
	public void linkWorkbenchToProject(Object projectId, ProjectWorkbench client);
}
