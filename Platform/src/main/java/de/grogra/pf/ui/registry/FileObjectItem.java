
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.pf.ui.registry;

import java.io.IOException;

import de.grogra.graph.impl.Node.NType;
import de.grogra.pf.io.FileSource;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.io.ProgressMonitor;
import de.grogra.pf.registry.LazyObjectItem;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.Workbench;
import de.grogra.util.MimeType;
import de.grogra.util.StringMap;

public class FileObjectItem extends LazyObjectItem
{
	MimeType mimeType;
	//enh:field getter

//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field mimeType$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (FileObjectItem.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		protected void setObjectImpl (Object o, Object value)
		{
			switch (id)
			{
				case 0:
					((FileObjectItem) o).mimeType = (MimeType) value;
					return;
			}
			super.setObjectImpl (o, value);
		}

		@Override
		public Object getObject (Object o)
		{
			switch (id)
			{
				case 0:
					return ((FileObjectItem) o).getMimeType ();
			}
			return super.getObject (o);
		}
	}

	static
	{
		$TYPE = new NType (new FileObjectItem ());
		$TYPE.addManagedField (mimeType$FIELD = new _Field ("mimeType", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (MimeType.class), null, 0));
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new FileObjectItem ();
	}

	public MimeType getMimeType ()
	{
		return mimeType;
	}

//enh:end

	/**
	 * set at protected so children classes do not set a baseobject on default creation
	 */
	protected FileObjectItem ()
	{
		super (null, true);
	}

	
	public FileObjectItem (String systemId, MimeType mimeType, Object object,
						   String type)
	{
		super (IO.toName(systemId), true);
		this.systemId = systemId;
		this.mimeType = mimeType;
		setBaseObject (object);
		setType (type);
	}


	public FileObjectItem (FileSource fs, Object object, String type)
	{
		this (fs.getSystemId (), fs.getFlavor ().getMimeType (), object, type);
	}


	public FileSource createFileSource ()
	{
		String id = (String)getSystemId();
		if (getRegistry ().isRootRegistry ())
		{
			id = "pluginfs:" + getPluginDescriptor ().getName () + '/' + id;
		}
		return FileSource.createFileSource (id, mimeType, this, new StringMap (this));
	}


	@Override
	protected void activateImpl ()
	{
		Object f = getRegistry ().getProjectFile ((String)getSystemId());
		if (f != null)
		{
			getRegistry ().getFileSystem ().setMimeType (f, mimeType);
		}
	}

	
	@Override
	protected boolean hasNullValue ()
	{
		return false;
	}


	@Override
	protected Object fetchBaseObject ()
	{
		FilterSource s = IO.createPipeline
			(createFileSource (),
			 IOFlavor.valueOf (getObjectType ().getImplementationClass ()));
		if (!(s instanceof ObjectSource))
		{
			Workbench.current ().logGUIInfo
				(IO.I18N.msg ("openfile.unsupported", systemId,
							  IO.getDescription (mimeType)));
			return null;
		}
		s.initProgressMonitor
			(UI.createProgressAdapter (Workbench.current ()));
		try
		{
			return ((ObjectSource) s).getObject ();
		}
		catch (IOException e)
		{
			Workbench.current ().logGUIInfo
				(IO.I18N.msg ("openfile.failed", systemId), e);
			return null;
		}
		finally
		{
			s.setProgress (null, ProgressMonitor.DONE_PROGRESS);
		}
	}


	@Override
	public void addRequiredFiles (java.util.Collection list)
	{
		Object f = getRegistry ().getProjectFile ((String)getSystemId());
		if (f != null)
		{
			list.add (f);
		}
	}
	
	@Override
	protected Object getDescriptionImpl (String type)
	{
		if (NAME.equals (type))
		{
			return IO.toName (getName ());
		}
		return super.getDescriptionImpl (type);
	}
	
	@Override
	/**
	 * Delete the associated File of the object
	 * 
	 * @see FileSystem
	 */
	public void delete() {
			try {
				if (getRegistry().getProjectFile(getName()) !=null )  
					getRegistry ().getFileSystem ().delete(getRegistry().getProjectFile(getName())) ;
			} catch (IOException e) {
				Workbench.get (this).logGUIInfo (
						IO.I18N.msg ("deletefile.failed", getName ()), e);
			}
	}

}