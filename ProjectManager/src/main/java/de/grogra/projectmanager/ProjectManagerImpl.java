package de.grogra.projectmanager;

import java.io.IOException;

import de.grogra.graph.impl.Node;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.registry.Directory;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.ObjectItem;
import de.grogra.pf.registry.Plugin;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.Project;
import de.grogra.pf.ui.ProjectManager;
import de.grogra.pf.ui.ProjectWorkbench;
import de.grogra.pf.ui.ProjectWorkbenchLauncher;
import de.grogra.pf.ui.UIApplication;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.registry.ProjectFactory;
import de.grogra.util.Map;

public class ProjectManagerImpl extends Plugin implements ProjectManager {
	private static ProjectManagerImpl PLUGIN;
	private static String projectPath = "/ProjectManager/projects";
	private static int projectId;

	
	public ProjectManagerImpl() {
		if(PLUGIN == null) {
			PLUGIN = this;
		}
		projectId = 0; // for the naming of the Project directories in the Registry
	}

	public static ProjectManager getInstance() {
		return PLUGIN;
	}

	/**
	 * Create a new directory for the Project and add a reference to the project as the first entry 
	 * @param ep
	 * @return Project Id (also name of the directory)
	 */
	private Object registerProject(ProjectImpl ep) {
		Item dir = getRegistry().getDirectory(projectPath +"/"+ projectId, getPluginDescriptor());
		dir.add(ObjectItem.createReference(this, ep, "project"));
		return projectId++;
	}

	/**
	 * remove the Project reference and than the project directory from the Registry
	 * @param projectId
	 */
	private void unregisterProject(Object projectId) {
		//TODO check if all workbenches are closed and if necessary close them form here
		Item x =getRegistry().getItem(projectPath +"/"+ projectId + "/project");
		
		ProjectImpl p = (ProjectImpl)((ObjectItem)x).getObject();
		p.dispose();
		x.remove();
		getRegistry().getItem(projectPath +"/"+ projectId).remove();
	}

	/**
	 * Add a workbench to a reference to a workbench to the project directory. 
	 * The reference is named after the application type, meaning that each UIApplication can only register one workbench per Project
	 * @param projectId
	 * @param client
	 */
	public void linkWorkbenchToProject(Object projectId, ProjectWorkbench client) {
		Item dir = getRegistry().getDirectory(projectPath +"/"+ projectId, null);
		dir.add(ObjectItem.createReference(this, client,(String)client.getToken()));
	}

	/**
	 * remove reference to the Workbench from the project directory and if it was the last reference, close the project 
	 * @param projectId
	 * @param client
	 */
	private void unlinkWorkbenchFromProject(Object projectId, ProjectWorkbench client) {
		Item dir = getRegistry().getItem(projectPath +"/"+ projectId );
		if(dir==null) {return;}
		Object t = client.getToken();
		Item token=dir.getItem((CharSequence) t);
		token.remove();
		if (getRegistry().getChildCount(dir) < 2) {
			unregisterProject(projectId);
		}
	}
	/**
	 * - Call a ProjectFactory to create a new ExecutableProject based on a child registry.
	 * - Register this new Project to the manager and than call the UIApplication to create a new Workbench around the project 
	 * - Return the Workbench to the Project.
	 * 
	 * The basic idea of the createNewWorkbench function is that a "Naked" Project should not leave the ProjectManager
	 * @throws IOException 
	 */
	@Override
	public Project createProject(ProjectFactory projectFactory) throws IOException {
		ProjectImpl ep = (ProjectImpl) projectFactory.createProject(Registry.create(getRegistry()), PLUGIN);
		Object projectId = registerProject(ep);
		ep.setId(projectId);
		return ep;
	}
	
	

	public Project openProject(ProjectFactory projectFactory, FilterSource fs,
			Map initParams) throws IOException {
		//create Registries for the Project and the Workbench
		Registry projectRegistry = Registry.create(getRegistry());

		//load the Project and with it the Registry and the Graph
		ProjectImpl ep = (ProjectImpl) projectFactory.loadProject(fs, initParams,projectRegistry , PLUGIN);
		Object projectId = registerProject(ep);
		ep.setId(projectId);
		return ep;
	
		
}
	
	

	@Override
	/**
	 * get the Id of the Project
	 */
	public Object getProjectId(Project ep) {
		return ep.getId();
	}

	public ProjectImpl getProject(Object id) {
		return (ProjectImpl) ((ObjectItem) getRegistry().getItem(projectPath +"/"+ id + "/project")).getObject();
	}

	
	/**
	 * create a new Workbench around the selected Project and register the Workbench 
	 * It pushes the registry 's project into the wb
	 */
	@Override
	public ProjectWorkbench connectToProject(Object id, ProjectWorkbenchLauncher wbLauncher) {
		ProjectImpl ep = getProject(projectId);
		ProjectWorkbench wb =wbLauncher.create(ep);
		linkWorkbenchToProject(projectId, wb);
		return wb;
	}

	/**
	 * Unlink workbench from a project
	 */
	@Override
	public void disconnectProject(ProjectWorkbench client) {
		unlinkWorkbenchFromProject(client.getProject().getId(), client);
	}

	
	/**
	 * iterate over all subfolders and select the project elements, to create a list
	 */
	@Override
	public String[] listOpenProjects() {
		Item projectDir = getRegistry().getDirectory(projectPath, null);
		int l= projectDir.getBranchLength();
		if (l == 0) {
			return null;
		}
		String[] list = new String[l-1];
		int i = 0;
		for (Node n = projectDir.getBranch(); n != null; n = n.getSuccessor()) {
			String key =((Item) n).getAbsoluteName() + "/project";
			Item n1 = ((Directory) n).getItem("project");
			//Object n1 = n.getAccessor("project");
			if (n1 instanceof ObjectItem) {
				if (((ObjectItem) n1).isInstance(ProjectImpl.class)) {
					Object x = ((ObjectItem) n1).getObject();
					list[i] = ((ProjectImpl) x).getId() + " : "+ ((ProjectImpl) x).getName();
					i++;
				}
			}
		}
		return list;
	}

	@Override
	/**
	 * iterate over all References of a project directory and select the workbench elements, to create a list
	 */
	public ProjectWorkbench[] getLinkedWorkbenches(Project ep) {
		Item pDir = getRegistry().getDirectory(projectPath +"/"+ ep.getId(), null);
		int l = pDir.getBranchLength() - 1;
		if (l < 1) {
			return new ProjectWorkbench[0];
		}
		ProjectWorkbench[] list = new ProjectWorkbench[l];
		int i = 0;
		for (Node n = pDir.getBranch(); n != null; n = n.getSuccessor()) {
			if (n instanceof ObjectItem) {
				if (((ObjectItem) n).isInstance(ProjectWorkbench.class)) {
					Object x = ((ObjectItem) n).getObject();
					list[i] = (ProjectWorkbench) x;
					i++;
				}
			}
		}
		return list;
	}


	
	

	
}
