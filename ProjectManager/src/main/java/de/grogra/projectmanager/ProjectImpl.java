package de.grogra.projectmanager;

import java.util.concurrent.Executor;
import java.util.logging.Filter;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import de.grogra.graph.impl.Node;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.ItemCriterion;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.Command;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.JobManager;
import de.grogra.pf.ui.Project;
import de.grogra.pf.ui.ProjectManager;
import de.grogra.pf.ui.ProjectWorkbench;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.registry.SourceFile;
import de.grogra.util.ThreadContext;
import de.grogra.xl.util.ObjectList;

public class ProjectImpl extends Project {
	ProjectManager pm;
	
	public ProjectImpl(Registry registry, ProjectManager pm) {
		super(registry);
		this.pm = pm;
	}

	public void initialize(JobManager jm) {
		if(!getRegistry().isActive()) {
			ThreadContext s = jm.getThreadContext();
			regState = getRegistry ().getRegistryGraph ().createStaticState (s);
			getRegistry().getProjectGraph().initMainState((Executor) jm);
			getRegistry().startup();
		}
		updateName();
	}

	public void dispose() {
		getRegistry().dispose();				
	}
		
	public final Logger getLogger() {
		return getRegistry().getLogger();
	}
	
	@Override
	protected void initLogger(){
		logFilter = getLogger ().getFilter ();
		pendingLogs = new ObjectList ();
		getLogger ().setFilter (new Filter ()
		{
			public boolean isLoggable (LogRecord r)
			{
				if ((logFilter == null) || logFilter.isLoggable (r))
				{
					pendingLogs.add (r);
				}
				return false;
			}
		});
	}
	
	/***
	 * Listing all commands of the Project, as an array of CommandItems If the
	 * project dose not contain CommandsItems it returns null
	 * 
	 * @return array of CommandItem
	 */
	public Command[] listFunctions() {
		Item projectDir = Item.resolveItem(getRegistry(), "/workbench/rgg/methods");
		if(projectDir==null) {
			return new Command[0];
		}
		int l = projectDir.getBranchLength();
		int i = 0;
		Command[] list = new Command[l];
		for (Node n = projectDir.getBranch(); n != null; n = n.getSuccessor()) {
			list[i] = (Command) n;
			i++;
		}

		return list;
	}

	/**
	 * Taking the command by name from the Registry and execute it
	 * 
	 * @param command
	 * @param ctx
	 */

	public void execute(String command, Context ctx) {
		Item c = Item.resolveItem(getRegistry(), "/workbench/rgg/methods/" + command);
		if (c != null && c instanceof Command) {
			this.execute((Command) c, ctx);
		}
	}

	/**
	 * executing a command with the Jobmanager of the Given Context using the
	 * ProjectGraph of the Projects Registry as a Lock
	 * 
	 * @param command
	 * @param ctx
	 */

	public void execute(Command command, Context ctx) {
		UI.executeLockedly(getRegistry().getProjectGraph(), true, new Command() {
			@Override
			public String getCommandName() {
				return null;
			}

			@Override
			public void run(Object arg, Context c) {
				command.run(arg,c);
			}
			}, null, ctx, JobManager.ACTION_FLAGS);
	}

	/***
	 * activates a file of the project and therefore triggers the compiler, if no
	 * file is found nothing is happening
	 */
	public void compile(Context context) {
		compile(context, null);
	}	
	/***
	 * activates a file of the project and therefore triggers the compiler, if no
	 * file is found nothing is happening
	 */
	public void compile(Context context, Command afterCommand) {
		Item fileDir = Item.resolveItem(getRegistry(), "/project/objects/files");
		Registry.setCurrent(getRegistry());
		Item file= Item.findFirst(fileDir,ItemCriterion.INSTANCE_OF,SourceFile.class, false);
//		((SourceFile)file).shutdown(true);
		SourceFile.refresh(fileDir, (SourceFile)file,"java.lang.Class", afterCommand);
	}
	
	
	/**
	 * Update the at all workbenches that are connected to the project
	 */
	@Override
	protected void updateName() {
		for (ProjectWorkbench pw : pm.getLinkedWorkbenches(this)) {
			pw.setName(getName());
		}
	}

	@Override
	protected void getState(Item s) {
		// TODO for now no Layout is saved with the project
	}

	@Override
	public void stopLocalFileSynchronization() {
	}

	@Override
	public void startLocalFileSynchronization() {
	}
	
	public String toString() {
		return getId()+": "+getName();
	}
}
