<chapter id="c-packages-cunits">
<title>Packages and Compilation Units</title>

<para>
As for the Java programming language, programmes of the XL
programming language are organized in a hierarchical structure
of <firstterm>packages</firstterm>. A package has subpackages,
class and interface types, and class predicates
as its members. Class and interface types are declared
in <firstterm>compilation units</firstterm> of the package.
A compilation unit is normally
stored in a single file of source code. For the XL programming
language, such a compilation unit has to be either a legal
compilation unit in the sense of the Java programming language,
or a legal compilation unit as defined in this XL language specification.
The former type of compilation units is normally indicated by filenames
ending with <filename>.java</filename>, whereas the latter type
is indicated by the suffix <filename>.xl</filename>.
</para>

<section><title>Compilation Units</title>

<para>
<emphasis>CompilationUnit</emphasis> is the goal symbol for the
syntactic grammar of XL programmes. It is defined by the following
production:
</para>
<productionset><title>Compilation Unit</title>
<production id="ebnf.cunit">
  <lhs>CompilationUnit</lhs>
  <rhs>[<nonterminal def="#ebnf.package">PackageDeclaration</nonterminal>]
       {<nonterminal def="#ebnf.import">ImportDeclaration</nonterminal>}
       {<nonterminal def="#ebnf.typedecl">TypeDeclaration</nonterminal>}
  </rhs>
</production>
<production id="ebnf.package">
  <lhs>PackageDeclaration</lhs>
  <rhs><literal>package</literal>
       <nonterminal def="#ebnf.name">Name</nonterminal> ';'
  </rhs>
</production>
</productionset>
<para>This is the same as for the Java programming language. Also
the predefined package <classname>java.lang</classname> is always
imported. However, the possibilities for import declarations
(<xref linkend="s-imports"/>) have
been extended to static imports.
</para>

</section>

<section id="s-imports"><title>Import Declarations</title>

<para>
An <firstterm>import declaration</firstterm> allows a named type or a
static member to be referred to by a simple name instead of a fully
qualified name. The XL programming language defines four kinds of
import declarations:
</para>
<productionset><title>Import Declarations</title>
<production id="ebnf.import">
  <lhs>ImportDeclaration</lhs>
  <rhs><nonterminal def="#ebnf.singletype">SingleTypeImportDeclaration
       </nonterminal> |
       <nonterminal def="#ebnf.typeondemand">TypeImportOnDemandDeclaration
       </nonterminal> |
       <nonterminal def="#ebnf.singlestatic">SingleStaticImportDeclaration
       </nonterminal> |
       <nonterminal def="#ebnf.staticondemand">StaticImportOnDemandDeclaration
       </nonterminal>
  </rhs>
</production>
<production id="ebnf.singletype">
  <lhs>SingleTypeImportDeclaration</lhs>
  <rhs><literal>import</literal>
  <nonterminal def="#ebnf.name">Name</nonterminal> ';'
  </rhs>
</production>
<production id="ebnf.typeondemand">
  <lhs>TypeImportOnDemandDeclaration</lhs>
  <rhs><literal>import</literal>
  <nonterminal def="#ebnf.name">Name</nonterminal> '.' '*' ';'
  </rhs>
</production>
<production id="ebnf.singlestatic">
  <lhs>SingleStaticImportDeclaration</lhs>
  <rhs><literal>import</literal> <literal>static</literal>
  <nonterminal def="#ebnf.name">Name</nonterminal> '.' Identifier ';'
  </rhs>
</production>
<production id="ebnf.staticondemand">
  <lhs>StaticImportOnDemandDeclaration</lhs>
  <rhs><literal>import</literal> <literal>static</literal>
  <nonterminal def="#ebnf.name">Name</nonterminal> '.' '*' ';'
  </rhs>
</production>
</productionset>
<para>
The first two kinds, <firstterm>single-type-import</firstterm> and
<firstterm>type-import-on-demand declarations</firstterm>,
are already known from the Java programming language. Thus, they are
not explained here.
The last two kinds, <firstterm>single-static-import</firstterm> and
<firstterm>static-import-on-demand declarations</firstterm>,
have been defined in the Java Language Specification,
Third Edition, and are defined for the XL programming language, too.
</para>
<para>
A single-static-import declaration imports all accessible static members
with the given name from the given type. For the details see the
Java Language Specification, Third Edition.
</para>
<para>
A static-import-on-demand declaration imports all accessible static members
declared in the given type as needed. For the details see the
Java Language Specification, Third Edition.
</para>

</section>

</chapter>
