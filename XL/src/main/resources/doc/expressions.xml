<chapter id="c-expressions">
<title>Expressions</title>

<para>
The <firstterm>expressions</firstterm> of the XL programming language
are a superset of the expressions of the Java programming language. For
the general terms and specifications in the context of expressions
and their evaluation,
see the Java language specification. Based on these definitions,
this chapter describes the
expressions which are introduced by the XL programming language.
</para>
<para>
To simplify the formulations, the following convention is implicitly
present in the following, if not specified otherwise: If a subexpression
<replaceable>s</replaceable> which is part of an expression
<replaceable>e</replaceable> completes abruptly, then
<replaceable>e</replaceable> completes abruptly for the same reason,
and remaining subexpressions of <replaceable>e</replaceable>, if any,
are not evaluated. The remaining subexpressions are the subexpressions
to the right of <replaceable>s</replaceable>.
</para>


<section id="s-genexpr"><title>Generator Expressions and Sequential Evaluation</title>

<para>
When an expression in a programme is <firstterm>evaluated</firstterm>,
the result denotes one of three things: A variable (in the C programming
language, this would be called an <firstterm>lvalue</firstterm>),
a value, or nothing (the expression is said
to be <classname>void</classname>). This holds
for the normal evaluation of expressions and is the same as for
the Java programming language.
</para>
<para>
The XL programming language defines
<firstterm>generator expressions</firstterm> which successively
<firstterm>yield</firstterm> results.
These expressions cannot be evaluated normally,
they have to be evaluated <firstterm>sequentially</firstterm> by
special language constructs, namely the enhanced
<literal>for</literal> statement, aggregate method invocation expressions,
expression statements, the <literal>yield</literal> statement,
or expression predicates. A sequential evaluation of a normal or
generator expression
<replaceable>e</replaceable> yields several results in succession and
is performed in the following way:
</para>
<itemizedlist>
<listitem><para>
Let <replaceable>s<subscript>1</subscript></replaceable>, ...,
<replaceable>s<subscript>n</subscript></replaceable> be the
subexpressions of <replaceable>e</replaceable> in their evaluation order,
i.e., from left to right.
</para></listitem>
<listitem><para>
<replaceable>s<subscript>1</subscript></replaceable> is evaluated
sequentially by a recursive application of this definition.
For each yielded result
<replaceable>v<subscript>1</subscript></replaceable>,
<replaceable>s<subscript>2</subscript></replaceable> is evaluated
sequentially yielding results
<replaceable>v<subscript>2</subscript></replaceable>
and so on, finally leading to a sequential evaluation
of <replaceable>s<subscript>n</subscript></replaceable>.
</para></listitem>
<listitem><para>
For each yielded result <replaceable>v<subscript>n</subscript></replaceable>,
the expression <replaceable>e</replaceable> is evaluated in a special
context: When
<replaceable>e</replaceable> evaluates its subexpressions
<replaceable>s<subscript>k</subscript></replaceable>, these are treated
as being constant expressions having the previously computed values
<replaceable>v<subscript>k</subscript></replaceable>. If
<replaceable>e</replaceable> is a normal expression, this special evaluation
of <replaceable>e</replaceable> is done normally, and the single result is
yielded as a result of the sequential evaluation of
<replaceable>e</replaceable>. Otherwise, <replaceable>e</replaceable>
is a generator expression. Then the special evaluation is done sequentially
as defined for the specific generator expression,
and each yielded result is yielded as a result of the
whole sequential evaluation.
</para></listitem>
</itemizedlist>
<para>
A generator expression is called a
<firstterm>sequential expression</firstterm>.
The following definitions and specifications apply to sequential expressions:
Let <replaceable>s</replaceable> be a sequential expression.
</para>
<itemizedlist>
<listitem><para>
If <replaceable>s</replaceable> is a subexpression of a
containment expression or an aggregate method invocation expression,
then this containing expression is called the <firstterm>target</firstterm>
of <replaceable>s</replaceable>.
</para></listitem>
<listitem><para>
Otherwise, if <replaceable>s</replaceable> is a subexpression of
an expression <replaceable>e</replaceable> not covered by the
previous definition, <replaceable>e</replaceable> is also called a
sequential expression, and the target of <replaceable>s</replaceable>
is defined to be the target of <replaceable>e</replaceable>. It is
a compile-time error if <replaceable>e</replaceable> is a conditional
operator (<literal>||</literal>, <literal>&amp;&amp;</literal>,
or <literal>?:</literal>) or a compound assignment operator and
<replaceable>s</replaceable> is not the first (left-most) operand.
</para></listitem>
<listitem><para>
Otherwise, if <replaceable>s</replaceable> is the expression of a
<literal>yield</literal> statement or an expression statement,
this statement is the target of <replaceable>s</replaceable>.
</para></listitem>
<listitem><para>
Otherwise, if <replaceable>s</replaceable> is the iterator expression of
an enhanced <literal>for</literal> statement, the <literal>for</literal>
statement is the target of <replaceable>s</replaceable>.
</para></listitem>
<listitem><para>
In any other case, a compile-time error occurs.
</para></listitem>
</itemizedlist>

</section>


<section id="s-method-invocation"><title>Method Invocation Expressions</title>

<para>
As for the Java programming language, a
<firstterm>method invocation expression</firstterm> 
is used to invoke a class or instance method. The XL programming language
extends the semantics of method invocation expressions in several
directions:
</para>
<itemizedlist>
<listitem><para>
Depending on the context of the method invocation expression, there may
be some <firstterm>implicit arguments</firstterm> for the invocation.
These are specified by the available
<firstterm>argument transformations</firstterm> of the context.
</para></listitem>
<listitem><para>
<firstterm>Generator method invocations</firstterm> are generator
expressions whose evaluation results in a sequence of values.
</para></listitem>
<listitem><para>
<firstterm>Aggregate method invocations</firstterm> invoke
aggregate methods repeatedly for each result of the
sequential evaluation of its arguments or for each component
of arrays and return a single result.
</para></listitem>
<listitem><para>
<firstterm>Filter method invocations</firstterm> invoke
filter methods repeatedly for each result of the
evaluation of generator expressions or for each component
of arrays and are used to filter sequentially yielded results.
</para></listitem>
</itemizedlist>
<para>
The Java Language Specification defines steps to determine the method that
will be invoked by a method invocation expressions.
The new features of the XL programming language modify these steps; the
modifications are described in the chapter on signatures
(<xref linkend="c-signatures"/>) and in the following.
</para>

<section id="s-mi-argtrans"><title>Argument Transformations</title>

<para>
In the context of a normal expression, a method invocation expression 
has four alternatives of argument transformations
(<xref linkend="s-argtrans"/>):
</para>
<orderedlist>
<listitem><para>
The arguments are not transformed at all. The invocation uses the
explicitly specified argument expressions as it is known from
the Java programming language.
</para></listitem>
<listitem><para>
The invocation has an implicit consumer argument as first actual
argument. Then the invocation is a generator method invocation.
</para></listitem>
<listitem><para>
The invocation has an implicit aggregate argument as first actual
argument. Then the invocation is an aggregate method invocation.
</para></listitem>
<listitem><para>
The invocation has an implicit filter argument as first actual
argument. Then the invocation is a filter method invocation.
</para></listitem>
</orderedlist>

</section>

<section id="s-gmethod-invocation"><title>Generator Method Invocations</title>

<para>
A generator method invocation expression is a generator expression.
It invokes a <firstterm>generator method</firstterm>,
the result is a sequence of values.
</para>
<para>
A generator method (<xref linkend="s-generator-methods"/>)
is a method with a non-<literal>void</literal>
return type <replaceable>T</replaceable> and a first parameter of type
<classname>de.grogra.xl.lang.</classname><replaceable>S</replaceable><classname>Consumer</classname>,
where <replaceable>S</replaceable> is
the type affix (<xref linkend="s-typeaffix"/>) of
<replaceable>T</replaceable>.
The type of the generator method invocation
expression is <replaceable>T</replaceable>.
A generator method invocation for such a method has a single
implicit argument which
is prepended before the explicit argument expressions and is
commensurate with the consumer parameter.
</para>
<para>
The evaluation of a generator method invocation expression is performed
in several steps:
</para>
<orderedlist>
<listitem><para>
An instance of <replaceable>S</replaceable><classname>Consumer</classname>
is provided as first argument, the other arguments are obtained by the
evaluation of the corresponding argument expressions.
</para></listitem>
<listitem><para>
The generator method is invoked.
</para></listitem>
<listitem><para>
Possibly the generator method yields a value by invoking the
<methodname>consume</methodname>-method on the consumer. This
method has to be implemented by the compiler such that it continues
the sequential evaluation of the yielded values, i.e., such that the
control flow continues at the expression
containing the generator method invocation expression.
</para></listitem>
<listitem><para>
Finally, if the method invocation completes normally,
its result value is discarded.
</para></listitem>
</orderedlist>

</section>

<section id="s-amethod-invocation"><title>Aggregate Method Invocations</title>

<para>
An aggregate method invocation expression invokes an
<firstterm>aggregate method</firstterm> in order to compute
some aggregate value for a sequence of values. This sequence may
result from a sequential expression or an expression of array type.
</para>
<para>
An aggregate method
is a method with at least two parameters. The first parameter has to be
of the type <classname>de.grogra.xl.lang.Aggregate</classname>. 
An aggregate method invocation for such a method uses an argument
transformation with a single
implicit argument of the type <classname>Aggregate</classname> which
is prepended before the explicit argument expressions and
commensurate with the aggregate parameter. In addition, if
the first explicit argument expression has an array type whose component type
can be converted to the type of the second parameter by a
method invocation conversion, the second
actual argument expression is the array generator expression
(<xref linkend="s-arraygen"/>) of the first explicit argument expression.
The return type of an aggregate method invocation expression is
determined as follows: Let <replaceable>T</replaceable> be the type of the
second actual argument expression. If <replaceable>T</replaceable> is
a primitive type, then let <replaceable>U</replaceable> be the type
of the corresponding method parameter. Otherwise,
let <replaceable>U</replaceable> be <replaceable>T</replaceable>.
Now if the return type of the method declaration is
<classname>void</classname>, the return type of the invocation expression
is <replaceable>U</replaceable>. If the return type of the declaration
is <classname>java.lang.reflect.Array</classname>, the return type of the
invocation is <replaceable>U[]</replaceable>. Otherwise, the return type
of the invocation is the return type of the declaration.
</para>
<para>
The evaluation of an aggregate method invocation expression is performed
in several steps:
</para>
<orderedlist>
<listitem><para>
An instance <replaceable>a</replaceable> of
<classname>Aggregate</classname> is allocated by the method
<methodname>Aggregate.allocate</methodname>. The
<literal>type</literal>-argument to this method is the return type of
the aggregate method invocation expression.
</para></listitem>
<listitem><para>
The method invocation expression is evaluated sequentially, with
<replaceable>a</replaceable> being the value of the first actual
argument expression. For each yielded result, it is checked whether
the invocation of the method
<methodname>isFinished</methodname> on <replaceable>a</replaceable>
returns <literal>true</literal>. If this is the case, the evaluation
of the whole aggregate method invocation completes, its result is
the result of the invocation of the method
<replaceable>c</replaceable><methodname>val</methodname>
on <replaceable>a</replaceable>, where <replaceable>c</replaceable> is
the type letter (<xref linkend="s-typeaffix"/>) of the return type
of the aggregate method invocation expression.
The returned value is converted to this type if necessary.
</para></listitem>
<listitem><para>
Finally, if the sequential evaluation has completed and
the invocation of the method
<methodname>isFinished</methodname> on <replaceable>a</replaceable>
has always returned <literal>false</literal>, the method
<methodname>setFinished</methodname> is invoked on
<replaceable>a</replaceable>. Then the aggregate method is invoked
for a last time, with <replaceable>a</replaceable> as first argument
and <literal>null</literal>-values of the corresponding parameter types
as the other arguments. The evaluation of the aggregate method invocation
completes with a result obtained as described in the previous step.
</para></listitem>
</orderedlist>

</section>

<section id="s-fmethod-invocation"><title>Filter Method Invocations</title>

<para>
A filter method invocation expression is a generator expression which invokes a
<firstterm>filter method</firstterm> in order to filter
a sequence of values. This sequence may
result from a sequential expression or an expression of array type.
</para>
<para>
A filter method
is a method with at least two parameters. The first parameter has to be
of the type <classname>de.grogra.xl.lang.Filter</classname>. 
A filter method invocation for such a method uses an argument
transformation with a single
implicit argument of the type <classname>Filter</classname> which
is prepended before the explicit argument expressions and
commensurate with the filter parameter. In addition, if
the first explicit argument expression has an array type whose component type
can be converted to the type of the second parameter by a
method invocation conversion, the second
actual argument expression is the array generator expression
(<xref linkend="s-arraygen"/>) of the first explicit argument expression.
The return type of a filter method invocation expression is determined
as described for an aggregate method invocation expression.
</para>
<para>
The sequential evaluation of a filter method invocation expression is performed
in several steps:
</para>
<orderedlist>
<listitem><para>
An instance <replaceable>f</replaceable> of
<classname>Filter</classname> is allocated by the method
<methodname>Filter.allocate</methodname>. The
<literal>type</literal>-argument to this method is the return type of
the filter method invocation expression.
</para></listitem>
<listitem><para>
The method invocation expression is evaluated sequentially, with
<replaceable>f</replaceable> being the value of the first actual
argument expression. For each yielded result, it is checked whether
the field <methodname>accept</methodname> of <replaceable>f</replaceable>
is <literal>true</literal>. Only if this is the case, the value of the field
<replaceable>c</replaceable><methodname>val</methodname>
of <replaceable>f</replaceable> is yielded as a result of the
sequential evaluation of the filter method invocation expression,
where <replaceable>c</replaceable> is
the type letter (<xref linkend="s-typeaffix"/>) of the return type
of the filter method invocation expression;
the field value is converted to this type if necessary.
Then it is checked if the invocation of the method
<methodname>isFinished</methodname> on <replaceable>f</replaceable>
returns <literal>true</literal>. If this is the case, the evaluation
of the whole filter method invocation completes.
</para></listitem>
</orderedlist>

</section>

</section>

<section><title>Primary Expressions</title>

<productionset><title>Primary Expressions</title>
<production id="ebnf.prim">
  <lhs>Primary</lhs>
  <rhs>'(' <nonterminal def="#ebnf.expr">Expression</nonterminal> ')'
    | '(*' <nonterminal def="#ebnf.query">Query</nonterminal> '*)'
    | <literal>new</literal> <nonterminal def="#ebnf.creator">Creator</nonterminal>
    | <nonterminal def="#ebnf.primnop">PrimaryNoParen</nonterminal>
  </rhs>
</production>
<production id="ebnf.primnop">
  <lhs>PrimaryNoParen</lhs>
  <rhs><literal>this</literal>
    | <nonterminal def="#ebnf.literal">Literal</nonterminal>
    | <nonterminal def="#ebnf.name">Name</nonterminal>
    | <literal>super</literal> '.' Identifier
    | <nonterminal def="#ebnf.name">Name</nonterminal> '.' (<literal>this</literal> | <literal>super</literal>)
    | (<nonterminal def="#ebnf.type">Type</nonterminal> | <literal>void</literal>) '.' <literal>class</literal>
    | <nonterminal def="#ebnf.name">Name</nonterminal> <nonterminal def="#ebnf.args">Arguments</nonterminal>
    | <literal>super</literal> '.' Identifier <nonterminal def="#ebnf.args">Arguments</nonterminal>
  </rhs>
</production>
<production id="ebnf.args">
  <lhs>Arguments</lhs>
  <rhs>'(' <nonterminal def="#ebnf.assignment">AssignmentExpression</nonterminal>
  {',' <nonterminal def="#ebnf.assignment">AssignmentExpression</nonterminal>} ')'
  </rhs>
</production>
</productionset>
</section>

<section id="s-qexpr"><title>Query Expressions</title>

<para>
A <firstterm>query expression</firstterm> is a generator expression
yielding values which match a query pattern (<xref linkend="c-queries"/>).
The type of a query expression is the type of the out-parameter of its
compound predicate, or <classname>void</classname> if no out-parameter exists.
The sequential evaluation of a query expression is performed by
invoking the query as specified in <xref linkend="s-query-invocation"/>. For
each match of the query, the value of the query variable associated
with the out-parameter is yielded, or
nothing if no out-parameter exists.
</para>
<para>
The scope of the query variables of the query's compound predicate
is the rest of the target (<xref linkend="s-genexpr"/>)
of the query expression, or the rest
of the innermost expression list between the target and the query
expression if such an expression list exists.
</para>

</section>


<section id="s-bracketaccess"><title>Bracket Access Expressions</title>

<para>
<firstterm>Bracket access expressions</firstterm> are those
expressions which are classified as array access expressions
by the Java programming language, i.e., expressions of the form
<replaceable>e</replaceable><literal>[</literal><replaceable>i</replaceable><literal>]</literal>.
</para>
<productionset><title>Bracket Access Expressions and Other Selector Expressions</title>
<production id="ebnf.selexpr">
  <lhs>SelectorExpression</lhs>
  <rhs><nonterminal def="#ebnf.prim">Primary</nonterminal>
    | <nonterminal def="#ebnf.bracket">BracketAccessExpression</nonterminal>
    | <nonterminal def="#ebnf.arraygen">ArrayGeneratorExpression</nonterminal>
    | <nonterminal def="#ebnf.instexpr">InstanceScopeExpression</nonterminal>
    | <nonterminal def="#ebnf.selexpr">SelectorExpression</nonterminal> '.' Identifier [ <nonterminal def="#ebnf.args">Arguments</nonterminal> ]
    | <nonterminal def="#ebnf.selexpr">SelectorExpression</nonterminal> '.' <literal>new</literal> Identifer <nonterminal def="#ebnf.args">Arguments</nonterminal> [ <nonterminal def="#ebnf.acbody">AnonymousClassBody</nonterminal> ]
  </rhs>
</production>
<production id="ebnf.bracket">
  <lhs>BracketAccessExpression</lhs>
  <rhs><nonterminal def="#ebnf.selexpr">SelectorExpression</nonterminal> '[' <nonterminal def="#ebnf.bracketsel">BracketSelector</nonterminal> ']'
  </rhs>
</production>
<production id="ebnf.bracketsel">
  <lhs>BracketSelector</lhs>
  <rhs>Identifier
    | <nonterminal def="#ebnf.name">Name</nonterminal>
    | <nonterminal def="#ebnf.expr">Expression</nonterminal>
  </rhs>
</production>
</productionset>
<para>
The XL programming language reclassifies these expressions
<orderedlist>
<listitem><para>
as <firstterm>property access expressions</firstterm>
which may access a property
variable of an object (see <xref linkend="s-properties"/>),
</para></listitem>
<listitem><para>
as invocation expressions of operator methods
(<xref linkend="s-operator-methods"/>),
</para></listitem>
<listitem><para>
or as conventional array access expressions.
</para></listitem>
</orderedlist>
These kinds of expressions cannot be distinguished syntactically,
a distinction is made by the compiler
with the help of semantic information.
Namely, the expression
<replaceable>e</replaceable><literal>[</literal><replaceable>i</replaceable><literal>]</literal>
is reclassified by the first applicable of the following rules:
</para>
<orderedlist>
<listitem><para>
If <replaceable>i</replaceable> is a single identifier
<replaceable>n</replaceable>,
<replaceable>e</replaceable> is a property access expression for a
property <replaceable>p</replaceable>,
and <replaceable>p</replaceable> has a subproperty named
<replaceable>n</replaceable>, then the whole expression is a property
access expression for this subproperty.
</para></listitem>
<listitem><para>
If <replaceable>i</replaceable> is a single identifier
<replaceable>n</replaceable>,
<replaceable>e</replaceable> is a local variable access expression for a
query variable <replaceable>v</replaceable> which is wrapped
by a query variable <replaceable>w</replaceable>
(<xref linkend="s-queryvar"/>), and 
if the invocation of the method <methodname>getWrapProperty</methodname> of the
current compile-time model returns a property
<replaceable>q</replaceable> for the type of <replaceable>w</replaceable>,
then the previous rule is checked again, with the expression
<replaceable>e</replaceable> being replaced by a
property access expression for <replaceable>q</replaceable>'s
type-cast property of <replaceable>v</replaceable>'s type on the
instance which is the value of <replaceable>w</replaceable>.
</para></listitem>
<listitem><para>
If <replaceable>i</replaceable> is a single identifier
<replaceable>n</replaceable> and the type of
<replaceable>e</replaceable> has a direct property named
<replaceable>n</replaceable>, then the whole expression is a property
access expression for this property on the
instance which is the result of the evaluation of <replaceable>e</replaceable>.
</para></listitem>
<listitem><para>
If the type of <replaceable>e</replaceable> is an array type,
the index expression undergoes unary numeric promotion;
the promoted type must be <literal>int</literal>.
Now if <replaceable>e</replaceable> is a property access expression for a
property <replaceable>p</replaceable> which has an array type, and if
<replaceable>p</replaceable> has a component property, then the whole
expression is a property access expression for this component property;
its index value is the result of the evaluation of
<replaceable>i</replaceable>.
Otherwise, the whole expression is classified as a conventional array
access expression.
</para></listitem>
<listitem><para>
If none of the above rules is applicable, the whole expression
is reclassified as an invocation expression of an operator method
named <literal>operator$index</literal> (<xref linkend="s-operator-methods"/>).
</para></listitem>
</orderedlist>

<para>
The result of a property access expression is a variable of the type of
the property.
</para>

</section>


<section id="s-arraygen"><title>Array Generator Expressions</title>

<para>
The <firstterm>array generator expression</firstterm> yields all
components of an array as its results.
</para>
<productionset><title>Array Generator Expression</title>
<production id="ebnf.arraygen">
  <lhs>ArrayGeneratorExpression</lhs>
  <rhs><nonterminal def="#ebnf.selexpr">SelectorExpression</nonterminal> '[' ':' ']'
  </rhs>
</production>
</productionset>
<para>
The type of an array generator expression is the component type
of the type of the subexpression. It is a
compile-time error if the latter type is not an array type. 
</para>
<para>
The array generator expression is a generator expression. Its sequential
evaluation evaluates the subexpression. If the result
is <literal>null</literal>, a <classname>NullPointerException</classname>
is thrown. Otherwise, all components of the resulting array
are yielded as variables in ascending order of indices.
</para>

</section>

<section id="s-iscope"><title>Instance Scope Expressions</title>

<para>
An <firstterm>instance scope expression</firstterm> evaluates a
subexpression in the context of an
<firstterm>instance scope</firstterm>. Their use is comparable
to the <literal>with</literal>-statement of the Basic and Pascal
programming languages.
</para>
<productionset><title>Instance Scope Expressions</title>
<production id="ebnf.instexpr">
  <lhs>InstanceScopeExpression</lhs>
  <rhs><nonterminal def="#ebnf.selexpr">SelectorExpression</nonterminal>
       '.' '(' <nonterminal def="#ebnf.expr">Expression</nonterminal> ')'
  </rhs>
</production>
</productionset>
<para>
The compile-time type of the expression <replaceable>e</replaceable>
to the left of the period
has to be a reference type, or a compile-time error
occurs. The evaluation proceeds in several steps:
<orderedlist>
<listitem><para>
The expression <replaceable>e</replaceable> is evaluated.
If the result is <literal>null</literal>,
a <classname>NullPointerException</classname> is thrown.
</para></listitem>
<listitem><para>
The result is assigned to a <literal>final</literal> local variable
<replaceable>i</replaceable> which is implicitly declared by the
instance scope expression and has the compile-time type of
<replaceable>e</replaceable>. The scope of <replaceable>i</replaceable>
is the parenthesised expression. Its name is the special identifier
<methodname>$</methodname> if no local variable of that name is in scope,
or an internal name which cannot conflict with legal identifiers.
</para></listitem>
<listitem><para>
The right expression is evaluated, its result is discarded. Within this
expression, all field and method members of the compile-time type of
<replaceable>i</replaceable> are in scope.
</para></listitem>
<listitem><para>
Type and result of the instance scope expression are those of
<replaceable>i</replaceable>.
</para></listitem>
</orderedlist>
</para>
<para>
The following are examples for instance scope expressions:
</para>
<programlisting>
container.add(new JCheckBox("foo").(setHorizontalAlignment(bar), setFont(myFont)));

System.out.(println(line1), println(line2), println(line3));
</programlisting>

</section>

<section><title>Exponentiation Operator</title>

<para>
The operator <literal>**</literal> is the
<firstterm>expontiation operator</firstterm>. It is
syntactically right-associative (it groups right-to-left), so
<literal>a ** b ** c</literal> is the same as <literal>a ** (b ** c)</literal>.
</para>
<productionset><title>Exponentiation Expression</title>
<production id="ebnf.expo">
  <lhs>ExponentiationExpression</lhs>
  <rhs><nonterminal def="#ebnf.unexpr">UnaryExpression</nonterminal>
       [ '**' <nonterminal def="#ebnf.expo">ExponentiationExpression</nonterminal> ]
  </rhs>
</production>
</productionset>
<para>
Binary floating-point promotion is performed on the operands
(see <xref linkend="s-fp-promotion"/>). The type of an exponentiation
expression is the promoted type of its operands. The result of the expression
is the result of the invocation of the method <methodname>pow</methodname>
in the class <classname>StrictMath</classname> if the expression is
FP-strict, or in the class <classname>Math</classname> otherwise. The left
operand is passed as first argument to the method, the right operand
as second argument. If the type of the expression is <literal>float</literal>,
an additional narrowing primitive conversion to <literal>float</literal>
is applied to the result of the invocation.
</para>

</section>

<section><title>Containment Operator</title>

<para>
The binary <firstterm>containment operator</firstterm>
is represented by the keyword
<literal>in</literal>. It is used for the containment relation
which is on the syntactic level of relational expressions as defined
by the Java programming language.
</para>
<productionset><title>Relational Expressions</title>
<production id="ebnf.rel">
  <lhs>RelationalExpression</lhs>
  <rhs><nonterminal def="#ebnf.shift">ShiftExpression</nonterminal>
       ( { <nonterminal def="#ebnf.relop">RelationalOperator</nonterminal>
           <nonterminal def="#ebnf.shift">ShiftExpression</nonterminal> }
       | <literal>instanceof</literal> <nonterminal def="#ebnf.reftype">ReferenceType</nonterminal>
       )
  </rhs>
</production>
<production id="ebnf.relop">
  <lhs>RelationalOperator</lhs>
  <rhs>'&lt;' | '>' | '&lt;=' | '>=' | <literal>in</literal>
  </rhs>
</production>
</productionset>
<para>
The containment operator may be used for two operands of numeric type,
two operands of type <classname>boolean</classname>,
or two operands that are each of either reference type or the
<classname>null</classname> type.
All other cases result in a compile-time error.
The type of a containment expression is <classname>boolean</classname>.
</para>

<para>
A containment relation <replaceable>e</replaceable> <literal>in</literal>
<replaceable>s</replaceable> is evaluated as follows:
The equality relation
<replaceable>e</replaceable> <literal>==</literal> <replaceable>s</replaceable>
is evaluated sequentially. For each yielded result, it is checked whether
it is <literal>true</literal>. If this is the case, the sequential evaluation
is terminated, and the result
of the containment relation is <literal>true</literal>.
If all comparisons result in <literal>false</literal>, the result is
<literal>false</literal>.
</para>

</section>

<section><title>Guard Operator</title>

<para>
The binary <firstterm>guard operator</firstterm> is represented by the
double colon <literal>::</literal>. The guard expression is a generator
expression and yields the value of the left operand only if
the right operand (the guard) evaluates to <literal>true</literal>.
</para>
<productionset><title>Guard Expression</title>
<production id="ebnf.guard">
  <lhs>GuardExpression</lhs>
  <rhs><nonterminal def="#ebnf.lor">LogicalOrExpression</nonterminal>
       { '::' <nonterminal def="#ebnf.lor">LogicalOrExpression</nonterminal> }
  </rhs>
</production>
</productionset>
<para>
The type of the guard expression is the type of the left operand.
The type of the right operand must be <literal>boolean</literal>,
or a compile-time error occurs.
</para>
<para>
The sequential evaluation of a guard expression evaluates both operands
from left to right. If the result of the evaluation of the right operand
is <literal>true</literal>, the result of the evaluation of the left
operand is yielded as result. Then the sequential evaluation completes.
</para>

</section>

<section id="s-range"><title>Range Operator</title>

<para>
The binary <firstterm>range operator</firstterm> is represented by the
colon <literal>:</literal>. It is
syntactically right-associative (it groups right-to-left).
The range expression yields a contiguous range of integral values.
</para>
<productionset><title>Range Expression</title>
<production id="ebnf.range">
  <lhs>RangeExpression</lhs>
  <rhs><nonterminal def="#ebnf.cond">ConditionalExpression</nonterminal>
       [ ':' <nonterminal def="#ebnf.range">RangeExpression</nonterminal> ]
  </rhs>
</production>
</productionset>
<para>
Binary numeric promotion is performed on the operands. The type of the range
expression is the promoted type. It is a
compile-time error if this type is not <classname>int</classname>
or <classname>long</classname>. 
</para>
<para>
The range expression is a generator expression. Its sequential
evaluation evaluates both operands and yields all values of its
type between the value of the first and the value of the
second operand in ascending order, including the boundary values. If the
value of the first operand is greater than the value of the second
operand, no value is yielded.
</para>

</section>


<section id="s-elist"><title>Expression Lists</title>

<para>
An <firstterm>expression list</firstterm> consists of a number of
comma-separated subexpressions. It evaluates them from left to right,
type and result are those of the last (right-most) expression. This is
similar to the concatenation of expressions with the comma operator
in the C programming language.
</para>
<productionset><title>Expression Lists</title>
<production id="ebnf.expr">
  <lhs>Expression</lhs>
  <rhs><nonterminal def="#ebnf.assignment">AssignmentExpression</nonterminal> |
       <nonterminal def="#ebnf.elist">ExpressionList</nonterminal>
  </rhs>
</production>
<production id="ebnf.elist">
  <lhs>ExpressionList</lhs>
  <rhs><nonterminal def="#ebnf.svardecl">SingleVariableDeclaration</nonterminal>
    | <nonterminal def="#ebnf.listexpr">ListExpression</nonterminal>
      ',' <nonterminal def="#ebnf.listexpr">ListExpression</nonterminal>
      { ',' <nonterminal def="#ebnf.listexpr">ListExpression</nonterminal> }
  </rhs>
</production>
<production id="ebnf.listexpr">
  <lhs>ListExpression</lhs>
  <rhs><nonterminal def="#ebnf.assignment">AssignmentExpression</nonterminal> |
       <nonterminal def="#ebnf.svardecl">SingleVariableDeclaration</nonterminal>
  </rhs>
</production>
</productionset>
<para>
As an addition to the comma operator of the C programming language,
local variables can be declared in expression lists.
The scope of these variables is the rest
of the expression list, starting with the initializer of the local variable.
</para>

</section>

</chapter>
