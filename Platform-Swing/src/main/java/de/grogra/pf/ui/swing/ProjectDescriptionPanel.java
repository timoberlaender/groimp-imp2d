package de.grogra.pf.ui.swing;

import de.grogra.pf.io.FilterSource;
import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.*;
import de.grogra.pf.ui.registry.FilterSourceFactory;
import de.grogra.pf.ui.registry.ProjectDirectory;
import de.grogra.util.ComparableVersion;
import de.grogra.util.Described;
import de.grogra.util.StringMap;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class ProjectDescriptionPanel implements ComponentWrapper {

    //The panel that contains all the elements
    Component component;
    // the description of the project (author, main desc, related project, ...)
    StringBuilder htmlComponent;

    Context ctx;

    ProjectDescriptionPanel(Context ctx, Item project){
        if (!(project instanceof ProjectDirectory)){
            return;
        }
        this.ctx=ctx;
        htmlComponent = new StringBuilder();
        component = createPanel((ProjectDirectory)project);
    }

    @Override
    public Object getComponent() {
        return component;
    }

    @Override
    public void dispose() {
        if (component!=null){
            component=null;
            htmlComponent = null;
            ctx=null;
        }
    }


    private JPanel createPanel(ProjectDirectory project){

        JPanel title = createTitle(project);
        JScrollPane desc = new JScrollPane(createDescription(project));
        JPanel versions = createVersions(project);
        JPanel p = new JPanel(new BorderLayout());
        p.setPreferredSize(new Dimension(800, 600));
        p. add(title, BorderLayout.NORTH);
        p. add(desc, BorderLayout.CENTER);
        p. add(versions, BorderLayout.SOUTH);
        return p;
    }


       private JPanel createDescription(ProjectDirectory project){

        JPanel p = new JPanel();
        p.setLayout(new BoxLayout(p,BoxLayout.PAGE_AXIS));

    	
        int h=650,w=650;
        ImageIcon icon=null;
        Image test = null;
        Object o = project.getImage();
        if (o instanceof ImageAdapter) {
            try {
                ImageAdapter image = (ImageAdapter) o;
             	BufferedImage buffImg = image.getBufferedImage();
                if(image.getPreferredIconSize(false).height>h || image.getPreferredIconSize(false).width>w) {
                	test = buffImg.getScaledInstance(h, w, Image.SCALE_SMOOTH);
                	icon = new ImageIcon(test,"image");
                }else {
                	icon = new ImageIcon(buffImg);
                }
            	h = Math.min(icon.getIconHeight(), h);
                w = Math.min(icon.getIconWidth(), w);
                JLabel Pic = new JLabel(icon);
                Pic.setPreferredSize(new Dimension(h, w));
                p.add(Pic);
                Pic.setAlignmentX(Component.CENTER_ALIGNMENT);

            } catch(Exception e) {e.printStackTrace();}
        }
        createHTMLDescription(project);
        JLabel descL = new JLabel(htmlComponent.toString());
        JScrollPane scrollDesc = new JScrollPane(descL);
        p.add(scrollDesc);
        return p;
    }

    private void createHTMLDescription(ProjectDirectory project) {
        htmlComponent.append("<html>");
        htmlComponent.append("<body >");
        htmlComponent.append(System.getProperty("line.separator"));

        if (project.getShorDescription()!=null){
            htmlComponent.append("<p>").append((String)project.getShorDescription()).append("</p>");
            htmlComponent.append(System.getProperty("line.separator"));
        }
        htmlComponent.append("<br>");
        if (project.getAuthors()!=null){
            htmlComponent.append("<p>").append("Author(s): ");
            Set<String> list = new HashSet<>(Arrays.asList(project.getAuthors()));
            htmlComponent.append(list.stream().collect(Collectors.joining(", ")));
            htmlComponent.append("</p>");
            htmlComponent.append(System.getProperty("line.separator"));
        }
        if (project.getTags()!=null){
            htmlComponent.append("<p>").append("Tag(s): ");
            Set<String> list = new HashSet<>(Arrays.asList(project.getTags()));
            htmlComponent.append(list.stream().collect(Collectors.joining(", ")));
            htmlComponent.append("</p>");
            htmlComponent.append(System.getProperty("line.separator"));
        }

        htmlComponent.append("</body>").append("</html>");
    }

    private JPanel createTitle(ProjectDirectory project){

        JPanel p = new JPanel();
        String name= (String)project.getDescription(Described.NAME);
        if (project.getProjectName()!=null){
            name = (String)project.getProjectName();
        }
        p.add(new JLabel( "<html><h1>"+name+"</h1></html>" ));
        p.setPreferredSize(new Dimension(30, 70));
        return p;
    }

    private JPanel createVersions(ProjectDirectory project){

        ArrayList<ComparableVersion> versions = project.getVersions();
        JPanel p = new JPanel();
        JComboBox<ComparableVersion> versionList = new JComboBox<ComparableVersion>();
        JButton openButton = new JButton("Open");
        JLabel t = new JLabel("Select version: ");

        p.add(t);
        p.add(versionList);
        p.add(openButton);

        if (versions.isEmpty()){
            // Make a unselectable droplist
            // and grayed button
            // and return

        }
        else{
            // versionList.addActionListener(this);
            versionList.putClientProperty("JComboBox.isTableCellEditor", Boolean.TRUE);
            for (ComparableVersion v : versions){
                versionList.addItem(v);
            }

            openButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            ComparableVersion selected = (ComparableVersion) versionList.getSelectedItem();
                            if (selected!=null){
                                Item p = project.getProjectForVersion(ctx, selected);
                                if (p!=null &&
                                    p instanceof FilterSourceFactory){
                                    StringMap m = new StringMap();
                                    Object x = ((FilterSourceFactory) p).evaluate(ctx.getWorkbench()
                                            .getMainWorkbench().getRegistry(),m);
                                    if(x!=null ) {
                                        m.putBoolean(Workbench.START_AS_DEMO, Boolean.TRUE);
                                        Workbench w = ctx.getWorkbench().open((FilterSource)x,m);
                                    }
                                }
                            }
                        }
                    });

                }
            });
        }
        return p;
    }

    public static BufferedImage getBufferedImageForURL(String imgUrl, boolean fromWeb)  {
        InputStream is=null;
        BufferedImage ia =null;
        try {
            if (fromWeb) {
                URL url = new URL(imgUrl);
                is = url.openStream();
            }
            else {
                File f = new File(imgUrl);
                is = new FileInputStream(f);
            }
            ia = ImageIO.read(is);
        } catch(MalformedURLException e) {} catch (IOException e) {}
        finally {
            if (is!=null) { try {
                is.close();
            } catch (IOException e) {}}
        }
        return ia;
    }
}