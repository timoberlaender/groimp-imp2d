package de.grogra.pf.ui.swing;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.TransferHandler;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.JobManager;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.event.ActionEditEvent;
import de.grogra.pf.ui.tree.SyncMappedTree;
import de.grogra.pf.ui.tree.UINodeHandler;
import de.grogra.util.MappedTree;
import de.grogra.pf.registry.Item;

class TreeTransferHandler extends TransferHandler {
    private static final long serialVersionUID = -6817955101045751121L;
	DataFlavor nodesFlavor;
    DataFlavor[] flavors = new DataFlavor[1];
    SwingTree originDragTree;

    public TreeTransferHandler() {
        try {
            String mimeType = DataFlavor.javaJVMLocalObjectMimeType +
                    ";class=\"" +
                    MappedTree.Node[].class.getName() +
                    "\"";
            nodesFlavor = new DataFlavor(mimeType);
            flavors[0] = nodesFlavor;
        } catch(ClassNotFoundException e) {
            System.out.println("ClassNotFound: " + e.getMessage());
        }
    }

    public boolean canImport(TransferHandler.TransferSupport support) {
        if(!support.isDrop()) {
            return false;
        }
        support.setShowDropLocation(true);
        if(!support.isDataFlavorSupported(nodesFlavor)) {
            return false;
        }
        // Do not allow a drop on the drag source selections.
        SwingTree.DropLocation dl =
                (SwingTree.DropLocation)support.getDropLocation();
        SwingTree tree = (SwingTree)support.getComponent();
        int dropRow = tree.getRowForPath(dl.getPath());
        int[] selRows = tree.getSelectionRows();
        for(int i = 0; i < selRows.length; i++) {
            if(selRows[i] == dropRow) {
                return false;
            }
            MappedTree.Node treeNode =
                    (MappedTree.Node)tree.getPathForRow(selRows[i]).getLastPathComponent();
            if (tree.getRowForPath(new TreePath(((MappedTree.Node)treeNode).getPath())) == dropRow) {
                return false;
            }  
        }
        // Do not allow to drop in another swingtree
        if (tree != originDragTree) {
	    	return false;
	    }
        return true;
    }

    protected Transferable createTransferable(JComponent c) {
    	SwingTree tree = (SwingTree) c;
    	originDragTree = tree;
	    TreePath[] paths = tree.getSelectionPaths();
	    if (paths == null) {
	    	return null;
	    }
        List<MappedTree.Node> copies =new ArrayList<MappedTree.Node>();
        MappedTree.Node firstNode =(MappedTree.Node) paths[0].getLastPathComponent();
        HashSet<TreeNode> doneItems = new LinkedHashSet<>(paths.length);
        copies.add(firstNode);
        MappedTree.Node[] nodes =
                copies.toArray(new MappedTree.Node[copies.size()]);
        return new NodesTransferable(nodes);
    }

    protected void exportDone(JComponent source, Transferable data, int action) {
    	return;
    }

    public int getSourceActions(JComponent c) {
        return COPY_OR_MOVE;
    }

    public boolean importData(TransferHandler.TransferSupport support) {
        if(!canImport(support)) {
            return false;
        }
        // Extract transfer data.
        SwingTree tree = (SwingTree)support.getComponent();
        Context c = tree.getUITree ().getContext ();
        MappedTree.Node[] toMoveNodes = null;

        try {
            Transferable t = support.getTransferable();
            toMoveNodes = (MappedTree.Node[])t.getTransferData(nodesFlavor);
        } catch(UnsupportedFlavorException ufe) {
            System.out.println("UnsupportedFlavor: " + ufe.getMessage());
        } catch(java.io.IOException ioe) {
            System.out.println("I/O error: " + ioe.getMessage());
        }
        // Get drop location info.
        JTree.DropLocation dl =
                (JTree.DropLocation)support.getDropLocation();
        int childIndex = dl.getChildIndex();
        TreePath dest = dl.getPath();
        MappedTree.Node parent =
                (MappedTree.Node)dest.getLastPathComponent();
        SyncMappedTree model = (SyncMappedTree)tree.getModel();
        // Configure for drop mode.
        int index = childIndex;    // DropMode.INSERT
        if(childIndex == -1) {     // DropMode.ON
            index = parent.getChildCount();
        }
        // Add data to model.
        for(int i = 0; i < toMoveNodes.length; i++) {
        	UI.getJobManager (c).runLater
				(tree, new ActionEditEvent (UINodeHandler.ACTION_MOVE, parent.getSourceNode(), 0)
				 .set (c, SwingTree.getNode (toMoveNodes[i])), c, JobManager.ACTION_FLAGS);
        }
        return true;
    }

    public String toString() {
        return getClass().getName();
    }

    public class NodesTransferable implements Transferable {
    	MappedTree.Node[] nodes;

        public NodesTransferable(MappedTree.Node[] nodes) {
            this.nodes = nodes;
        }

        public Object getTransferData(DataFlavor flavor)
                throws UnsupportedFlavorException {
            if(!isDataFlavorSupported(flavor))
                throw new UnsupportedFlavorException(flavor);
            return nodes;
        }

        public DataFlavor[] getTransferDataFlavors() {
            return flavors;
        }

        public boolean isDataFlavorSupported(DataFlavor flavor) {
            return nodesFlavor.equals(flavor);
        }
    }
}