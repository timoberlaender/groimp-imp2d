module pov.ray {
	exports de.grogra.ext.povray;

	requires graph;
	requires imp;
	requires imp3d;
	requires math;
	requires platform;
	requires platform.core;
	requires raytracer;
	requires utilities;
	requires vecmath;
	requires xl.core;
	requires java.desktop;
	requires java.xml;
}