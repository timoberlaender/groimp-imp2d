
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.ext.povray;

import java.io.*;
import javax.vecmath.*;
import de.grogra.imp3d.objects.*;
import de.grogra.imp3d.shading.*;
import de.grogra.pf.io.*;

public class POVWriter extends IndentWriter
{

	public POVWriter (Writer out)
	{
		super (out, 1);
	}


	public void print (float x, float y) throws IOException
	{
		checkIndentation ();
		write ('<');
		print (x);
		write (',');
		print (y);
		write ('>');
	}


	public void print (Tuple3f t) throws IOException
	{
		checkIndentation ();
		write ('<');
		print (t.x);
		write (',');
		print (t.y);
		write (',');
		print (t.z);
		write ('>');
	}


	public void print (float x, float y, float z) throws IOException
	{
		checkIndentation ();
		write ('<');
		print (x);
		write (',');
		print (y);
		write (',');
		print (z);
		write ('>');
	}


	public void print (Tuple3d t) throws IOException
	{
		checkIndentation ();
		write ('<');
		print (t.x);
		write (',');
		print (t.y);
		write (',');
		print (t.z);
		write ('>');
	}


	public void print (Tuple4f t) throws IOException
	{
		checkIndentation ();
		write ('<');
		print (t.x);
		write (',');
		print (t.y);
		write (',');
		print (t.z);
		write (',');
		print (t.w);
		write ('>');
	}


	public void print (Tuple4d t) throws IOException
	{
		checkIndentation ();
		write ('<');
		print (t.x);
		write (',');
		print (t.y);
		write (',');
		print (t.z);
		write (',');
		print (t.w);
		write ('>');
	}


	public void print (float x, float y, float z, float w) throws IOException
	{
		checkIndentation ();
		write ('<');
		print (x);
		write (',');
		print (y);
		write (',');
		print (z);
		write (',');
		print (w);
		write ('>');
	}


	public void printRGB (int rgba) throws IOException
	{
		checkIndentation ();
		write ('<');
		printRGBValues (rgba);
		write ('>');
	}
	
	
	public void printRGBValues (int rgba) throws IOException
	{
		print (((rgba >> 16) & 255) * (1f / 255));
		write (',');
		print (((rgba >> 8) & 255) * (1f / 255));
		write (',');
		print ((rgba & 255) * (1f / 255));
	}


	public void printRGBT (int rgba) throws IOException
	{
		checkIndentation ();
		write ('<');
		printRGBValues (rgba);
		write (',');
		print ((255 - ((rgba >> 24) & 255)) * (1f / 255));
		write ('>');
	}


	public void print (SceneTree.InnerNode transform) throws IOException
	{
		print ("matrix <");
		print (transform.m00);
		write (',');
		print (transform.m10);
		write (',');
		print (transform.m20);
		write (',');
		print (transform.m01);
		write (',');
		print (transform.m11);
		write (',');
		print (transform.m21);
		write (',');
		print (transform.m02);
		write (',');
		print (transform.m12);
		write (',');
		print (transform.m22);
		write (',');
		print (transform.m03);
		write (',');
		print (transform.m13);
		write (',');
		print (transform.m23);
		write ('>');
	}


	public void print (Matrix4d transform) throws IOException
	{
		print ("matrix <");
		print (transform.m00);
		write (',');
		print (transform.m10);
		write (',');
		print (transform.m20);
		write (',');
		print (transform.m01);
		write (',');
		print (transform.m11);
		write (',');
		print (transform.m21);
		write (',');
		print (transform.m02);
		write (',');
		print (transform.m12);
		write (',');
		print (transform.m22);
		write (',');
		print (transform.m03);
		write (',');
		print (transform.m13);
		write (',');
		print (transform.m23);
		write ('>');
	}


	public void print (Matrix3f transform) throws IOException
	{
		print ("matrix <");
		print (transform.m00);
		write (',');
		print (transform.m10);
		write (',');
		print (transform.m20);
		write (',');
		print (transform.m01);
		write (',');
		print (transform.m11);
		write (',');
		print (transform.m21);
		write (',');
		print (transform.m02);
		write (',');
		print (transform.m12);
		write (',');
		print (transform.m22);
		write (",0,0,0>");
	}


	public void print (AffineUVTransformation t) throws IOException
	{
		float u = t.getShear (), v;
		if (u != 0)
		{
			print ("matrix <1,0,0,");
			print (u);
			print (",1,0,0,0,1,0,0,0>");
			println ();
		}
		u = t.getScaleU ();
		v = t.getScaleV ();
		if ((u != 1) || (v != 1))
		{
			print ("scale ");
			print (1 / u, 1 / v, 1);
			println ();
		}
		u = t.getAngle ();
		if (u != 0)
		{
			print ("rotate ");
			print (0, 0, u * (float) (180 / Math.PI));
			println ();
		}
		u = t.getOffsetU ();
		v = t.getOffsetV ();
		if ((u != 0) || (v != 0))
		{
			print ("translate ");
			print (u, v, 0);
			println ();
		}
	}

}
