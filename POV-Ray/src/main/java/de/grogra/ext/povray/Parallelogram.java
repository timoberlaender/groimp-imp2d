
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.ext.povray;

import java.io.IOException;

import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4d;
import javax.vecmath.Vector3f;

import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.SceneTree.InnerNode;
import de.grogra.imp3d.objects.SceneTreeWithShader.Leaf;
import de.grogra.imp3d.shading.Phong;
import de.grogra.vecmath.Math2;

public class Parallelogram extends POVNode
{
	public Parallelogram ()
	{
		super ("polygon");
	}


	@Override
	protected void exportImpl (Object object, Leaf leaf, POVExport export)
		throws IOException
	{
		export.out.print ("5,<0,0>,<0,1>,<1,1>,<1,0>,<0,0>");
		export.out.println ();
	}


	@Override
	protected void exportTextureHeader (Phong mat, Leaf leaf, POVExport export)
		throws IOException
	{
	}


	@Override
	protected void exportTransform (Leaf leaf, InnerNode transform,
									POVExport export) throws IOException
	{
		Matrix4d t = export.pool.m4d0;
		Math2.makeAffine (t);
		Vector3f a1 = export.pool.v3f0;
		Vector3f a2 = export.pool.v3f1;
		a1.set ((Vector3f) leaf.getObject (a2, Attributes.AXIS));
		t.m03 = -a1.x;
		t.m13 = -a1.y;
		t.m23 = -a1.z;
		a1.scale (2);
		a2.set (0, 0, (float) leaf.getDouble (Attributes.LENGTH));
		Matrix3f m = export.pool.m3f0;
		m.setColumn (0, a1);
		m.setColumn (1, a2);
		a1.cross (a1, a2);
		m.setColumn (2, a1);
		t.setRotationScale (m);
		export.out.print (t);
		export.out.println ();
		if (transform != null)
		{
			export.out.print (transform);
			export.out.println ();
		}
	}

}
