
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.ext.povray;

import java.io.*;
import javax.vecmath.*;
import de.grogra.imp3d.objects.*;

public class Frustum extends POVNode
{
	public Frustum ()
	{
		super ("cone");
	}


	@Override
	protected void exportImpl (Object object, SceneTreeWithShader.Leaf leaf, POVExport export)
		throws IOException
	{
		Vector3f v = export.pool.v3f0;
		v.set (0, 0, (float) leaf.getDouble (Attributes.LENGTH));
		boolean valid = v.lengthSquared () > export.getView ().getEpsilonSquared ();
		if (!valid)
		{
			v.set (0, 0, 1);
		}
		export.out.print ("0,");
		export.out.print (leaf.getFloat (Attributes.BASE_RADIUS));
		export.out.write (',');
		export.out.print (v);
		export.out.write (',');
		export.out.print (leaf.getFloat (Attributes.TOP_RADIUS));
		export.out.println ();
		if (!valid)
		{
			export.out.print ("scale ");
			export.out.print (1, 1, export.getView ().getEpsilon ());
			export.out.println ();
		}
	}

}
