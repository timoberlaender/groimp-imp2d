module graph {
	exports de.grogra.persistence;
	exports de.grogra.graph.impl;
	exports de.grogra.graph;

	requires utilities;
	requires xl.core;
	requires java.desktop;
	requires java.xml;
	
	opens de.grogra.graph to utilities;
	opens de.grogra.graph.impl to utilities;
}