/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.imp;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.vecmath.Tuple3f;

import de.grogra.graph.impl.GraphManager;
import de.grogra.graph.impl.Node;
import de.grogra.imp.io.ImageWriter;
import de.grogra.imp.net.ClientConnection;
import de.grogra.imp.net.Commands;
import de.grogra.imp.net.Connection;
import de.grogra.imp.objects.FixedImageAdapter;
import de.grogra.persistence.PersistenceBindings;
import de.grogra.pf.boot.Main;
import de.grogra.pf.io.FileSource;
import de.grogra.pf.io.FileTypeItem;
import de.grogra.pf.io.FileTypeItem.Filter;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.io.ObjectSourceImpl;
import de.grogra.pf.io.OutputStreamSource;
import de.grogra.pf.io.RegistryLoader;
import de.grogra.pf.registry.*;
import de.grogra.pf.registry.expr.Expression;
import de.grogra.pf.ui.Command;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.FileChooserResult;
import de.grogra.pf.ui.JobManager;
import de.grogra.pf.ui.Project;
import de.grogra.pf.ui.ProjectManager;
import de.grogra.pf.ui.ProjectWorkbench;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.UIApplication;
import de.grogra.pf.ui.UIProperty;
import de.grogra.pf.ui.Window;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.WorkbenchManager;
import de.grogra.pf.ui.event.ActionEditEvent;
import de.grogra.pf.ui.registry.CommandItem;
import de.grogra.pf.ui.registry.CommandPlugin;
import de.grogra.pf.ui.registry.ProjectFactory;
import de.grogra.pf.ui.registry.SourceDirectory;
import de.grogra.pf.ui.registry.SourceFile;
import de.grogra.projectmanager.ProjectFactoryImpl;
import de.grogra.projectmanager.ProjectManagerImpl;
import de.grogra.pf.ui.HeadlessToolkit;
import de.grogra.pf.ui.UIToolkit;
import de.grogra.reflect.Type;
import de.grogra.util.*;
import de.grogra.vfs.FileSystem;
import de.grogra.vfs.MemoryFileSystem;
import de.grogra.xl.lang.ObjectConsumer;
import de.grogra.xl.util.ObjectList;

public final class IMP extends UIApplication implements CommandPlugin, ResourceConverter, WorkbenchManager
{
	public static final I18NBundle I18N = I18NBundle.getInstance (IMP.class);

	public static final MimeType TYPES_MIME_TYPE = MimeType
		.valueOf (Type[].class);
	public static final IOFlavor TYPES_FLAVOR = new IOFlavor (TYPES_MIME_TYPE);
	
	// wb manager id given to  wb
	private static int wbID;

	public static final Command CLOSE = new Command ()
	{
		@Override
		public void run (Object info, Context ctx)
		{
			((IMPWorkbench) ctx.getWorkbench ()).close ((Command) info);
		}

		@Override
		public String getCommandName ()
		{
			return null;
		}
	};

	private static IMP PLUGIN;

	public static IMP getInstance ()
	{
		return PLUGIN;
	}

	private final ObjectList workbenches = new ObjectList (4, false);
//	private IMPWorkbench mainWorkbench;
//	private UIToolkit ui;

	public IMP ()
	{
		assert PLUGIN == null;
		PLUGIN = this;
	}

	@Override
	public void startup() {
		super.startup();
		I18NBundle.addResourceConverter(this);
	}

	public boolean canHandleConversion(String name) {
		return "image".equals(name);
	}

	public Object convert(String name, String argument, I18NBundle bundle) {
		if ("image".equals(name)) {
			FixedImageAdapter image=null;
			InputStream in=null;
			try {
				in =bundle.getClassLoader().getResourceAsStream(argument);
				if (in != null) {
					BufferedImage img = ImageIO.read(in);
					if (img!=null){
						image = new FixedImageAdapter(img);
					}
				}
			} catch (IOException e) {
				throw new RuntimeException(e);
			} finally {
				if (in !=null){
					try{in.close();}
					catch (IOException e){}
				}
			}
			return image;
		} else {
			throw new IllegalArgumentException(name);
		}
	}


	public static void run (Application app)
	{
		getInstance ().runImpl (app);
	}

	private void runImpl (Application app)
	{
		UIToolkit ui;
		
		if ("true".equals (Main.getProperty ("headless")))
		{
			ui = new HeadlessToolkit ();
		}
		else
		{
			Expression e = (Expression) getRegistry ().getItem ("/ui/toolkits");
			Object o;
			if ((e == null)
				|| !((o = e.evaluate (this, new StringMap ().putObject ("registry",
					getRegistry ()))) instanceof UIToolkit))
			{
				de.grogra.pf.boot.Main.showMessage ("No UI toolkit found", true);
				return;
			}
			ui = (UIToolkit) o;
		}
		// TODO: change this bad implementation to properly do the init
		ProjectManager projm = ProjectManagerImpl.getInstance();
		PLUGIN.init(projm, ui, PLUGIN);
		try {
			setMainWorkbench( createWorkbench(new ProjectFactoryImpl()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		((IMPJobManager)getMainWorkbench().getJobManager ()).run ();
		setMainWorkbench(null);
	}
	;
	void registerWorkbench (IMPWorkbench wb)
	{
		synchronized (workbenches)
		{
			workbenches.add (wb);
		}
	}

	public void start (final IMPWorkbench workbench, Window feedback)
	{
		boolean hide;
		synchronized (workbenches)
		{
			hide = workbenches.size () == 2;
		}
		if (hide)
		{
			getMainWorkbench().getJobManager ().runLater (new Command ()
			{
				@Override
				public void run (Object info, Context ctx)
				{
					Window w = ctx.getWindow ();
					if ((w != null) && (w.getPanels (null).length == 0))
					{
						w.hide ();
					}
					((IMPJobManager)workbench.getJobManager ()).start (null);
				}

				@Override
				public String getCommandName ()
				{
					return null;
				}
			}, null, getMainWorkbench(), JobManager.UPDATE_FLAGS);
		}
		else
		{
			((IMPJobManager)workbench.getJobManager ()).start (feedback);
		}
	}

	void deregisterWorkbench (IMPWorkbench wb, Window window)
	{
		synchronized (workbenches)
		{
			workbenches.remove (wb);
			if (workbenches.size () == 1)
			{
				getMainWorkbench().getJobManager ().runLater (new Command ()
				{
					@Override
					public void run (Object info, Context ctx)
					{
						if (ctx.getWorkbench ().isHeadless ())
						{
							return;
						}
						ctx.getWindow ().show (true, null);
					}

					@Override
					public String getCommandName ()
					{
						return null;
					}
				}, null, getMainWorkbench(), JobManager.UPDATE_FLAGS);
			}
		}
	}

	public void exit ()
	{
		IMPWorkbench w;
		synchronized (workbenches)
		{
			if (workbenches.isEmpty ())
			{
				return;
			}
			w = (IMPWorkbench) workbenches.peek (1);
		}
		w.getJobManager ().execute (CLOSE, new Command ()
		{
			@Override
			public void run (Object i, Context c)
			{
				exit ();
			}

			@Override
			public String getCommandName ()
			{
				return null;
			}
		}, w, JobManager.UI_PRIORITY);
	}

	//done by the project when created
//	public static void loadRegistry (Registry reg, RegistryLoader loader,
//			boolean project) throws IOException
//	{
//		StringMap m = new StringMap ().putObject ("registry", reg);
//		if (project)
//		{
//			((Item) reg.getRoot ()).add (new Directory ("project"));
//		}
//		Executable.runExecutables (reg.getRootRegistry (), "/hooks/configure",
//			reg, m);
//		if (loader != null)
//		{
//			loader.loadRegistry (reg);
//		}
//		Executable.runExecutables (reg.getRootRegistry (), "/hooks/complete",
//			reg, m);
//		reg.activateItems ();
//	}

	@Override
	public void run (Object info, Context ctx, CommandItem item)
	{
		final IMPWorkbench wb = (IMPWorkbench) ctx.getWorkbench ();
		String n = item.getName ();
		if ("close".equals (n))
		{
			CLOSE.run (null, ctx);
		}
		else if ("exit".equals (n))
		{
			exit ();
		}
		else if ("aboutsoftware".equals (n))
		{
			wb.showAboutAppDialog (getPluginDescriptor ());
		}
		else if ("export".equals (n))
		{
			if (!(info instanceof ActionEditEvent))
			{
				return;
			}
			ActionEditEvent e = (ActionEditEvent) info;
			if (e.isConsumed () || !(e.getPanel () instanceof View))
			{
				return;
			}
			ctx.getWorkbench ().export (toFilterSource ((View) e.getPanel ()));
		}
		else if ("snapshot".equals (n))
		{
			if (!(info instanceof ActionEditEvent))
			{
				return;
			}
			ActionEditEvent e = (ActionEditEvent) info;
			if (e.isConsumed () || !(e.getPanel () instanceof View))
			{
				return;
			}
			final View v = (View) e.getPanel ();
			
			class Snapshot implements Command, ObjectConsumer<RenderedImage>
			{
				@Override
				public String getCommandName ()
				{
					return null;
				}

				@Override
				public void run (Object info, Context context)
				{
					IOFlavor flavor = ImageWriter.RENDERED_IMAGE_FLAVOR;
					Filter filter = null;
					Filter[] fileFilter = IO.getWritableFileTypes (flavor);
					if (fileFilter != null) {
						for (int i = 0; i < fileFilter.length; i++) {
							FileTypeItem fti = (fileFilter[i].getItem());
							MimeType mt = fti.getMimeType();
							if (mt.equals(MimeType.PNG)) {
								filter = fileFilter[i];
								break;
							}
						}
					}
					FileChooserResult fr = wb.chooseFileToSave (
							I18N.msg ("snapshot.title"), flavor, filter);
					if (fr == null)
					{
						return;
					}
					writeImage (v, (RenderedImage) info, fr.getMimeType (), fr.file);
				}

				@Override
				public void consume (RenderedImage value)
				{
					wb.getJobManager ().runLater (this, value, v, JobManager.ACTION_FLAGS);
				}
				
			}

			v.getViewComponent ().makeSnapshot (new Snapshot ());
		}
	}

	private static FilterSource toFilterSource (View view)
	{
		return new ObjectSourceImpl (view, "view", view.getFlavor (), view
			.getWorkbench ().getRegistry ().getRootRegistry (), null);
	}

	public static void export (View view, MimeType mt, File file)
	{
		view.getWorkbench ().export (toFilterSource (view), mt, file);
	}

	public static void writeImage (Image img, File file)
	{
		if (!(img instanceof RenderedImage))
		{
			throw new IllegalArgumentException ("image has to be instance of RenderedImage");
		}
		Workbench w = Workbench.current ();
		if (w == null)
		{
			throw new IllegalStateException ("no current workbench");
		}
		FileTypeItem i = FileTypeItem.get (w, file.getName ());
		if (i == null)
		{
			throw new UnsupportedOperationException ("unsupported image format of " + file);
		}
		writeImage (w, (RenderedImage) img, i.getMimeType (), file);
	}

	public static void writeImage (Context ctx, RenderedImage r, MimeType mt,
			File file)
	{
		try
		{
			OutputStream out = new BufferedOutputStream (new FileOutputStream (file));
			writeImage (ctx, r, mt, out, file.getPath ());
			out.flush ();
			out.close ();
		}
		catch (IOException e)
		{
			ctx.getWorkbench ().logGUIInfo (I18N.msg ("snapshot.failed", file), e);
		}
	}

	public static void writeImage (Context ctx, RenderedImage r, MimeType mt,
			OutputStream out, String outName)
	{
		FilterSource fs = IO.createPipeline (new ObjectSourceImpl (r,
			"snapshot", ImageWriter.RENDERED_IMAGE_FLAVOR, ctx.getWorkbench ()
				.getRegistry ().getRootRegistry (), null), new IOFlavor (mt,
			IOFlavor.OUTPUT_STREAM, null));
		if (fs == null)
		{
			ctx.getWorkbench ().logGUIInfo (
				IO.I18N.msg ("save.unsupported", outName, IO.getDescription (mt)));
			return;
		}
		try
		{
			((OutputStreamSource) fs).write (out);
		}
		catch (IOException ex)
		{
			ctx.getWorkbench ().logGUIInfo (
				I18N.msg ("snapshot.failed", outName), ex);
		}
	}

	public static void closeWorkbench (Context ctx)
	{
		UI.getJobManager (ctx).runLater (CLOSE, null, ctx,
			JobManager.ACTION_FLAGS);
	}


	public static void openClientWorkbench (Item item, Object info,
			Context context) throws IOException
	{
		Socket s = Commands.getSocket (context, "localhost:58090");
		if (s != null)
		{
			Connection cx = new Connection (s);
			cx.start ();
			openClientWorkbench (cx, context);
		}
	}

	public static void openClientWorkbench (final Connection server, Context context)
			throws IOException
	{
		final IMPWorkbench wb = (IMPWorkbench) context.getWorkbench ();
		wb.removeConnection (server);
		final Registry r = new Registry (getInstance ().getRegistry ());

		final ClientConnection conn = new ClientConnection (new PersistenceBindings (
			r, r), server);

		r.createGraphs (conn);

		new Thread (new Runnable ()
		{
			@Override
			public void run ()
			{
				try
				{
					conn.initialize (r.getRegistryGraph ());
					r.initialize ();
					r.initFileSystem (new MemoryFileSystem (de.grogra.pf.io.IO.PROJECT_FS));
				}
				catch (IOException e)
				{
					wb.logGUIInfo (null, e);
					return;
				}
				IMPWorkbench w = new IMPWorkbench (null);
				Registry.setCurrent (r);
				try
				{
//					loadRegistry (r, null, true);
					//TODO/
					conn.initialize (r.getProjectGraph ());
					w.setName (conn.getName () + '@'
						+ server.getSocket ().getRemoteSocketAddress ());
					w.ignoreIfModified ();
					getInstance ().start (w, wb.getWindow ());
					w.addConnection (server);
				}
				catch (IOException e)
				{
					wb.logGUIInfo (null, e);
				}
			}
		}, "OpenClientWorkbench@" + server).start ();
	}

	public static Object getFileToAdd (Context ctx)
	{
		Workbench wb = ctx.getWorkbench ();
		FileChooserResult fr = wb.getToolkit().chooseFile (null,
			IO.getReadableFileTypes (new IOFlavor [] {IOFlavor.RESOURCE_LOADER}), Window.ADD_FILE, false, null, ctx, null); 
		if (fr == null)
		{
			return null;
		}
		MimeType mt = fr.getMimeType ();
		if (fr.files != null && fr.files.length > 1) {
			ObjectList<Item> l = new ObjectList<Item>(fr.files.length);
			for (File f : fr.files) {
				l.add( toSourceFile (f, mt, ctx));
			}
			return l;
		}
		 {
			if (fr.file.exists ())
			{
				switch (ctx.getWindow ().showChoiceDialog (fr.file.getName (), UI.I18N,
					"addfiledialog", new String[] {"add", "link"}))
				{
					case 0:
						break;
					case 1:
						return new SourceFile (fr.file.getName(), mt, IO.toSystemId (fr.file));
					default:
						return null;
				}
			}
			return toSourceFile (fr.file, mt, ctx); // used when file are linked not added
		}
	}
	
	/**
	 * 
	 * @param file
	 * @param mt
	 * @param ctx
	 * @param dest An Item, the node where the file will be added
	 * @return
	 */
	public static SourceFile toSourceFile (File file, MimeType mt, Context ctx, Object dest)
	{
		FileSystem fs = ctx.getWorkbench ().getRegistry ().getFileSystem ();
		Object f;
		if (dest == null) 
			dest = UIProperty.WORKBENCH_SELECTION.getValue(ctx.getWorkbench());
		if (dest instanceof SourceDirectory)
			dest = ctx.getWorkbench ().getRegistry ().getProjectFile( 
					(String)((Item) dest).getSystemId());
		else
			dest = fs.getRoot();
		try
		{
			if (file.exists ())
			{
				f = fs.addLocalFile (file, dest, file.getName ());
			}
			else
			{
				f = fs.create (dest, file.getName (), false, true);
			}
		}
		catch (IOException e)
		{
			ctx.getWorkbench().logGUIInfo (IO.I18N.msg ("addfile.failed", file), e);
			return null;
		}
		return new SourceFile (file.getName(), mt, IO.toSystemId (fs, f));
	}
	
	
	public static SourceFile toSourceFile (File file, MimeType mt, Context ctx) {
		return toSourceFile(file, mt, ctx, null);
	}
	

	public static SourceFile addSourceFile (File file, MimeType mt, Context ctx)
	{
		return addSourceFile(file, mt, ctx, null);
	}
	
	
	public static SourceFile addSourceFile (File file, MimeType mt, Context ctx, Object dest)
	{
		Object destDir=null;
		if (dest!=null) {
			destDir = dest;
		}
		else {
			destDir = ctx.getWorkbench ().getRegistry ().getDirectory ("/project/objects/files", null);
		}
		SourceFile f = toSourceFile(file, mt, ctx, destDir);
		if (f != null)
		{
			((Item) destDir).addUserItem (f);
		}
		return f;
	}

	public static void addNode (Item item, Object info, Context context)
	{
		final Node node;
		final Expression expr;
		if (info instanceof Node)
		{
			node = (Node) info;
			expr = null;
		}
		else if (info instanceof ActionEditEvent)
		{
			node = null;
			ActionEditEvent e = (ActionEditEvent) info;
			if (e.isConsumed ()
				|| !((info = e.getSource ()) instanceof Expression))
			{
				return;
			}
			e.consume ();
			expr = (Expression) info;
		}
		else
		{
			return;
		}
		final Workbench w = context.getWorkbench ();
		UI.executeLockedly (w.getRegistry ().getProjectGraph (), true,
			new Command ()
			{
				@Override
				public String getCommandName ()
				{
					return null;
				}

				@Override
				public void run (Object arg, Context c)
				{
					Object o = (node != null) ? node : expr.evaluate (w, UI
						.getArgs (c, expr));
					if (!(o instanceof Node))
					{
						return;
					}
					((Node) o).setExtentIndex (Node.LAST_EXTENT_INDEX);
					GraphManager g = w.getRegistry ().getProjectGraph ();
					g.getRoot ()
						.addEdgeBitsTo (
							(Node) o,
							de.grogra.graph.Graph.BRANCH_EDGE,
							g.getActiveTransaction ());
				}
			}, null, context, JobManager.ACTION_FLAGS);
	}

	public static void addEdgeBits (final Node source, final Node target, final int bits, Context context)
	{
		final Workbench w = context.getWorkbench ();
		UI.executeLockedly (w.getRegistry ().getProjectGraph (), true,
			new Command ()
			{
				@Override
				public String getCommandName ()
				{
					return null;
				}

				@Override
				public void run (Object arg, Context c)
				{
					GraphManager g = w.getRegistry ().getProjectGraph ();
					source.addEdgeBitsTo (target, bits, g.getActiveTransaction ());
				}
			}, null, context, JobManager.ACTION_FLAGS);
	}
	
	public static void removeEdgeBits (final Node source, final Node target, final int bits, Context context)
	{
		final Workbench w = context.getWorkbench ();
		UI.executeLockedly (w.getRegistry ().getProjectGraph (), true,
			new Command ()
			{
				@Override
				public String getCommandName ()
				{
					return null;
				}

				@Override
				public void run (Object arg, Context c)
				{
					GraphManager g = w.getRegistry ().getProjectGraph ();
					source.removeEdgeBitsTo (target, bits, g.getActiveTransaction ());
				}
			}, null, context, JobManager.ACTION_FLAGS);
	}

	private static float clamp (float v)
	{
		return (v >= 1) ? 1 : (v <= 0) ? 0 : v;
	}

	public static Color getAWTColor (float r, float g, float b, float a)
	{
		return new Color (clamp (r), clamp (g), clamp (b), clamp (a));
	}

	public static Color getAWTColor (Tuple3f color)
	{
		return getAWTColor (color.x, color.y, color.z, 1);
	}

	public static Tuple3f setColor (Tuple3f t, int rgb)
	{
		t.set (((rgb >> 16) & 255) * (1f / 255), ((rgb >> 8) & 255)
			* (1f / 255), (rgb & 255) * (1f / 255));
		return t;
	}

	public static void exportGraphToFile (GraphManager graphManager, File file) {
		Workbench w = Workbench.current ();
		if (w == null) {
			throw new IllegalStateException ("no current workbench");
		}
		writeGraph (w, graphManager, MimeType.TEXT_XML, file);
	}

	public static void writeGraph (Context ctx, GraphManager graphManager, MimeType mt, File file) {
		try {
			OutputStream out = new BufferedOutputStream (new FileOutputStream (file));
			writeGraph (ctx, graphManager, mt, out, file.getPath ());
			out.flush ();
			out.close ();
		} catch (IOException e) {
			ctx.getWorkbench ().logGUIInfo (I18N.msg ("export.graph.failed", file), e);
		}
	}
	
	public static void writeGraph (Context ctx, GraphManager graphManager, MimeType mt, OutputStream out, String outName) {
		FilterSource fs = null; 
		//ToDo
		/*fs = IO.createPipeline (new ObjectSourceImpl (graphManager,
				"snapshot", ImageWriter.RENDERED_IMAGE_FLAVOR, ctx.getWorkbench ()
					.getRegistry ().getRootRegistry (), null), new IOFlavor (mt, IOFlavor.OUTPUT_STREAM, null));
		*/
		if (fs == null) {
			ctx.getWorkbench ().logGUIInfo (IO.I18N.msg ("save.unsupported", outName, IO.getDescription (mt)));
			return;
		}
		try {
			((OutputStreamSource) fs).write (out);
		} catch (IOException ex) {
			ctx.getWorkbench ().logGUIInfo (I18N.msg ("export.graph.failed", outName), ex);
		}
	}
	
	public static SourceDirectory getDirectoryToAdd (Context ctx)
	{
		String name = ctx.getWindow ().showInputDialog (I18N.getString ("fileexplorer.add_dir.title"), 
				I18N.getString ("fileexplorer.add_dir.msg"), 
				I18N.getString ("fileexplorer.add_dir.default"));
		if (name == null) {
			return null;
		}
		return toSourceDirectory(name, ctx, null);
	}
	
	public static SourceDirectory toSourceDirectory (String name, Context ctx, Object dest)
	{
		FileSystem fs = ctx.getWorkbench ().getRegistry ().getFileSystem ();
		if (dest == null)
			dest = UIProperty.WORKBENCH_SELECTION.getValue(ctx.getWorkbench());
		if (dest instanceof SourceDirectory)
			dest = ctx.getWorkbench ().getRegistry ().getProjectFile( 
					(String)((Item) dest).getSystemId());
		else
			dest = fs.getRoot ();
		Object f;
		try
		{
			f = fs.create (dest, name, true, true);
		}
		catch (IOException e)
		{
			ctx.getWorkbench().logGUIInfo (IO.I18N.msg ("mkdir.failed", name), e);
			return null;
		}
		return new SourceDirectory (name, IO.toSystemId (fs, f));
	}
	
	
	public static SourceDirectory addSourceDirectory (String dirName, Context ctx, Object dest)
	{
		if (dest==null) 
			dest = ctx.getWorkbench ().getRegistry ().getDirectory ("/project/objects/files", null);
		SourceDirectory f = toSourceDirectory(dirName, ctx, dest);
		if (f != null)
		{
			((Item) dest).addUserItem (f);
		}
		return f;
	}

	/**
	 * Used with command lines
	 */
	@Override
	public String getCommandName() {
		return null;
	}

	@Override
	public Workbench getCurrentWorkbench() {
		return Workbench.current();
	}

	/**
	 * wrapper to open a workbench, that contains a filtersource and param in the info
	 */
	@Override
	public ProjectWorkbench open(Object info) throws Exception {
		if (info instanceof FilterSource) {
			return open((FilterSource)info, null);
		}
		return null;
	}

	@Override
	public ProjectWorkbench open(FilterSource fs, Map initParams) {
		
		Workbench wb = getMainWorkbench();
		
		FilterSource s = IO.createPipeline (fs, IOFlavor.PROJECT_LOADER);
		if (!(s instanceof ObjectSource))
		{
			wb.logGUIInfo (IO.I18N.msg ("openproject.unsupported", IO
				.toName (fs.getSystemId ()), IO.getDescription (fs.getFlavor ()
				.getMimeType ())));
			return null;
		}

		IMPWorkbench w=(IMPWorkbench) getWorkbenchManager()
				.createWorkbench(new ProjectFactoryImpl(), fs, initParams);
		if (fs instanceof FileSource)
			{
				w.setFile (((FileSource) fs).getInputFile (), fs.getFlavor ()
					.getMimeType ());
			}
		try
		{
			w.setName (IO.toSimpleName (fs.getSystemId ()));
		}
		catch (Exception e)
		{
			w.disposeWhenNotInitialized ();
			wb.logGUIInfo (IO.I18N.msg ("openproject.failed", IO.toName (fs
				.getSystemId ())), e);
			return null;
		}
		finally
		{
			Registry.setCurrent (wb);
		}
		start (w, wb.getWindow ());
		return w;
		
	}

	/**
	 * Wrapper that close a workbench whose id contained in info
	 */
	@Override
	public void close(Object info) {
		if (info instanceof ProjectWorkbench)
			wbm.closeWorkbench((ProjectWorkbench)info);
	}

	/**
	 * Create a new workbench from the templates
	 * !!!! Not used
	 */
	@Override
	public void create(Object info) throws IOException, Exception {
		Workbench w =(Workbench) create("NewRGG","Model");
		w.setFile(null, null);
	}

	
	@Override
	public void loadExample(Object info) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void listExamples(Object info) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void listTemplates(Object info) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void selectWorkbench(Object info) {
		if (info instanceof Workbench)
			selectWorkbench((Workbench)info);
	}

	@Override
	public void listWorkbenches(Object info) {
		for(Object w: workbenches) {
			getMainWorkbench().logGUIInfo(w.toString());
		}
	}

	@Override
	public void listProjects(Object info) {
		for (String p : pm.listOpenProjects()) {
			getMainWorkbench().logGUIInfo(p);
		}
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}

	
	// WB MANAGER 
	
	/**
	 * need to be addapted with objectlist, as the id can get out of the object list size
	 */
	@Override
	public ProjectWorkbench getWorkbench(Object id) throws IndexOutOfBoundsException {
		return (ProjectWorkbench) workbenches.get((int) id);
	}

	//TODO: untested
	@Override
	public ArrayList<Workbench> getWorkbenches() {
		Workbench[] w = (Workbench[]) workbenches.toArray(new Workbench[0]);
		return new ArrayList<>(Arrays.asList(w));
	}

	@Override
	public HashMap<Integer, Workbench> listWorkbenches() {
		HashMap<Integer, Workbench> result = new HashMap<Integer,Workbench>();
		for (Object wb : workbenches) {
			result.put(workbenches.indexOf(wb), (Workbench)wb);
		}
		return result;
	}

	@Override
	public Object getWorkbenchId(ProjectWorkbench wb) {
		return workbenches.indexOf(wb);
	}

	@Override
	public void registerWorkbench(ProjectWorkbench w) {
		registerWorkbench((IMPWorkbench)w);
		w.setToken(w.getApplicationName()+"-"+(wbID++));
	}

	@Override
	public void deregisterWorkbench(ProjectWorkbench w) {
		deregisterWorkbench((IMPWorkbench)w, w.getWindow());
	}

	@Override
	public void closeWorkbench(ProjectWorkbench w) {
		run (null, w, new CommandItem("close"));
	}

	@Override
	public void closeWorkbench(ProjectWorkbench w, Command afterDispose) {
		run (null, w, new CommandItem("close"));
	}

	@Override
	public boolean isManager(ProjectWorkbench w) {
		return (workbenches.indexOf(w) == -1) ? false : true;
	}

	/**
	 * Create workbench not the main one
	 */
	@Override
	public ProjectWorkbench createWorkbench(ProjectFactory pf, FilterSource fs, Map initParams) {
		try {
			Project p = pm.openProject(pf, fs, initParams);
			return createWorkbench(p, initParams);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Only for mainworkbench
	 */
	@Override
	public ProjectWorkbench createWorkbench(ProjectFactory pf) throws IOException {
		Project p =pm.createProject( pf);		
		return createWorkbench(p);
	}

	@Override
	public ProjectWorkbench createWorkbench(Object projectId) {
		Project p = pm.getProject(projectId);
		return createWorkbench(p);
	}
	
	/**
	 * Mainworkbench only
	 * @param p
	 * @return
	 */
	protected ProjectWorkbench createWorkbench(Project p) {
		IMPWorkbench wb = new IMPWorkbench (null);
		wb.setProject(p);
		wb.initLogger();
		wb.setName("MainWorkbench");
		getWorkbenchManager().registerWorkbench(wb); 
		getProjectManager().linkWorkbenchToProject(p.getId(), wb);
		wb.getJobManager().initialize(wb);
		return wb;
	}
	
	
	protected ProjectWorkbench createWorkbench(Project p, Map initParams) {
		IMPWorkbench wb = new IMPWorkbench (initParams);
		try {
			wb.setProject(p);
			wb.initLogger();
			getWorkbenchManager().registerWorkbench(wb);
			getProjectManager().linkWorkbenchToProject(p.getId(), wb);
			wb.getJobManager().initialize(wb);
		}		
		catch (Exception e)
		{
			wb.disposeWhenNotInitialized ();
			getMainWorkbench().logGUIInfo (IO.I18N.msg ("openproject.failed", IO.toName (p
				.getName() )), e);
			return null;
		}
		return wb;
	}
	
}