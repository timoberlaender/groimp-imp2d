package de.grogra.imp;

import de.grogra.graph.ArrayPath;
import de.grogra.graph.Graph;
import de.grogra.graph.GraphFilter;
import de.grogra.graph.Visitor;
import de.grogra.imp.GraphDescriptor;
import de.grogra.imp.View;

public class CustomGraphDescriptor  extends GraphDescriptor{

	//enh:sco
	
	@Override
	public Graph getGraph (View view)
	{
		
		return new GraphFilter (view.getWorkbench ().getRegistry ().getProjectGraph ())
		{
			@Override
			public Object getRoot (String key)
			{
				if (MAIN_GRAPH.equals (key))
				{
					String na=customKey;
					if(na!=null) {
						Object r = source.getRoot (customKey);
						if (r != null)
						{
							return r;
						}
					}
				}
				return super.getRoot (key);
			}

			public void accept (Object startNode, final Visitor visitor,
								ArrayPath placeInPath)
			{
				accept (startNode, visitor, placeInPath, false);
			}
		};
	}
	String customKey;
	//enh:field

//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;

	public static final Type.Field customKey$FIELD;

	public static class Type extends GraphDescriptor.Type
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (CustomGraphDescriptor representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, GraphDescriptor.$TYPE);
		}

		private static final int SUPER_FIELD_COUNT = GraphDescriptor.Type.FIELD_COUNT;
		protected static final int FIELD_COUNT = GraphDescriptor.Type.FIELD_COUNT + 1;

		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		protected void setObject (Object o, int id, Object value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					((CustomGraphDescriptor) o).customKey = (String) value;
					return;
			}
			super.setObject (o, id, value);
		}

		@Override
		protected Object getObject (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					return ((CustomGraphDescriptor) o).customKey;
			}
			return super.getObject (o, id);
		}

		@Override
		public Object newInstance ()
		{
			return new CustomGraphDescriptor ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (CustomGraphDescriptor.class);
		customKey$FIELD = Type._addManagedField ($TYPE, "customKey", 0 | Type.Field.SCO, de.grogra.reflect.ClassAdapter.wrap (String.class), null, Type.SUPER_FIELD_COUNT + 0);
		$TYPE.validate ();
	}

//enh:end
}
