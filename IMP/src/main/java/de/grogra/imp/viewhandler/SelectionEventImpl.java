package de.grogra.imp.viewhandler;

import java.awt.event.MouseEvent;
import java.util.EventObject;

import de.grogra.graph.Path;
import de.grogra.imp.edit.ViewSelection;

public class SelectionEventImpl implements SelectionEvent {

	protected final ViewEventHandler handler;
		
	public SelectionEventImpl (ViewEventHandler h, java.util.EventObject e)
	{
		handler = h;
	}


	@Override
	public void eventOccured(EventObject e) {
		if (!(e instanceof MouseEvent))
		{
			return;
		}
		MouseEvent me = (MouseEvent) e;
		me.consume ();
		switch (me.getID ())
		{
			case MouseEvent.MOUSE_CLICKED:
				selectImpl(me);
			case MouseEvent.MOUSE_MOVED:
			case MouseEvent.MOUSE_EXITED:
				handler.disposeEvent (null);
				return;
		}
	}
	
	public void selectImpl(MouseEvent me) {
		
		handler.pick (me.getX (), me.getY ());
		
		Path path = null;
		for (int s = 0; s < handler.getPickSize (); s++)
		{
			Path p = handler.getPickedAt(s);
			if (p != null)
			{
				path = p;
				break;
			}
		}

		handler.setLastSelected(path); 
		
		if ((me.getID () == MouseEvent.MOUSE_CLICKED)
			&& !(me.isAltDown () || me.isMetaDown ()))
		{
			ViewSelection s = ViewSelection.get (handler.getView());
			if (s != null)
			{
				if (path != null)
				{
					if (me.isControlDown ())
					{
						s.toggle (ViewSelection.SELECTED, path);
					}
					else
					{
						s.set (ViewSelection.SELECTED | ViewSelection.MOUSE_OVER_SELECTED | ViewSelection.MOUSE_OVER
								, new Path[] {path}, true);				
					}
				}
				else
				{
					s.set (ViewSelection.SELECTED | ViewSelection.MOUSE_OVER_SELECTED | ViewSelection.MOUSE_OVER
							, Path.PATH_0, true);
				}
			}
		}
	}


	@Override
	public void dispose() {
	}

}
