
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.imp.viewhandler;

import java.awt.event.MouseEvent;
import java.util.EventObject;

import de.grogra.graph.ArrayPath;
import de.grogra.graph.Graph;
import de.grogra.graph.GraphState;
import de.grogra.graph.Path;
import de.grogra.imp.PickElement;
import de.grogra.imp.PickList;
import de.grogra.imp.View;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.event.ClickEvent;
import de.grogra.pf.ui.event.DragEvent;
import de.grogra.util.Disposable;
import de.grogra.util.EventListener;

public abstract class ViewEventHandler implements Disposable, EventListener
{
	private boolean disposed = false;
	private View view;
	private de.grogra.util.DisposableEventListener event = null;

	//list contains the path when selecting/ list2 contains the path of the tools
	private PickList list, list2;
	private PickElement pickInfo = new PickElement ();
	private ArrayPath lastSelected = new ArrayPath ((Graph) null);
	private ArrayPath lastPicked = new ArrayPath ((Graph) null);

	private static final int NORMAL = 1, RESOLVE_EVENT = 2;
	private int state = NORMAL;
	
	private boolean hasPicked, hasPickedTool=false;
	
	
	public ViewEventHandler (View view, boolean allowNegativePickDist)
	{
		this.view = view;
		list = new PickList (10, allowNegativePickDist); 
		list2 = new PickList (10, allowNegativePickDist); 
	}


	public void dispose ()
	{
		if (disposed)
		{
			return;
		}
		disposed = true;
		getViewEventFactory().dispose();
		list.reset ();
		list2.reset ();
		pickInfo = null;
		if (event != null)
		{
			event.dispose ();
			event = null;
		}
		view = null;
	}

	/**
	* If Normal -> possible to start an event (tool, nav, selection)
	* Once an event is activated, it is consumed. Thus, the order in NORMAL matters.
	* Else -> continue the current event
	*/
	public void eventOccured (EventObject e)
	{
		if (e instanceof MouseEvent && (
				((MouseEvent)e).getID()==MouseEvent.MOUSE_PRESSED ||
				((MouseEvent)e).getID()==MouseEvent.MOUSE_MOVED ) ) {
			// first event in the loop (then released/ dragged/ clicked)
			hasPicked=false;
			hasPickedTool=false;
		}
		try {
			View.set (GraphState.current (view.getGraph ()), view);
			ViewEventFactory ef = getViewEventFactory();

			switch (state)
			{
				case NORMAL:
					if (ef.getEventFactory(ViewEventFactory.TOOL).isActivationEvent(e) && 
						 (	isMouseOnTool(e) || isMouseOnSelected(e) )
							) {
						startEventManagement(e, ef.getEventFactory(ViewEventFactory.TOOL));
						break;
					}
					if (ef.getEventFactory(ViewEventFactory.NAVIGATION).isActivationEvent(e)) {
						startEventManagement(e, ef.getEventFactory(ViewEventFactory.NAVIGATION));
						break;
					}
					if (ef.getEventFactory(ViewEventFactory.SELECTION).isActivationEvent(e)){
						startEventManagement(e, ef.getEventFactory(ViewEventFactory.SELECTION));
						break;
					}
					ef.getHighlighter().calculateHighlight(e);
					break;
				case RESOLVE_EVENT:
					manageEvent(e);
					break;
				default:
					break;
			}
		}
		finally
		{
			View.set (GraphState.current (view.getGraph ()), null);
		}
	}
	

	public final View getView ()
	{
		return view;
	}


	private void setState (int newState)
	{
		if (state == newState)
		{
			return;
		}
		switch (state)
		{
			case NORMAL:
				getViewEventFactory().getHighlighter().resetHighlight ();
				break;
			case RESOLVE_EVENT:
				event.dispose ();
				event = null;
				break;
		}
		state = newState;
	}


	public void disposeEvent (EventObject e)
	{
		setState (NORMAL);
		if (e != null)
		{
			eventOccured (e);
		}
	}
	
	
	
	public abstract ViewEventFactory getViewEventFactory();
		
	private void manageEvent(EventObject e) {
		if (!UI.isConsumed(e)) {
			event.eventOccured (e);
			UI.consume (e);
		}
	}
	
	private void startEventManagement(EventObject e, DisposableViewEventFactory tf) {
		if (!UI.isConsumed(e)) {
			setState (RESOLVE_EVENT);
			event = tf.createEvent(this, e);
			UI.consume (e);
		}
	}
	
	/**
	 * Visiting the graph for tool is way faster that visiting the complete graph
	 * @param e
	 * @return
	 */
	public boolean isMouseOnTool(EventObject e) {
		if (!(e instanceof MouseEvent)) {
			return false;
		}
		MouseEvent me = (MouseEvent)e;
		pickTool(me.getX(), me.getY());
		for (int s = 0; s < list2.getSize (); s++)
		{
			Path p = getPickedTool(s);
			if (p != null)
			{
				if  (view.isToolGraph (p.getGraph ()))
				{
					if (hasListener (p)) {
						return true;
					}
				}
				}
			}
		return false;
	}
	
	/**
	 * This is faster that picking for real. As it only test on one path
	 * @param e
	 * @return
	 */
	public boolean isMouseOnSelected(EventObject e) {
		if (!(e instanceof MouseEvent)) {
			return false;
		}
		MouseEvent me = (MouseEvent)e;
		if (lastSelected.getNodeAndEdgeCount() < 1) {
			return false;
		}
		pickTool(me.getX(), me.getY());
		for (int s = 0; s < list2.getSize (); s++)
		{
			Path p = getPickedTool(s);
			if (p != null)
			{
				if( p.getObject(-1) == lastSelected.getObject(-1)) {
					return true;
				}
			}
		}
		return false;
	}
	

	public static void send (EventObject e, Path p)
	{
		Object last = p.getObject (-1);
		if (last instanceof EventListener)
		{
			((EventListener) last).eventOccured (e);
		}
		if (p.getGraph () instanceof EventListener)
		{
			((EventListener) p.getGraph ()).eventOccured (e);
		}
	}
	
	public static boolean hasListener (Path p)
	{
		if (p == null)
		{
			return false;
		}
		return (p.getObject (-1) instanceof EventListener)
			|| (p.getGraph () instanceof EventListener);
	}

	public void pick(int x, int y) {
		if (hasPicked) {
			return;
		}
		view.pick(x, y, list);
		hasPicked=true;
	}
	
	public void pick(int x, int y, PickList l) {
		view.pick(x, y, l);
	}
	
	public void setPick(PickList l) {
		this.list=l;
	}
	
	public void pickTool(int x, int y) {
		if (hasPickedTool) {
			return;
		}
		view.pickTool(x, y, list2);
		hasPickedTool=true;
	}
	
	public Path getPicked(int index) {
		if (index==-1) {
			return getPicked(pickInfo.path.getNodeAndEdgeCount()-1);
		}
		if (list.getSize()<=index) {
			return pickInfo.path;
		}
		list.getItem(index, pickInfo);
		if (pickInfo.path == null) {
			lastPicked.clear(null);
		}else {
			lastPicked.set(pickInfo.path);
		}
		return pickInfo.path;
	}
		
	/**
	 * Add the possible shift from the highlighter
	 */
	public Path getPickedAt(int shift) {
		if (list.getSize()<=shift) {
			return pickInfo.path;
		}
		list.getItem((getHighlightedIndex() + shift + list.getSize ()) % list.getSize (), pickInfo);
		if (pickInfo.path == null) {
			lastPicked.clear(null);
		}else {
			lastPicked.set(pickInfo.path);
		}
		return pickInfo.path;
	}
	
	public Path getPickedTool(int index) {
		if (index==-1) {
			return getPickedTool(pickInfo.path.getNodeAndEdgeCount()-1);
		}
		if (list2.getSize()<=index) {
			return pickInfo.path;
		}
		list2.getItem(index, pickInfo);
		return pickInfo.path;
	}
	
	public int getPickSize() {
		return list.getSize();
	}
	
	public PickList getPick() {
		return list;
	}
	
	public Path getLastSelected() {
		return lastSelected;
	}
	
	public void setLastSelected(Path p) {
		if (p == null) {
			this.lastSelected.clear(null);
		}
		else {
			this.lastSelected.set(p);
		}
	}

	public int getHighlightedIndex() {
		return getViewEventFactory().getHighlighter().getIndex();
	}
	
	protected abstract ClickEvent createClickEvent (MouseEvent event);

	protected abstract DragEvent createDragEvent (MouseEvent event);
}
