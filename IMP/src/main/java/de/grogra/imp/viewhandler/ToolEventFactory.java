package de.grogra.imp.viewhandler;

import java.awt.event.MouseEvent;
import java.util.EventObject;

import javax.swing.SwingUtilities;

import de.grogra.persistence.SCOType;
import de.grogra.persistence.ShareableBase;

public class ToolEventFactory extends ShareableBase implements DisposableViewEventFactory {
	
	//enh:sco SCOType
	
	@Override
	public ToolEvent createEvent
	(ViewEventHandler h, EventObject e) {
		return new ToolEventImpl(h,e);
	}

	@Override
	public boolean isActivationEvent(EventObject e) {
		if (!(e instanceof MouseEvent))
		{
			return false;
		}
		MouseEvent me = (MouseEvent) e;
		return (SwingUtilities.isLeftMouseButton(me) && (me.getID () == MouseEvent.MOUSE_PRESSED));
	}

	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;


	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (ToolEventFactory representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}


		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		public Object newInstance ()
		{
			return new ToolEventFactory ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (ToolEventFactory.class);
		$TYPE.validate ();
	}

//enh:end

}
