package de.grogra.imp.viewhandler;

import java.util.EventObject;

import de.grogra.persistence.ManageableType;
import de.grogra.persistence.SCOType;
import de.grogra.persistence.ShareableBase;
import de.grogra.util.DisposableEventListener;

public class EmptyEventFactory extends ShareableBase implements DisposableViewEventFactory{

	//enh:sco SCOType
	
	@Override
	public boolean isActivationEvent(EventObject e) {
		return false;
	}

	@Override
	public DisposableEventListener createEvent(ViewEventHandler h, EventObject e) {
		return null;
	}

	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;


	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (EmptyEventFactory representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}


		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		public Object newInstance ()
		{
			return new EmptyEventFactory ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (EmptyEventFactory.class);
		$TYPE.validate ();
	}

//enh:end
}
