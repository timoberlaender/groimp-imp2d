package de.grogra.imp.viewhandler;

/**
 * The <code>SelectionEvent</code> is the base for the <code>Selection</code>.
 */
public interface SelectionEvent extends de.grogra.util.DisposableEventListener{
}
