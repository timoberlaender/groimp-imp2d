package de.grogra.imp.viewhandler;

import java.awt.event.MouseEvent;
import java.util.EventObject;

import de.grogra.graph.ArrayPath;
import de.grogra.graph.Path;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.event.DragEvent;
import de.grogra.util.Map;
import de.grogra.util.Utils;

public class ToolEventImpl implements ToolEvent{

	protected final ViewEventHandler handler;
	protected int lastX, lastY;
	private MouseEvent dragEvent = null, pressEvent = null;
	
	public ToolEventImpl(ViewEventHandler h, EventObject e) {
		handler = h;
		pressEvent=(MouseEvent) e;
	}
	
	@Override
	public void eventOccured(EventObject e) {
		if (!(e instanceof MouseEvent))
		{
			return;
		}
		MouseEvent me = (MouseEvent) e;
		me.consume ();
		switch (me.getID ())
		{		
			case MouseEvent.MOUSE_RELEASED:
				Map opt = UI.getOptions (handler.getView().getWorkbench());
				if (dragEvent==null && 
						Utils.getBoolean(opt, "changeToolOnClick", false) && 
						handler.isMouseOnSelected(me)) {
					handler.getView().nextTool();
				}
			case MouseEvent.MOUSE_MOVED:
			case MouseEvent.MOUSE_EXITED:
				handler.disposeEvent (null);
				return;
			case MouseEvent.MOUSE_DRAGGED:
				dragEvent = me;
				if (pressEvent != null)
				{
					mouseDragged (pressEvent,
								  DragEvent.DRAGGING_STARTED, 0, 0);
					lastX = pressEvent.getX ();
					lastY = pressEvent.getY ();
					pressEvent = null;
				}
				mouseDragged (me, DragEvent.DRAGGING_CONTINUED,
							  me.getX () - lastX,
							  me.getY () - lastY);
				lastX = me.getX ();
				lastY = me.getY ();
		}
	}
	
	protected void mouseDragged (MouseEvent event, int dragState, int dx, int dy)
	{
		Path p = handler.getPickedTool(-1);
		if (handler.hasListener (p))
		{
			p = new ArrayPath (p);
			DragEvent me = handler.createDragEvent (event);
			me.set (handler.getView (), p);
			me.set (event);
			me.setDragData (dragState, dx, dy);
			handler.send (me, p);
		}
	}
	

	@Override
	public void dispose() {
		if (dragEvent!=null) {
			mouseDragged (dragEvent, DragEvent.DRAGGING_FINISHED,
					  0, 0);
			dragEvent = null;
		}
	}

}
