package de.grogra.imp.viewhandler;

import java.awt.event.MouseEvent;
import java.util.EventObject;

import de.grogra.graph.Path;
import de.grogra.imp.PickList;
import de.grogra.imp.edit.ViewSelection;
import de.grogra.imp.View;
import de.grogra.pf.ui.Command;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.JobManager;

/**
 * Class that handle the highlight of object in a View.
 */
public class HighlighterImpl extends Highlighter{
	
	private Unhighlight unhighlight = null; 
	private static final int NOTHING = 0, HIGHLIGHTED = 1, SELECTED = 2;
	private int highlightState = NOTHING, highlightIndex;
	private JobManager jm; 
	private PickList oldList;
	
	private final int[] a2 = new int[2];
	
	private int tolerance = 10, highlightDelay = 50;
	
	protected ViewEventHandler handler;
	
	public void init(Context ctx) {
		this.handler=((View)ctx).getEventHandler();
		this.jm=ctx.getWorkbench().getJobManager();
		this.oldList = new PickList (10, false); 
	}
	
	public Unhighlight getUnhighlight() {
		return unhighlight;
	}
	public void setUnhighlight(Unhighlight v) {
		unhighlight=v;
	}
	
	public void dispose() {
		if (unhighlight != null)
		{
			unhighlight.cancel ();
			unhighlight = null;
		}
		jm = null;
		if (oldList!=null) {
			oldList.reset ();
		}
	}
	
	@Override
	public void calculateHighlight (EventObject e)
	{
		if (!(e instanceof MouseEvent)) {
			return ;
		}
		MouseEvent me = (MouseEvent) e;
		if (handler==null) {
			return;
		}
		// automatic highlight when mouse over
		if (me.getID() == MouseEvent.MOUSE_MOVED
				) {
			PickList l = new PickList (10, true) ;
			handler.pick(me.getX(), me.getY(), l);
			if (!l.equals(oldList)) {
				highlightIndex = 0;
				resetHighlight();
				oldList = l;
				handler.setPick(l);
			}
			if (oldList.getSize() > 0) {
				computeIndex(me, oldList);
				highlight (oldList, highlightIndex, null); // the handler pick is not affected
				highlightState = HIGHLIGHTED;
			}
		}
		
		// highlight when ALT+CLICK -> several click make the "highlighted" one change
		if (me.getID() == MouseEvent.MOUSE_CLICKED && me.isAltDown()) {
			PickList l = new PickList (10, true) ;
			handler.pick(me.getX(), me.getY(), l);
			if (!l.equals(oldList)) {
				highlightIndex = 0;
				resetHighlight();
				oldList = l;
				handler.setPick(l);
			}
			computeIndex(me, handler.getPick());
			highlight (handler.getPick(), highlightIndex, a2);
			highlightState = HIGHLIGHTED;
		}
	}

	@Override
	public void resetHighlight ()
	{
		if (unhighlight != null)
		{
			unhighlight.cancel ();
			unhighlight = null;
		}
		if (highlightState != NOTHING)
		{
			highlight (handler.getPick(), -1, null);
			highlightState = NOTHING;
		}
	}
	
	
	/**
	 * remove all highlighting - a command to apply this async
	 */
	public class Unhighlight implements Command
	{
		private boolean canceled = false;

		void cancel ()
		{
			canceled = true;
		}

		public String getCommandName ()
		{
			return null;
		}

		public void run (Object info, Context ctx)
		{
			if (!canceled)
			{
				resetHighlight ();
			}
		}
	}
	private void enqueueUnhighlight ()
	{
		if (unhighlight != null)
		{
			unhighlight.cancel ();
		}
		jm.runLater	(highlightDelay, unhighlight = new Unhighlight (), null,
					 handler.getView());
	}
	
	public void computeIndex(MouseEvent me, PickList list) {
		if ((highlightState != NOTHING) && (list.getSize () > 1))
		{
			a2[0] = highlightIndex;
			if (me.isControlDown ())
			{
				if (highlightIndex == 0)
				{
					highlightIndex = list.getSize ();
				}
				highlightIndex--;
			}
			else
			{
				highlightIndex++;
				if (highlightIndex == list.getSize ())
				{
					highlightIndex = 0;
				}
			}
			a2[1] = highlightIndex;
//			highlight (highlightIndex, a2);
//			highlightState = SELECTED;
			//lastSelected.set (getHighlightedPath (0));
//				objectPicked (event);
			if (unhighlight != null)
			{
				enqueueUnhighlight ();
			}
		}
		
		
	}
	
	@Override
	public void highlight(PickList list, int index, int[] changed) {
		if (handler.getPickSize() < 1) {
			return;
		}
		ViewSelection s = ViewSelection.get (handler.getView());
		int i = -1, j = -1, k;
		while (true)
		{
			if (changed == null)
			{
				i++;
				if (i == list.getSize ())
				{
					break;
				}
			}
			else
			{
				j++;
				if (j == changed.length)
				{
					break;
				}
				i = changed[j];
			}
			if (index >= 0)
			{
				if (i == index)
				{
					k = ViewSelection.MOUSE_OVER_SELECTED | ViewSelection.MOUSE_OVER;
				}
				else
				{
					k = ViewSelection.MOUSE_OVER;
				}
			}
			else
			{
				k = 0;
			}
			Path p = list.getPath(i);
			if (!handler.getView().isToolGraph (p.getGraph ()))
			{
				s.removeAndAdd (ViewSelection.MOUSE_OVER_SELECTED | ViewSelection.MOUSE_OVER,
								k, p);
			}
		}
	}
	
	public int getIndex() {
		return highlightIndex;
	}
	
}
