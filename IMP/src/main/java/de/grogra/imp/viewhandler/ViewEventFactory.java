package de.grogra.imp.viewhandler;

import de.grogra.util.Disposable;
import de.grogra.graph.Visitor;
import de.grogra.persistence.ShareableBase;
import de.grogra.pf.ui.Context;

public abstract class ViewEventFactory extends ShareableBase implements Disposable{
	
	protected DisposableViewEventFactory navigatorFactory;
	protected DisposableViewEventFactory toolFactory;
	protected DisposableViewEventFactory selectionFactory;
	
	protected Highlighter highlighter;
	
	private Visitor pickVisitor;
	private Visitor pickToolVisitor;
	
	public static final int TOOL = 0;
	public static final int NAVIGATION = 1;
	public static final int SELECTION = 2;
	
	public DisposableViewEventFactory getEventFactory(int type) {
		switch(type) {
			case TOOL:
				return toolFactory;
			case NAVIGATION:
				return navigatorFactory;
			case SELECTION:
				return selectionFactory;
		}
		return null;
	}
	
	public void init(Context ctx) {
		highlighter.init(ctx);
		toolFactory.init(ctx);
		navigatorFactory.init(ctx);
		selectionFactory.init(ctx);
	}
	
	public Highlighter getHighlighter() {
		return highlighter;
	}
	
	@Override
	public void dispose() {
		if (highlighter!=null) {
			highlighter.dispose();
		}
	}
	
	public Visitor getPickVisitor() {
		return pickVisitor;
	}
	public Visitor getPickToolVisitor() {
		return pickToolVisitor;
	}
	public void setPickVisitor(Visitor v) {
		this.pickVisitor=v;
	}
	public void setPickToolVisitor(Visitor v) {
		this.pickToolVisitor = v;
	}
}
