package de.grogra.imp.io;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.jar.Attributes;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import de.grogra.imp.IMP;
import de.grogra.imp.View;
import de.grogra.pf.boot.Main;
import de.grogra.pf.io.FileSource;
import de.grogra.pf.io.FileTypeItem;
import de.grogra.pf.io.FileTypeItem.Filter;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.IO;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ProjectLoader;
import de.grogra.pf.registry.Directory;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.ItemCriterion;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.registry.expr.ObjectExpr;
import de.grogra.pf.ui.Command;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.JobManager;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.registry.FileFactory;
import de.grogra.pf.ui.registry.ItemFactory;
import de.grogra.pf.ui.registry.ResourceDirectory;
import de.grogra.pf.ui.registry.SourceDirectory;
import de.grogra.pf.ui.registry.SourceFile;
import de.grogra.util.I18NBundle;
import de.grogra.util.MimeType;
import de.grogra.util.ModifiableMap;
import de.grogra.util.Utils;

public class LocalFileManager {

	public static final String OBJECTS_FILE_PATH = "/project/objects/files";
	public static final I18NBundle I18N = I18NBundle.getInstance (LocalFileManager.class);
	
	final static String MANIFEST = "MANIFEST.MF";
	static Set<String> dontAddList = new HashSet<>();
	static String rootDirectory;
			
	/**
	 * A manager for local file project. It load the file from the local system into the groimp project.
	 * It scans local directories, check if the files are already included in the project,
	 * get their loader, and associated resource directory. Then, adds the file to the project.
	 * It should n't copy files into the local filesystem. It only load them as groimp objects and adds them in
	 * the project.
	 * 
	 * There is a "weird" exception for SourceFile items, the way they are handled by groimp
	 * is different from any other items because the object they represent can also be 
	 * seen as a compilation unit. 
	 * 
	 * @param wb the current workbench
	 * @throws IOException 
	 */
	public LocalFileManager(Context ctx) throws IOException {
		// try to load all directories and load files
		// Check all repositories within the project root except META-INF, if the repo contains 
		// mimtetype that could be loaded as SourceFile/LightDistrib/ObjectSource
		// > add the repo as SourceDirector and the files as SourceFiles/LightDistrib/ObjectSource.
		// Check if the files are not already included
		rootDirectory = (String) ctx.getWorkbench().getProperty (Workbench.PROJECT_DIRECTORY);
		initializeFileList(ctx);
	}
	
	
	/**
	 * initialize the list of file monitored by the project
	 * @throws IOException 
	 */
	private void initializeFileList(Context ctx) throws IOException {
		dontAddList.add(MANIFEST);
		loadManifestToFileList(ctx);
		getAlreadyLoadedResourceFiles(ctx);
	}

	/**
	 * Scan a directory and load all files that haven't been loaded in the project already
	 * 
	 * @param dir the directory to scan
	 * @param wb the current workbench
	 * @throws IOException
	 */
	public static void loadFromDirectory(String dir, Context ctx) throws IOException {
	    Files.walkFileTree(Paths.get(dir), new SimpleFileVisitor<Path>() {
	        @Override
	        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws MalformedURLException {
	            if (!Files.isDirectory(file) && 
	            		shouldBeLoaded(file, ctx) )
	            {
	            	loadFile(file.toFile(), ctx);
	            }
	            return FileVisitResult.CONTINUE;
	        }
	    });
	}
	
	
	/**
	 * Truncates a path so it starts from the project root.
	 * 
	 * @return The path from the project root to p
	 */
	public static Path toProjectPath(Path p, Context ctx) {
		Path ProjectRoot = Paths.get(rootDirectory);
		return p.subpath(ProjectRoot.getNameCount(), p.getNameCount());
	}
	
	public static Path toProjectPath(File p, Context ctx) {
		return toProjectPath(p.toPath(), ctx);
	}
	
	
	public String getProjectPath(Context ctx) {
		return rootDirectory;
	}
	
	
	public static boolean shouldBeLoaded(Path file, Context ctx) {
		if ( !(dontAddList.contains((file.getFileName().toString()))) 
    		// Check from the manifest file because only 
    		///objects/files are listed from registry.getProjectFile()
			//TODO: should only look at the dontaddlist - which should be init
//    		!(ctx.getWorkbench().getRegistry().getFileSystem().getManifest().getEntries()
//    				.containsKey(toProjectPath(file, ctx.getWorkbench()).toString())) 
    		)
			return true;
		return false;
	}
	
	public static boolean shouldBeLoaded(File file, Context ctx) {
		return shouldBeLoaded(file.toPath(), ctx);
	}
	
	
	/**
	 * Load the file into the current impworkbench,
	 *  in the resource directory associated with its type.
	 * 
	 * @param file A file
	 * @throws MalformedURLException 
	 */
	public static Object loadFile(File file, Context ctx, String outClass) throws MalformedURLException {
		Object outFile;
		if (isProject(file)) {
			return null;
		}
		if (isSource(file)){
			Item f = getResourceDirectory(file, ctx);
			Object dest = ctx.getWorkbench ().getRegistry ().getDirectory (OBJECTS_FILE_PATH, null);
			String pathToDest = toProjectPath(file, ctx).getParent()!=null ?
					toProjectPath(file, ctx).getParent().toString() : 
						null;
			if (pathToDest != null) {
    			Object[] pathToDestArray = Utils.pathToArray( pathToDest );
    			dest = createSourceDirectories(f, pathToDestArray, ctx);
			}
			outFile=IMP.addSourceFile(file, IO.getMimeType (file.toString()), ctx, dest);
		}
		else {
			Item filter = getResourceFilter(file, ctx);
			if (filter == null) {
				return null;
			}
			Item f = getResourceDirectory((FilterItem) filter, ctx);
    		FileFactory ff = new FileFactory();
    		ff.getManageableType ().getManagedField("directory")
    				.setObject(ff, (toProjectPath(file, ctx).getParent()!=null ? 
    							toProjectPath(file, ctx).getParent().toString() : null));
    		if (outClass == null) {
	    		ff.getManageableType ().getManagedField("type")
				.setObject(ff, (((FilterItem)filter).getOutputFlavor().getObjectClass()!=null ? 
						((FilterItem)filter).getOutputFlavor().getObjectClass().getName() : null));
    		}
    		outFile=ff.linkFromURLToResourceDir(ctx.getWorkbench().getRegistry(), 
        			file.toURL(), new de.grogra.util.StringMap(), ctx.getWorkbench(), f);
    		}
		dontAddList.add(file.getName());
		return outFile;
	}
	
	
	/**
	 * Load the file into the current impworkbench,
	 *  in the resource directory associated with its type.
	 * 
	 * @param file A file
	 * @throws MalformedURLException 
	 */
	public static Object loadFile(File file, Context ctx) throws MalformedURLException {
		return loadFile(file, ctx, null);
	}
	
	
	/**
	 * Create SourceDirectories to linked to the parent. 
	 * 
	 * @param parent the parent node where the SourceDirectories are added. Usually, /project/objects/files
	 * @param path an array of names of the SourceDirectories to add (e.g. [subDir1, subDir2], 
	 * subDir1, subDir2 are Strings
	 */
	public static Object createSourceDirectories(Object parent, Object[] path, Context ctx) {
		// loop on each path item, if doesnt exist add the dir else, select it 
		System.out.println("restart");
		if (path.length==0)
			return parent;
		String offset = "";
		if (parent instanceof Directory)
			offset=IO.toSystemId(ctx.getWorkbench().getRegistry().getFileSystem(), "");
		else
			offset=((Item) parent).getSystemId()+"/";
		Item dir = ((Item) parent).getItem((String) path[0]);
		if (dir == null)
			dir = IMP.addSourceDirectory((String) path[0], ctx, parent);
		return createSourceDirectories(dir, Arrays.copyOfRange(path, 1, path.length), ctx);
	}
	
	
	/**
	 * Get the parent of the last item in the path
	 * 
	 * @param systemId a systemId of an object which can include several subfolders (e.g. pfs:dir1/dir2/filename.rgg) 
	 * @return the parent of the last file in the path (Usually a sourcedirectory or a directory)
	 */
	public static Object getDirectParent(Item parent, String systemId, Object[] path, Context ctx) {
		if (parent.getItem(systemId) != null) 
			return parent;

		if (path.length == 0 || 
				parent.getItem(path[0].toString() ) == null)
			return null;
		
		if (parent.getItem(path[0].toString() ).getItem(systemId) != null)
			return parent.getItem(path[0].toString() );
		
		path[1] = path[0].toString()+"/"+path[1].toString();
		return getDirectParent( parent.getItem(path[0].toString()), systemId, 
				Arrays.copyOfRange(path, 1, path.length), ctx);
	}
	
	
	
	/**
	 * Find the associated resource filter of a given file type. 
	 * 
	 * @param file A file
	 * @param wb the current workbench
	 * @return Return the associated resource filter in the registry of a given File
	 * @throws MalformedURLException 
	 */
	public static Item getResourceFilter(File file, Context ctx) {
		//TODO: return the correct filter for PROJECT FILES || very ugly -discard all ProjectLoader outputclass for now
		// the files are supposed to be FILE not PROJECTS (should be separated more properly)
		MimeType mt = IO.getMimeType (file.toString());	
    	Item filter = Item.findFirst (ctx.getWorkbench().getRegistry(), "/io/mimetypes",
	    		new ItemCriterion ()
    		{
    			@Override
    			public boolean isFulfilled (Item item, Object info)
    			{
					return ( (item instanceof FilterItem) && 
							( !isFilterAProjectLoader((FilterItem)item) ) ) ?
							(((FilterItem) item).getInputFlavor().getMimeType().getMediaType().equals(mt.getMediaType()))
							: false;
    			}
				@Override
				public String getRootDirectory() {return null;}
    		}, file, false);
    	
    	if (filter != null) {
			return filter;
    	}
		return null;
	}
	
	
	public static boolean isFilterAProjectLoader(FilterItem filter) {
		boolean isProjectLoader = false;
		if ( ( filter.getOutputFlavor() != null ) &&
		( filter.getOutputFlavor().getObjectClass() != null ) &&
		( filter.getOutputFlavor().getObjectClass().getName().contains("ProjectLoader") ) ) 
			isProjectLoader = true;
		return isProjectLoader;
	}
	
	
	/**
	 * Find the associated resource directory of a given file type. (i.e., .rgg, 
	 * .java files are located in /objects/files; .png, .jpeg are located in /objects/images).
	 * 
	 * @param file A file
	 * @param wb the current workbench
	 * @return Return the associated resource directory in the registry of a given File, or null if no such directory exists
	 * @throws MalformedURLException 
	 */
	public static Item getResourceDirectory(File file, Context ctx) {
		// For some reason the fileSource have a different filter that the other objects
		if (isSource(file))
			return ctx.getWorkbench().getRegistry().getDirectory (OBJECTS_FILE_PATH, null);
		return getResourceDirectory((FilterItem) getResourceFilter(file, ctx), ctx);
	}
	
	/**
	 * Find the associated resource directory of a given filter
	 * 
	 * @param filter a FilterItem
	 * @param wb the current workbench
	 * @return Return the associated resource directory in the registry of a given File, or null if no such directory exists
	 */
	public static Item getResourceDirectory(FilterItem filter, Context ctx) {
		// For some reason the fileSource have a different filter that the other objects
//		if (isSource(filter))
//			return ctx.getWorkbench().getRegistry().getDirectory (OBJECTS_FILE_PATH, null);
		Item factory = Item.findFirst (ctx.getWorkbench().getRegistry(), "/objects",
	    		new ItemCriterion ()
    		{
    			@Override
    			public boolean isFulfilled (Item item, Object info)
    			{
					return (item instanceof ItemFactory) ?
							filter.getOutputFlavor().getObjectClass()
							.equals(((ItemFactory) item).getObjectType().getImplementationClass())
							: false;
    			}
				@Override
				public String getRootDirectory() {return null;}
    		}, filter, false);
		if (factory != null) {
			return  ResourceDirectory.get (factory);
    	}
		return null;
	}
	
	
	/**
	 * Check if a file is a source type
	 * 
	 * @param file a file
	 * @return Return true if the file is a type accepted as source file in groimp (.xl, .rgg, .java, ...)
	 */
	public static boolean isSource(File f) {
		Filter[] filterSource = IO.getReadableFileTypes (new IOFlavor [] {IOFlavor.RESOURCE_LOADER});
		for (Filter filter:filterSource)
			if (filter.accept(f))
				return true;
		return false;
	}
	
	
	/**
	 * Check if a file is a project type
	 * 
	 * @param file a file
	 * @return Return true if the file is a type accepted as project file in groimp (.gs, .gsz)
	 */
	public static boolean isProject(File f) {
		Filter[] filterSource = IO.getReadableFileTypes (new IOFlavor [] {IOFlavor.PROJECT_LOADER});
		for (Filter filter:filterSource)
			if (filter.accept(f))
				return true;
		return false;
	}
	
	
	/**
	 * Find the resources in the Project that are not on the File system (i.e. that have been
	 * deleted while the project wasn't open)
	 * 
	 * @param ctx the Context of the project
	 * @return Return an array of items that doesn't exist in the filesystem anymore. 
	 * And which should probably be delete from the project (These items will create
	 * an error when opening the project, but will not prevent the opening)
	 */
	public Item[] getResourceToDelete(Context ctx) {
		return null;
	}
	
	
	/** 
	 * load the element in the manifest to the dontaddlist to monitor them
	 */
	private void loadManifestToFileList(Context ctx) {
		Map<String, Attributes> manifest = ctx.getWorkbench().getRegistry().getFileSystem().getManifest().getEntries();
		for (String file:manifest.keySet()) {
			dontAddList.add(Path.of(file).getFileName().toString());
		}
	}
	
	
	/**
	 * Find all occurences of Resource files that already exist in the project and add them to the dontAddList
	 * so even if they are not a the same path on the local file system as they are in the groimp project path, 
	 * they are not added again. 
	 * The detection is based on the simple name of the files. Knowing resource files should all have a unique 
	 * simple name (i.e. it shouldn't be two model.rgg files in one project (even in different sub directories))
	 * 
	 * @param ctx
	 * @return The list of items that already are loaded in the project
	 * @throws IOException 
	 */
	private Set<String> getAlreadyLoadedResourceFiles(Context ctx) throws IOException{
		Set<String> loadedOnDifferentPath = new HashSet<>();
		Map<String, Attributes> manifest = ctx.getWorkbench().getRegistry().getFileSystem().getManifest().getEntries();
		Files.walkFileTree(Paths.get(rootDirectory), new SimpleFileVisitor<Path>() {
	        @Override
	        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws MalformedURLException {
	            if (!Files.isDirectory(file) && isSource(file.toFile()) )
	            {
	            	if (manifest.containsKey(toProjectPath(file, ctx.getWorkbench()).toString())) {
	            		dontAddList.add(file.getFileName().toString());
	            	}
	            	else  {
	            		List<String> filesOnDifferentPath = manifest.keySet().stream()
	            				.filter( key -> Paths.get((String) key).getFileName().equals(file.getFileName()) )
	            			    .map(elt -> Paths.get((String) elt).getFileName().toString()).collect(Collectors.toList());
	            		if (!filesOnDifferentPath.isEmpty()) {
	            			dontAddList.addAll(filesOnDifferentPath);
	            			loadedOnDifferentPath.addAll(filesOnDifferentPath);
	            		}
	            	}
	            }
	            return FileVisitResult.CONTINUE;
	        }
	    });
		if (!loadedOnDifferentPath.isEmpty())
			ctx.getWorkbench().logGUIInfo ( 
					I18N.msg ("localFileManager.fileAlreadyExist", loadedOnDifferentPath.toString()), null);
		return loadedOnDifferentPath;
	}
	
	
	public void reset() {
		dontAddList.clear();
		dontAddList.add(MANIFEST);
	}
	
	
	/**
	 * Remove an item from the given context based on its file
	 * 
	 */
	public static void deleteItem(File file, Context ctx) {
		String systemId = IO.toSystemId(ctx.getWorkbench().getRegistry().getFileSystem(),
				toProjectPath(file, ctx).toString());
		Item node = Item.findFirst (ctx.getWorkbench().getRegistry(), "/project/objects",
	    		new ItemCriterion ()
    		{
    			@Override
    			public boolean isFulfilled (Item item, Object info)
    			{
    				return item.getName().equals(systemId);
    			}
				@Override
				public String getRootDirectory() {return null;}
    		}, systemId, false);
		if (node!=null) {
			
			UI.executeLockedly
			(((Item) node).getRegistry ().getProjectGraph (),
				 true, deactivateNodeCommand, node, 
				 ctx, JobManager.ACTION_FLAGS);
		// remove from list only if deleted from here	
			//TODO: no?
		if (dontAddList.contains(file.getName()))
			dontAddList.remove(file.getName());
		}
		
	}
	
	public static void deleteDirectory(String dir, Context ctx) {
		
	}
	
	
	
	
	static Command deactivateNodeCommand = new Command ()
		{
			@Override
			public void run (Object info, Context ctx)
			{
				((Item) info).deactivate ();
				((Item) info).remove ();
			}

			@Override
			public String getCommandName ()
			{
				return null;
			}
		};
	
}
