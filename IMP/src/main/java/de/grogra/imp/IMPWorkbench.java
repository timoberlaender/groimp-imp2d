/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.imp;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Filter;
import java.util.logging.LogRecord;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;

import de.grogra.graph.GraphUtils;
import de.grogra.graph.Path;
import de.grogra.graph.impl.GraphManager;
import de.grogra.graph.impl.Node;
import de.grogra.imp.edit.ViewSelection;
import de.grogra.imp.fswatcher.ProjectFSWatcher;
import de.grogra.imp.io.LocalFileManager;
import de.grogra.imp.net.Commands;
import de.grogra.imp.net.Connection;
import de.grogra.imp.net.MessageHandler;
import de.grogra.pf.boot.Main;
import de.grogra.pf.io.FileSource;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSourceImpl;
import de.grogra.pf.registry.Executable;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.registry.Value;
import de.grogra.pf.ui.Command;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.FileChooserResult;
import de.grogra.pf.ui.JobManager;
import de.grogra.pf.ui.Panel;
import de.grogra.pf.ui.ProjectWorkbench;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.UIProperty;
import de.grogra.pf.ui.Window;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.edit.Selection;
import de.grogra.pf.ui.event.ActionEditEvent;
import de.grogra.projectmanager.ProjectImpl;
import de.grogra.util.Map;
import de.grogra.util.StringMap;
import de.grogra.util.ThreadContext;
import de.grogra.util.Utils;
import de.grogra.vfs.FileSystem;
import de.grogra.vfs.LocalFileSystem;
import de.grogra.xl.util.ObjectList;

public final class IMPWorkbench extends ProjectWorkbench implements TreeModelListener
{
	private Window window;

	// these presence of this field ensures that the graph
	// state is not garbage collected
//	private GraphState regState;

	private ObjectList connections;
	
	ProjectFSWatcher prjFSWatcherThread;
	LocalFileManager fileManager;

	public IMPWorkbench (Map initParams)
	{
		super (new IMPJobManager (),
			IMP.getInstance ().getToolkit (), initParams);
		this.app = IMP.getInstance();
		connections = new ObjectList ();
	}
	

	public static IMPWorkbench get (Context ctx)
	{
		return (IMPWorkbench) ctx.getWorkbench ();
	}	

	@Override
	public Workbench getMainWorkbench ()
	{
		return IMP.getInstance ().getMainWorkbench ();
	}

	protected void close (final Command afterDispose)
	{
		if (getWindow () != null)
		{
			final Panel[] panels = getWindow ().getPanels (null);
			new Command ()
			{
				public void run (Object info, Context ctx)
				{
					for (int i = 0; i < panels.length; i++)
					{
						Panel p;
						if ((p = panels[i]) != null)
						{
							panels[i] = null;
							p.checkClose (this);
							return;
						}
					}
					close0 (afterDispose);
				}

				public String getCommandName ()
				{
					return null;
				}
			}.run (null, this);
		}
		else
		{
			close0 (afterDispose);
		}
	}

	void close0 (Command afterDispose)
	{
		if (isModified () && (getWindow () != null))
		{
			int res = getWindow ().showDialog (
				UI.I18N.msg ("project.savequestion.title"),
				UI.I18N.msg ("project.savequestion.msg", getName ()),
				Window.QUESTION_CANCEL_MESSAGE);
			if (res == Window.CANCEL_RESULT)
			{
				return;
			}
			else if (res == Window.YES_OK_RESULT)
			{
				if (!save (true))
				{
					return;
				}
			}
		}
		Registry r = getRegistry ().getRootRegistry ();
		Executable.runExecutables (r, "/hooks/close", r, UI.getArgs (this, null));
		getJobManager ().stop (afterDispose);
	}

	void disposeWhenNotInitialized ()
	{
		getRegistry ().dispose ();
		IMP.getInstance ().deregisterWorkbench (this, window);
	}

	public void dispose (Command afterDispose)
	{
		if (window != null)
		{
			window.dispose ();
		}
		getRegistry ().removeFileSystemListener (this); 
		if (prjFSWatcherThread!=null)
			prjFSWatcherThread.stopThread();
		disposeWhenNotInitialized ();
		if (afterDispose != null)
		{
			afterDispose.run (null, this);
		}
		setCurrent (null);
		window = null;
	}

	public void initialize ()
	{
		super.initialize();
		ThreadContext s = getJobManager ().getThreadContext ();
		setCurrent (this);
		getRegistry ().addFileSystemListener (this);
		
		
		Map propMap = Map.EMPTY_MAP;
		Object o = getRegistry().getRootRegistry().getUserProperty(Main.SCREEN_PROPERTY_ID);
		if (o != null)
		{
			propMap = new StringMap().putInt(Main.SCREEN_PROPERTY, (Integer) o);
		}
		window = getToolkit ().createWindow (IMP.CLOSE, propMap);
		if (window != null)
		{
			initializeWindow ();
			window.show (true, null);
		}
		Main.closeSplashScreen ();
		getLogger ().setFilter (null);
		ObjectList pendingLogs = ((ProjectImpl)getProject()).getPendingLogs();
		Filter logFilter = ((ProjectImpl)getProject()).getLogFilter();
		for (int i = 0; i < pendingLogs.size (); i++)
		{
			getLogger ().log ((LogRecord) pendingLogs.get (i));
		}
		getLogger ().setFilter (logFilter);
		((ProjectImpl)getProject()).setPendingLogs(null);
		((ProjectImpl)getProject()).setLogFilter(null);;
		StringMap m = new StringMap ().putObject ("registry", getRegistry ());
		m.putObject("workbench", this);
		m.putObject("filesystem", this.getRegistry().getFileSystem());
		Executable.runExecutables (getRegistry ().getRootRegistry (), "/hooks/projectloaded",
			getRegistry (), m);
		if (this == IMP.getInstance ().getMainWorkbench ())
		{
			getJobManager ().runLater (new Command ()
			{
				public void run (Object info, Context ctx)
				{
					executeCommandLine ();
				}

				public String getCommandName ()
				{
					return null;
				}
			}, null, this, JobManager.ACTION_FLAGS);
		}
//		UIProperty.FILE_SYNCHRONISATION.setValue(this, false);
	}

	void executeCommandLine ()
	{
		int argc = Main.getArgCount ();
		int arg = 0;
		while (arg < argc)
		{
			String s = Main.getArg (arg);
			if (s.equals ("-cmd"))
			{
				if (arg + 1 < argc)
				{
					String cmd = Main.getArg (arg + 1);
					String param = null;
					int i = cmd.indexOf ('=');
					if (i > 0)
					{
						param = cmd.substring (i + 1);
						cmd = cmd.substring (0, i);
					}
					Item c = Item.resolveItem (this, cmd);
					if (c instanceof Command)
					{
						((Command) c).run (param, this);
					}
				}
				arg += 2;
			}
			else if (arg + 1 == argc)
			{
				File f = new File (s);
				if (f.isFile ())
				{
					open (FileSource.createFileSource (IO.toSystemId (f), IO
						.getMimeType (f.getName ()), this, null), null);
				}
				arg++;
			}
			else
			{
				arg++;
			}
		}
	}

	void initializeWindow ()
	{
		initializeWindow (window);
	}

	public Item getRegistryItem (String item)
	{
		return getRegistry ().getItem (item);
	}

	public Window getWindow ()
	{
		return window;
	}

	@Override
	public Workbench open (FilterSource fs, Map initParams)
	{
		// Should it link to PROJECT_DIRECTORY?
		IMPWorkbench w = (IMPWorkbench) IMP.getInstance ()
			.open ( fs, initParams);
		if (Utils.getBoolean (initParams, START_AS_DEMO) && (w != null))
		{
			w.ignoreIfModified ();
			w.setFile (null, null);
		}
		return w;
	}
	

	public void open(Object info) {
		FileChooserResult fr = getToolkit().chooseFile(
				UI.I18N.getString("filedialog.openproject", "Open Project"), 
				IO.getReadableFileTypes(new IOFlavor[] { IOFlavor.PROJECT_LOADER }),
				Window.OPEN_FILE, 
				true, null, this, null);
		if (fr != null) {
			setProperty(PROJECT_DIRECTORY, fr.file.getParentFile().getAbsolutePath());
			FileSource fs = fr.createFileSource(getRegistry(), null);
			open(fs, null);
			addToLastUsed(this, fr.file);
		}
	}
	
	public void delete(Object info) {
		Object s = UIProperty.WORKBENCH_SELECTION.getValue(this);
		if ((s instanceof Selection) && ((((Selection) s).getCapabilities() & Selection.DELETABLE) != 0)) {
			((Selection) s).delete(true);
		}
	}
	
	public void openRecent(Object info) {
		ActionEditEvent aee = (ActionEditEvent) info;
		String filename = (String) ((Value) aee.getSource()).getObject();
		setProperty(PROJECT_DIRECTORY, (new File(filename)).getParentFile().getAbsolutePath());
		FileSource fs = FileSource.createFileSource(filename, IO.getMimeType(filename), this, null);
		open(fs, null);
		addToLastUsed(this, fs.getInputFile());
	}


	public void treeNodesInserted (TreeModelEvent e)
	{
		treeStructureChanged (e);
	}

	public void treeNodesRemoved (TreeModelEvent e)
	{
		treeStructureChanged (e);
	}

	public void treeNodesChanged (TreeModelEvent e)
	{
		treeStructureChanged (e);
	}

	public void treeStructureChanged (TreeModelEvent e)
	{
		if (!((FileSystem) e.getSource ()).isPersistent ())
		{
			setModified ();
		}
	}

	private final MessageHandler msgHandler = new Commands (this);

	public void addConnection (Connection cx)
	{
		cx.addMessageHandler (msgHandler);
		synchronized (connections)
		{
			connections.add (cx);
		}
	}

	public void removeConnection (Connection cx)
	{
		cx.removeMessageHandler (msgHandler);
		synchronized (connections)
		{
			connections.remove (cx);
		}
	}

	public Connection[] getConnections ()
	{
		synchronized (connections)
		{
			for (int i = connections.size () - 1; i >= 0; i--)
			{
				if (((Connection) connections.get (i)).isClosed ())
				{
					connections.remove (i);
				}
			}
			return (Connection[]) connections
				.toArray (new Connection[connections.size ()]);
		}
	}
	
	public static void startLocalFileSynchronization (Item item, Object info, Context ctx)
	{
		ctx.getWorkbench ().startLocalFileSynchronization ();
	}
	
	
	public static void stopLocalFileSynchronization (Item item, Object info, Context ctx)
	{
		ctx.getWorkbench ().stopLocalFileSynchronization ();
	}
	
	public void startLocalFileSynchronization() {
		if (prjFSWatcherThread!=null || fileManager!=null) {
			//TODO: add msg already stared
			return;
		}
		if (!(getRegistry().getFileSystem() instanceof LocalFileSystem)) {
			String msg = Utils.formatMessage(IMP.I18N.msg("filesynchronization.dialog.failed.msg", 
					(String) getProperty (PROJECT_DIRECTORY)), 400);
			getWindow().showDialog (IMP.I18N.msg("filesynchronization.dialog.title"), 
					msg,
					Window.PLAIN_MESSAGE);
			return;
		}
		// Add a listener to the file system on the device
		if ((String) getProperty (PROJECT_DIRECTORY) != null &&
				!(this == IMP.getInstance ().getMainWorkbench ())) {

			String msg = Utils.formatMessage(IMP.I18N.msg("filesynchronization.dialog.started.msg", 
					(String) getProperty (PROJECT_DIRECTORY)), 400);
			getWindow().showDialog (IMP.I18N.msg("filesynchronization.dialog.title"), 
					msg,
					Window.PLAIN_MESSAGE);
			// start by saving the workbench (to update the projects files and the filesystem)
			save(false);
			UIProperty.FILE_SYNCHRONISATION.setValue(this, true);
			
			prjFSWatcherThread = new ProjectFSWatcher((String) getProperty (PROJECT_DIRECTORY), this);
			prjFSWatcherThread.start();
			
			
			try {
				fileManager = new LocalFileManager(this);
				fileManager.loadFromDirectory((String) getProperty (PROJECT_DIRECTORY), this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		else {
			//TODO: change this message
			String msg = Utils.formatMessage(IMP.I18N.msg("filesynchronization.dialog.failed.msg", 
					(String) getProperty (PROJECT_DIRECTORY)), 400);
			getWindow().showDialog (IMP.I18N.msg("filesynchronization.dialog.title"), 
					msg,
					Window.PLAIN_MESSAGE);
			return;
		}
	}
	
	
	public void stopLocalFileSynchronization() {
		UIProperty.FILE_SYNCHRONISATION.setValue(this, false);
		if (prjFSWatcherThread!=null)
			prjFSWatcherThread.stopThread();
		if (fileManager!=null)
			fileManager.reset();
	}

	@Override
	public boolean isSelected() {
		return false;
	}

	@Override
	public void getProjectInfo(Object info) {
	}
	
	// Duplicate of IMP
	private static FilterSource toFilterSource (View view)
	{
		return new ObjectSourceImpl (view, "view", view.getFlavor (), view
			.getWorkbench ().getRegistry ().getRootRegistry (), null);
	}

	/**
	 * Wrapper for export command - the info should be "transformed" into the proper used data 
	 * for export(FilterSource)
	 */
	@Override
	public void export(Object info) throws Exception {
		if (!(info instanceof ActionEditEvent)){
			return;
		}
		setProperty(EXPORT_VISIBLE_LAYER, false);
		ActionEditEvent e = (ActionEditEvent)info;
		export (toFilterSource ((View) e.getPanel ()));
	}
	
	
	@Override
	public void exportFromNode(Object info) throws Exception {
		if (!(info instanceof ActionEditEvent)){
			return;
		}
		
		// The export works as follow: 
		// 1: Get the selected node and its subgraph. 
		// 2: Cache the "ignored" of each node
		// 3: Set selected node as ignored=false, the other nodes are set as ignored=true
		// 5: Export
		// 6: return the ignored to their original state.
		ActionEditEvent e = (ActionEditEvent)info;
		View view = (View) e.getPanel ();
		
		ViewSelection s = ViewSelection.get (view);
		if (s== null) {
			return;
		}
		Node root;
		ViewSelection.Entry[] entries = s.getAll (ViewSelection.SELECTED);
		Path p = entries[0].getPath ();
		int l = GraphUtils.lastIndexOfTree (p, view.getWorkbenchGraphState ());
		Object o = p.getObject(l);
		if (o != null &&
				o instanceof Node) {
			root = (Node) o;
		} else {
			return;
		}		
		
		exportFromNode(info, root);
	}
	

	@Override
	public void exportFromNode(Object info, Node root) throws Exception {		
		// The export works as follow: 
		// 1: Get the selected node and its subgraph. 
		// 2: Cache the "ignored" of each node
		// 3: Set selected node as ignored=false, the other nodes are set as ignored=true
		// 5: Export
		// 6: return the ignored to their original state.
		// get the list of all nodes
		if (root == null) {return;}
		
		HashMap<Node, String> allNodes = new HashMap<Node, String> ();
		ObjectList<Node> v = new ObjectList<Node> ();
		GraphManager graph = getRegistry().getProjectGraph();
		graph.getListOfNodes (graph.getRoot(), null, false, allNodes, v);	
		
		// get the list of node in the subgraph from the "root" selected node
		HashMap<Node, String> visited = new HashMap<Node, String> ();
		ObjectList<Node> toVisit = new ObjectList<Node> ();
		graph.getListOfNodes (root, null, false, visited, toVisit);	
		visited.put(root,"");
		
		for(Entry<Node, String> item : allNodes.entrySet()) {
			item.setValue(Boolean.toString(item.getKey().getIgnored()));
			if (visited.containsKey(item.getKey())
//											&& item.getKey() instanceof ShadedNull
					) 
				item.getKey().setIgnored(false);
			else item.getKey().setIgnored(true);
		}

		
		setProperty(EXPORT_VISIBLE_LAYER, true);
		export(toFilterSource ((View)((ActionEditEvent)info).getPanel()));
		
		for(Entry<Node, String> item : allNodes.entrySet()) {
			item.getKey().setIgnored(Boolean.valueOf(item.getValue()));
		}
	}

	/**
	 * Get the file to save as from GUI command (from swing toolkit & window)
	 */
	@Override
	public boolean saveAs(Object info) {
		FileChooserResult fr = chooseFileToSave(UI.I18N.getString("filedialog.saveproject", "Save Project"),
				IOFlavor.REGISTRY, null);
		if ((fr != null) && save(getRegistry(), fr.file, fr.getMimeType())) {
			setIgnoreIfModified(false);
			setModified(false);
			setName(IO.toSimpleName(fr.file.getName()));
			setFile(fr.file, fr.getMimeType());
			return true;
		}
		return false;
	}

	
	/** 
	 * In IMP workbenches addnodes is perfomed by IMP
	 */
	@Override
	public void addNode(Object info) throws Exception {
		// TODO Auto-generated method stub
	}

	/**
	 * In imp workbenches source files are managed by the file explorer
	 */
	@Override
	public void addSourceFile(Object info) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void renameSourceFile(Object info) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeSourceFile(Object info) throws Exception {
		// TODO Auto-generated method stub
		
	}

	/**
	 * this is handled by the rgg toolbar . Should not be accessed from the workbench
	 */
	@Deprecated
	@Override
	public void listFunctions(Object info) {
		// TODO Auto-generated method stub
	}

	/**
	 * Execute a command, given by command line, 
	 */
	@Override
	public void execute(Object info) throws Exception {
		// TODO Auto-generated method stub	
	}

	@Override
	public String getApplicationName() {
		return "IMP";
	}
	
}