module imp {
	exports de.grogra.imp;
	exports org.jibble.epsgraphics;
	exports de.grogra.imp.objects;
	exports de.grogra.imp.awt;
	exports de.grogra.imp.feedback;
	exports de.grogra.imp.edit;
	exports de.grogra.imp.net;
	exports de.grogra.imp.registry;
	exports de.grogra.imp.io;
	exports de.grogra.imp.viewhandler;
	
	requires graph;
	requires math;
	requires platform;
	requires platform.core;
	requires platform.swing;
	requires projectmanager;
	requires utilities;
	requires vecmath;
	requires xl.core;
	requires java.datatransfer;
	requires java.desktop;
	requires java.logging;
	requires java.prefs;
	requires java.sql;
	requires java.xml;
	requires jocl;

	opens de.grogra.imp to platform.core, utilities;
	}