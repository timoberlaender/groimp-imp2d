# IMP plugin 

The IMP plugin adds facilities to display and edit graphs as defined by the Graph project from above. Furthermore, it defines support for images, fonts and some other data types. Finally, it contains a simple implementation of an HTTP server. This is an interesting feature and enables GroIMP models to serve as dynamic content providers for web applications.

Additionally, the IMP plugin contains the default application of GroIMP, the GUI. 
