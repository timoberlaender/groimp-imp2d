package de.grogra.imp2d;

import de.grogra.imp.viewhandler.HighlighterImpl;
import de.grogra.imp.viewhandler.SelectionEventFactory;
import de.grogra.imp.viewhandler.ToolEventFactory;
import de.grogra.imp.viewhandler.ViewEventFactory;
import de.grogra.persistence.SCOType;

public class View2DEventFactory extends ViewEventFactory {

	//enh:sco SCOType
	
	public View2DEventFactory() {
		this.navigatorFactory = new Navigator2DFactory();
		this.toolFactory = new ToolEventFactory();
		this.selectionFactory = new SelectionEventFactory();
		this.highlighter = new HighlighterImpl();
		
		setPickVisitor(new PickRayVisitor ());
		setPickToolVisitor(new PickRayVisitor ());
	}
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;


	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (View2DEventFactory representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}


		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		public Object newInstance ()
		{
			return new View2DEventFactory ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (View2DEventFactory.class);
		$TYPE.validate ();
	}

//enh:end
}
