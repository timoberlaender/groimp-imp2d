# IMP-2D plugin 

IMP-2D provides node classes for 2D geometry. Such a 2D scene graph can be displayed in a 2D view, but there is also the option to display a 2D view on the topology of an arbitrary graph.
